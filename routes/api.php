<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('test', 'TestController@test');
Route::apiResource('subjects', 'SubjectController');
Route::apiResource('modules', 'ModuleController');
Route::apiResource('chapters', 'ChapterController');
Route::apiResource('options', 'OptionController');
Route::resource('cards', 'CardController');
Route::resource('profiles', 'ProfileController');
Route::resource('review', 'ReviewController');

Route::resource('sources', 'SourceController');
Route::resource('tags', 'TagController');
Route::resource('classify', 'ImageClassifyController');
Route::resource('settings', 'SettingController');
Route::resource('performances', 'PerformanceController');
Route::resource('smc', 'SmcController');
Route::resource('generals', 'GeneralController');
Route::resource('form-tester', 'FormTesterController');

Route::get('select-option-label-value/{table}/{label}/{value}/{all?}', 'StockController@selectOptionNameValue');

//Route::apiResource('profiles/create-page-data', 'ProfileController@createPageData');