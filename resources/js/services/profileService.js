import sharedService from './sharedService';
import { profileData } from '../dataSource/profileData';

export default class profileService extends sharedService {

	constructor(){
		super();
		this.namespace = profileData.namespace;
		this.dataSource = profileData;
	}

}