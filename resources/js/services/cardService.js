import sharedService from './sharedService';
import { cardData } from '../dataSource/cardData';

export default class cardService extends sharedService {

	constructor(){
		super();
		this.namespace = cardData.namespace;
		this.dataSource = cardData;
	}
	
}