import sharedService from './sharedService';
import { settingData } from '../dataSource/settingData';

export default class settingService extends sharedService {

	constructor(){
		super();
		this.namespace = settingData.namespace;
		this.dataSource = settingData;
	}
	
}