import sharedService from './sharedService';
import { tagData } from '../dataSource/tagData';

export default class tagService extends sharedService {

	constructor(){
		super();
		this.namespace = tagData.namespace;
		this.dataSource = tagData;
	}
	
}