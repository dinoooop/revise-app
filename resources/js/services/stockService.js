import { fielder, basic, request } from '../helpers';

const namespace = {
    selectLabelValue:  '/select-option-label-value',
    selectOption:  '/select-option',
};

export default class stockService {
	
	static roles(){
		let endpoint = namespace.selectLabelValue + '/roles/label/id/all';
		return request.stock(endpoint);
	}

	static caps() {
        let endpoint = namespace.selectLabelValue + '/capabilities/label/id/all';
        return request.stock(endpoint);
    }

    static countries(search) {
        let endpoint = namespace.selectLabelValue + '/countries/name/id';
        return request.stock(endpoint, {search: search});
    }

    static states(search){
        let endpoint = namespace.selectOption + '/addresses/state';
        return request.stock(endpoint, {search: search});
    }

    static cities(search){
        let endpoint = namespace.selectOption + '/addresses/city';
        return request.stock(endpoint, {search: search});
    }

    static smc(param = {}){
        let endpoint = '/smc';
        return request.stock(endpoint, param);
    }

    static modules(){
        let endpoint = namespace.selectLabelValue + '/modules/name/id/all';
        return request.stock(endpoint);
    }

    static tags(){
        let endpoint = namespace.selectLabelValue + '/tags/name/id/all';
        return request.stock(endpoint);
    }

}


