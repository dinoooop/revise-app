import sharedService from './sharedService';
import { imageClassifyData } from '../dataSource/imageClassifyData';

export default class imageClassifyService extends sharedService {

	constructor(){
		super();
		this.namespace = imageClassifyData.namespace;
		this.dataSource = imageClassifyData;
	}
	
}