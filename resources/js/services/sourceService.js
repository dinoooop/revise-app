import sharedService from './sharedService';
import { sourceData } from '../dataSource/sourceData';

export default class sourceService extends sharedService {

	constructor(){
		super();
		this.namespace = sourceData.namespace;
		this.dataSource = sourceData;
	}
	
}