import sharedService from './sharedService';
import { generalData } from '../dataSource/generalData';

export default class generalService extends sharedService {

	constructor(){
		super();
		this.namespace = generalData.namespace;
		this.dataSource = generalData;
	}
	
}