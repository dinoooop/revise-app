import sharedService from './sharedService';
import { formTesterData } from '../dataSource/formTesterData';

export default class formTesterService extends sharedService {

	constructor(){
		super();
		this.namespace = formTesterData.namespace;
		this.dataSource = formTesterData;
	}
	
}