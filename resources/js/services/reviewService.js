import sharedService from './sharedService';
import { reviewData } from '../dataSource/reviewData';

export default class reviewService extends sharedService {

	constructor(){
		super();
		this.namespace = reviewData.namespace;
		this.dataSource = reviewData;
	}
	
}