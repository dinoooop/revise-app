import sharedService from './sharedService';
import { smcData } from '../dataSource/smcData';

export default class smcService extends sharedService {

	constructor(){
		super();
		this.namespace = smcData.namespace;
		this.dataSource = smcData;
	}

}