import { fielder, request } from '../helpers';
import { index } from '../dataSource/sharedData';

export default class sharedService {
	
	index(toSubmit){
		return request.index(this.namespace, toSubmit);
	}

	// Data needs to show in create page, GET request
	create(toSubmit){
		return request.get(`${this.namespace}/create`, toSubmit);
	}

	// Save new data
	store(toSubmit){
		return request.store(this.namespace, toSubmit);
	}

	// Data needs to show in edit page, GET request
	edit(toSubmit){
		const {id, formData} = fielder.extractIdToSubmit(toSubmit);
		return request.get(`${this.namespace}/${id}/edit`, formData);
	}

	// Update existing params
	update(toSubmit){
		const {id, formData} = fielder.extractIdToSubmit(toSubmit);
		return request.update(`${this.namespace}/${id}`, formData);
	}

	show(toSubmit){
		const {id, formData} = fielder.extractIdToSubmit(toSubmit);
		return request.show(`${this.namespace}/${id}`, toSubmit);
	}

	delete(toSubmit){
		const {id, formData} = fielder.extractIdToSubmit(toSubmit);
		return request.delete(`${this.namespace}/${id}`, formData);
	}

	getFields(formGroup){

		
		switch(formGroup){

			case 'index':
				return fielder.getFormFields('index', this.dataSource.index);
				
			case 'create':
				return fielder.getFormFields('create', this.dataSource.create);

			case 'edit':
				return fielder.getFormFields('edit', this.dataSource.edit);

			case 'login':
				return fielder.getFormFields('login', this.dataSource.login);

		}
		
	}

	authUser(){
		return request.show('/login/auth');
	}

}