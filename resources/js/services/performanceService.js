import sharedService from './sharedService';
import { performanceData } from '../dataSource/performanceData';

export default class performanceService extends sharedService {

	constructor(){
		super();
		this.namespace = performanceData.namespace;
		this.dataSource = performanceData;
	}
	
}