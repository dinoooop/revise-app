import sharedService from './sharedService';
import { subjectData } from '../dataSource/subjectData';

export default class subjectService extends sharedService {

	constructor(){
		super();
		this.namespace = subjectData.namespace;
		this.dataSource = subjectData;
	}
	
}