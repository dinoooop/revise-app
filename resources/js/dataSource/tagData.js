import { indexForm } from './sharedData';

const generalForm = [
	{
        label: 'Tag Name',
        name: 'name',
        type: 'text',
        rules: {
            required: true
        }
    },
    {
        label: 'Description',
        name: 'description',
        type: 'textarea',
    }
];

export const tagData = {
    edit: generalForm,
	create: generalForm,
    index: indexForm,
    dmSet: true, // Dynamicaly change the data with auth 
	namespace: '/tags',
    manage: 'manage_tags',
    createNew: true,
    indexTableProps: {
        bulkAction: false,
        search: false,
        actions: true,
        availActions: {
            edit: true,
            deleteItem: true
        },
        columns: [
            {
                name: "id",
                label: "ID",
                sort: true,
            },
            {
                name: "text",
                label: "Tag Name",
                sort: true,
            }
            
        ]
        
    },
}