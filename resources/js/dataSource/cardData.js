import { indexForm } from './sharedData';
import { configData } from './configData';

const csvImport = [
    {
        name: 'csv_import',
        label: 'CSV Import',
        type: 'file-upload',
    },
    {
        name: 'message',
        type: 'hidden',
        default: '',
    },
    {
        name: 'matrix_que',
        label: 'Matrix format questions',
        type: 'checkbox-single',
    },
    {
        name: 'processing',
        label: 'Need custom code processing',
        type: 'checkbox-single',
    },
    {
        name: 'voc',
        label: 'Vocabulary Import',
        type: 'checkbox-single',
    },
];

const editForm = [
	{
        label: 'Question',
        name: 'que',
        type: 'textarea',
        rules: { required: true }
    },
    {
        label: 'Answer',
        name: 'ans',
        type: 'textarea',
        rules: { required: true }
    },
    {
        id: 'subject',
        name: 'subject_id',
        label: 'Subject',
        type: 'radio',
        rules: { 'required': true },
        options: [],
        onChange: true,
    },
    {
        id: 'module',
        name: 'module_id',
        label: 'Module',
        type: 'radio',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        id: 'chapter',
        name: 'chapter_id',
        label: 'Chapter',
        type: 'radio',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    
    {
        id: 'ans_type',
        name: 'ans_type',
        label: 'Ans Type',
        type: 'radio',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        name: 'source_id',
        label: 'Source',
        type: 'radio',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        name: 'tag_id',
        label: 'Tags',
        type: 'checkbox',
        rules: { 'required': false },
        options: [],
        onChange: true,
        default:[]
    },
    {
        id: 'stage',
        name: 'stage',
        label: 'Stage',
        type: 'radio',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        name: 'level',
        label: 'Level',
        type: 'radio',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        id: 'status',
        name: 'status',
        label: 'Status',
        type: 'radio',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        name: 'history',
        type: 'no-element',
        varType: 'array',
    },
];

export const cardData = {
    edit: editForm,
	create: editForm,
    index: indexForm,
    csvImport: csvImport,
    dmSet: true, // Dynamicaly change the data with auth 
	namespace: '/cards',
    manage: 'manage_cards',
    createNew: true,
    indexTableProps: {
        bulkAction: false,
        search: true,
        actions: true,
        availActions: {
            edit: true,
            deleteItem: true,
            source_copy: true,
        },
        columns: [
            {
                name: "id",
                label: "ID",
                sort: true,
            },
            {
                name: "que",
                label: "Question",
                sort: true,
            },
            {
                name: "ans",
                label: "Answer",
                sort: true,
            },
            
            {
                name: "review_history_count",
                label: "Count",
                sort: true,
            },
            
        ]
    },
}