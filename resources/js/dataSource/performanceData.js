import { indexForm } from './sharedData';

const generalForm = [
	
];

export const performanceData = {
    edit: generalForm,
	create: generalForm,
    index: indexForm,
    dmSet: true, // Dynamicaly change the data with auth 
	namespace: '/performances',
    manage: 'manage_performances',
    createNew: false,
    indexTableProps: {
        bulkAction: false,
        search: false,
        actions: false,
        availActions: {
            edit: false,
            deleteItem: false
        },
        columns: [
            {
                name: "review_date",
                label: "Review Date",
                sort: true,
            },
            {
                name: "success",
                label: "Success",
                sort: true,
            },
            {
                name: "failed",
                label: "Failed",
                sort: true,
            },
            {
                name: "nc",
                label: "NC",
                sort: true,
            },
            {
                name: "total_card",
                label: "Total Cards",
                sort: true,
            },
            {
                name: "mark",
                label: "Marks",
                sort: true,
            },
            
        ]
        
    },
}