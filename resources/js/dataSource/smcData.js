const createForm = [
    {
        name: 'name',
        label: 'SMC',
        type: 'text',
        rules: {
            required: false
        }
    },
    {
        name: 'subject_button',
        label: 'Add',
        type: 'button-input',
        hiddenName: 'smctype',
        default: 'subject',
        onChange: true,
    },
    {
        name: 'module_button',
        label: 'Add',
        type: 'button-input',
        hiddenName: 'smctype',        
        default: 'module',
        onChange: true,        
    },
    {
        name: 'chapter_button',
        label: 'Add',
        type: 'button-input',
        hiddenName: 'smctype',
        default: 'chapter',
        onChange: true,
    },
    {
        name: 'submit_add',
        label: 'Submit',
        type: 'button-input',
        hiddenName: 'action',
        default: 'store',
        onChange: true,
    },
    {
        name: 'submit_edit',
        label: 'Submit',
        type: 'button-input',
        hiddenName: 'action',
        default: 'update',
        onChange: true,
    },
    {
        name: 'name_edit',
        label: 'SMC',
        type: 'text',
    },
    {
        name: 'id_edit',
        type: 'hidden',
    },
    {
        name: 'smctype',
        type: 'hidden',
    },
    {
        name: 'action',
        type: 'hidden',
    },
    {
        name: 'subject_id',
        label: 'Subject',
        type: 'radio',
        onChange: true,
    },
    {
        name: 'module_id',
        label: 'Module',
        type: 'radio',
        onChange: true,
    },
    {
        name: 'chapter_id',
        label: 'Chapter',
        type: 'radio',        
        onChange: true,
    },
];

export const smcData = {
	create: createForm,
    init: true,
	namespace: '/smc',
    createFormServer: true,
}
