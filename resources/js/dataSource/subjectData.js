import { indexForm } from './sharedData';

const generalForm = [
	{
        label: 'Subject Name',
        name: 'name',
        type: 'text',
        rules: {
            required: true
        }
    }
    

];

export const subjectData = {
    edit: generalForm,
	create: generalForm,
    index: generalForm,
    dmSet: true, // Dynamicaly change the data with auth 
	namespace: '/subjects',
    manage: 'manage_subjects',
    createNew: false,
    indexTableProps: {
        bulkAction: false,
        search: false,
        actions: true,
        availActions: {
            edit: true,
            deleteItem: false,
            review: true,
        },
        columns: [
            {
                name: "id",
                label: "ID",
                sort: true,
            },
            {
                name: "text",
                label: "Subject Name",
                sort: true,
            }
            
        ]
        
    },
}