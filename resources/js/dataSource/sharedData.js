export const indexForm = [
    {
        label: 'Search',
        name: 'search',
        type: 'text',
        onChange: true,
        showLabel: false,
    },
    {
        label: 'Bulk action',
        name: 'bulkaction',
        type: 'select',
        onChange: true,
        showLabel: false,
        options: [
            { label: "Delete", value: "delete" }
        ]
    }
];

export const sharedDataWithDescription = {
    init: true, // show components only when datasource is set
    dmSet: true, // Dynamicaly change the data with auth 
    createFormServer: true, // there are checkbox/radio options from server for the create page
}