export const configData = {
    host: 'http://127.0.0.1:8000',
	status: {
		'active': 1,
		'suspend': 2,
        'processing': 3,
	},
	ansType: {
		'vocal': 1,
        'write': 2,
	},
    stage :{
    	'D1': 1,
        'H60': 2,
        'F60': 3,
        'S60': 4,
        'E60': 5,
        'KW': 6,
    },
    success: 1,
    failed: 2,
    imgType: {
    	que: 'que',
    	ans: 'ans',
    	sub: 'sub',
        note: 'note',
        ignore: 'ignore',
        no_answer: 'no_answer',
    },
    append_txt: [
        '(Y)', '(D)', '(L)', '(EL)', '(Kn)',
        '(P)', '(O)', '(b)', '(Nz)', '(Mz)', 
        '(ff)', '(A)', '(Riv)', '(rel)',
        '(Ab)', '(Count)', '(C)', '(L2L)', '(Y-Y)', '(origin)', '(mouth)', 
    ],
};