const statisticsForm = [
	{
        name: 'action',
        type: 'hidden',
        default: 'statistics'
    },
    {
        name: 'status', // statistics related to status 
        type: 'hidden',
        default: []
    },
    {
        name: 'today', 
        type: 'hidden',
        default: []
    },
    {
        name: 'stage', // statistics related to stage
        type: 'hidden',
        default: []
    },
    {
        name: 'other', 
        type: 'hidden',
        default: []
    },
    {
        name: 'level',
        type: 'hidden',
        default: []
    },
    {
        name: 'levels',
        type: 'hidden',
        default: []
    },
    
];

const reviewLeft = [
    {
        name: 'items',
        type: 'hidden',
        default: []
    },
    {
        name: 'action',
        type: 'hidden',
        default: 'review_left'
    },
];

const mathsClassify = [
    {
        name: 'image_url',
        type: 'hidden',
        default: ''
    },
    {
        name: 'id',
        type: 'hidden',
        default: ''
    },
    {
        name: 'action',
        type: 'hidden',
        default: 'maths_classify'
    },
    {
        name: 'error_server',
        type: 'hidden',
        default: ''
    },
    {
        name: 'error_server_msg',
        type: 'hidden',
        default: ''
    },
];


const reviewHistoryView = [
    {
        name: 'histories',
        type: 'hidden',
        default: []
    },
    {
        name: 'action',
        type: 'hidden',
        default: 'review_history'
    },
];

export const generalData = {
    statistics: statisticsForm,
    review_left: reviewLeft,
    maths_classify: mathsClassify,
    review_history_view: reviewHistoryView,
	namespace: '/generals',
}
