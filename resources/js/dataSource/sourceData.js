import { indexForm } from './sharedData';

const generalForm = [
	{
        label: 'Source Name',
        name: 'name',
        type: 'text',
        rules: {
            required: true
        }
    }
];

export const sourceData = {
    edit: generalForm,
	create: generalForm,
    index: indexForm,
    dmSet: true, // Dynamicaly change the data with auth 
	namespace: '/sources',
    manage: 'manage_sources',
    createNew: true,
    indexTableProps: {
        bulkAction: false,
        search: false,
        actions: true,
        availActions: {
            edit: true,
            deleteItem: true
        },
        columns: [
            {
                name: "id",
                label: "ID",
                sort: true,
            },
            {
                name: "text",
                label: "Source Name",
                sort: true,
            }
            
        ]
        
    },
}