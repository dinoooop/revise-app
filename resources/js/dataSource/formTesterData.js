const generalForm =  [
    {
        name: 'csv_import',
        label: 'CSV Import',
        type: 'file-upload',
    },
    {            
        label: 'Name',
        name: 'name',
        type: 'text',
    },
];


export const formTesterData = {
    edit: generalForm,
    create: generalForm,
    index: generalForm,
    namespace: '/form-tester',
    manage: 'manage_form_tester',
    createHaveFile: true,
}


const tested = [
    {
        name: 'activate',
        label: 'Activate',
        labelAtFirstTime: 'Activate',
        labelChangeOnClick: 'Activated',
        changed: true,
        style: 'text', // text: look like text / button: looks like button
        type: 'button-self-input',
        rules: { 'required': false },
    },
    {
        name: 'user',
        label: 'User',
        type: 'select-auto',
        stockOption: 'nonDeptHead',
        oneDimOption: false,
        rules:{
            required: true,
        },
    },
    // example for select auto create on two dimentional array options
    {
        name: 'country',
        label: 'Country',
        type: 'select-auto',
        oneDimOption: false,
        stockOption: 'countries', 
        countOption: 0,
        createNewOption: true,
        rules:{
            required: false,
        }
    },
    // Example for select auto create for one dimentional array options
    {
        name: 'city',
        label: 'City',
        type: 'select-auto', 
        oneDimOption: true, 
        stockOption: 'cities',
        countOption: 0,
        createNewOption: true,
        rules:{
            required: true,
        }
    },
    {
        type: 'select',
        name: 'department_id',
        label: 'Department',            
        rules: {
            'required': true
        },
        options: [
            {value: 1, label: "Software Dev"},
            {value: 2, label: "Designer"},
        ],
    },
    // select-create
    // types are different on new item
    {            
        name: 'designation',
        label: 'Designation',
        type: 'select-create',
        rules: {
            'required': true
        },
        options: [
            {value: 1, label: "Software Dev"},
            {value: 2, label: "Designer"},
        ],
    },
    {            
        name: 'active',
        label: 'Active',
        type: 'checkbox-single',
        value: 1,
    },
    {
        name: 'user_id',
        type: 'hidden',
        value: '10',
    },
    {            
        name: 'dob',
        label: 'Date of birth',
        type: 'date',
        value: "",
    },
    {
        name: 'description',
        label: 'Description',
        type: 'textarea',            
        rules: {
            required: true
        }
    },
    {
        name: 'password',
        label: 'Password',
        type: 'password',
        value:'',
        rules: {
            required: true
        }
    },
    {            
        name: 'confirm_password',
        label: 'Confirm password',
        type: 'password',
        rules: {
            required: true
        }
    },
    {            
        label: 'Email address',
        name: 'email',
        type: 'email',
        rules: {
            required: true
        }
    },
    {            
        label: 'Gender',
        name: 'gender',
        type: 'radio',
        options: [
            {value: 'male', label: "Male"},
            {value: 'female', label: "Female"},
        ],
        rules: {
            'required': true
        },            
    },
    {            
        label: 'Hobbies',
        name: 'hobbies',
        value: [],
        default: [1,2],
        type: 'checkbox',
        changed: true, // is user change this field
        rules: {
            'required': true
        },
        options: [
            {value: 'chess', label: "Chess"},
            {value: 'swim', label: "Swimming"},
            {value: 'guitar', label: "Guitar"},
        ],
    },
    {
        label: 'Name',
        name: 'name',            
        type: 'text',
        onChange: false,
        rules: {
            'required': true,
            'max': 20,
        },
    },      
];