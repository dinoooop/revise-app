const generalForm = [
	{
        label: 'SMC',
        name: 'name',
        type: 'text',
        rules: {
            required: true
        }
    }
];

const indexForm = [
    {
        name: 'is_active',
        type: 'hidden',
        value: '',
    },
    {
        name: 'id',
        type: 'hidden',
    },
];

const createForm = [
    {
        label: 'Profile Name',
        name: 'name',
        type: 'text',
        
        rules: {
            required: false
        }
    },
    {
        name: 'interval',
        label: 'Interval',
        type: 'text',
        rules: {
            required: false
        },
    },
    {
        name: 'nc_gap',
        label: 'NC Gap',
        type: 'text',
        rules: {
            required: false
        },
        onChange: true,
    },
    {
        name: 'action',
        type: 'hidden',
    },
    {
        id: 'subject',
        name: 'subject_id',
        label: 'Subject',
        type: 'checkbox',
        rules: { 'required': true },
        options: [],
        onChange: true,
    },
    {
        name: 'id',
        type: 'hidden',
    },
    {
        id: 'module',
        name: 'module_id',
        label: 'Module',
        type: 'checkbox',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        id: 'chapter',
        name: 'chapter_id',
        label: 'Chapter',
        type: 'checkbox',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        name: 'source_id',
        label: 'Source',
        type: 'checkbox',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        name: 'tag_id',
        label: 'Tags',
        type: 'checkbox',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    
    {
        id: 'ans_type',
        name: 'ans_type',
        label: 'Ans Type',
        type: 'checkbox',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        name: 'card_count',
        type: 'display-value',
        label: 'Card Count',
    },
    {
        id: 'stage',
        name: 'stage',
        label: 'Stage',
        type: 'checkbox',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        name: 'level',
        label: 'Level',
        type: 'checkbox',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        id: 'status',
        name: 'status',
        label: 'Status',
        type: 'checkbox',
        rules: { 'required': false },
        options: [],
        onChange: true,
    },
    {
        name: 'submit',
        label: 'Submit',
        labelAtFirstTime: 'Submit',
        labelChangeOnClick: 'Submit',
        type: 'button-self-input',
        onChange: true,
        rules: { 'required': false },
    },
];

export const profileData = {
    init: true,
    edit: createForm,
	create: createForm,
    index: indexForm,
    dmSet: true,
	namespace: '/profiles',
    manage: 'manage_profile',
    createNew: true,
    createFormServer:true,
    indexTableProps: {
        bulkAction: false,
        search: false,
        actions: true,
        availActions: {
            edit: true,
            deleteItem: true,
            activate: true,
        },
        columns: [
            {
                name: "id",
                label: "ID",
                sort: true,
            },
            {
                name: "text",
                label: "Profile Name",
                sort: false,
            }
        ]        
    },
}