import { configData } from './configData';
import { basic } from '../helpers';

const generalForm = [
    {
        name: 'error', // Server Error (Boolean)
        type: 'hidden',
        default: false,
        toSubmit: false

    },
    {
        name: 'errorMsg', // Server Error message (txt)
        type: 'hidden',
        default: '',
        toSubmit: false
    },
    {
        name: 'vocal',
        type: 'hidden',
        varType: 'array',
    },
    {
        name: 'write',
        type: 'no-element',
        varType: 'array',
    },
    {
        name: 'card',
        type: 'hidden',
        varType: 'array',
        toSubmit: false,
    },
    {
        name: 'history',
        type: 'no-element',
        varType: 'array',
    },
    {
        label: 'Tag',
        name: 'tag_id',
        type: 'checkbox-list',
        stockOption: 'tags',
        rules: { required: false }
    },
    {
        label: 'Know Well',
        name: 'stage',
        type: 'radio',
        options: [{label: 'KW', value: configData.stage.KW}],
        rules: { required: false },
    },
    {
        label: 'Status',
        name: 'status',
        type: 'radio',
        options: [{label: 'Suspend', value: configData.status.suspend}],
        rules: { required: false }
    },
    {
        label: 'Result',
        name: 'result',
        type: 'radio',
        options: [
            {label: 'Success', value: configData.success}, 
            {label: 'Failed', value: configData.failed}
        ],
        rules: { required: false }
    },
    {
        name: 'review_count',
        label: 'Review Count',
        type: 'display-value',
    },
    {
        name: 'review_after',
        label: 'Review After',
        type: 'display-value',
    },
    {
        name: 'card_count',
        label: 'Card Count',
        type: 'display-value',
    },
    {
        name: 'subdocs_count',
        type: 'no-element',
        varType: 'string',
        default: 0
    },
    {
        name: 'card_id',
        label: 'Card Id',
        type: 'display-value',
    },
    {
        name: 'subdocs_list', //list of subdocs
        type: 'hidden',
        default: []
    },
    {
        name: 'review_mode',
        type: 'hidden',
    },
    {
        name: 'subject_id',
        type: 'hidden',
    },
    {
        name: 'ans_type',
        type: 'hidden',
    },
    {
        name: 'append_txt', // e.g (Y), (L)
        label: 'Quick Edit',
        type: 'radio',
        default: '',
        options: basic.labelValueOneArray(configData.append_txt),
    },
];

export const reviewData = {
	create: generalForm,
    index: generalForm, // List subjects
    dmSet: true, // Dynamicaly change the data with auth 
	namespace: '/review',
    manage: 'manage_reviews',
    createNew: false,
    createFormServer: true,
    indexFormServer: true,
}
