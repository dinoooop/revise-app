import {configData} from './configData';
import {basic} from '../helpers';

const generalForm = [
    {
        name: 'error_server', // Server Error (Boolean)
        type: 'hidden',
        default: false,
        toSubmit: false

    },
    {
        name: 'error_server_msg', // Server Error message (txt)
        type: 'hidden',
        default: '',
        toSubmit: false
    },
    {
        name: 'module', // url param
        type: 'hidden',
        default: ''
    },
    {
        name: 'module_list', // module list array
        type: 'hidden',
        default: []
    },
    {
        name: 'image_url',
        type: 'hidden',
        varType: 'string',
        toSubmit: true, // submit as current
    },
    {
        name: 'next_prev_active',
        type: 'hidden',
        varType: 'array',
        toSubmit: false, // submit as current
    },
    {
        label: 'Tag',
        name: 'tag_id',
        type: 'checkbox-list',
        stockOption: 'tags',
        rules: { required: false }
    },
    {
        name: 'image_type',
        label: 'Type',
        type: 'radio',
        rules: { 'required': true },
        options: basic.labelValueConvertObj(configData.imgType),
    },
    {
        name: 'ans_type',
        label: 'Ans Type',
        type: 'radio',
        rules: { 'required': false },
        options: basic.labelValueConvertObj(configData.ansType),
    },
    {
        name: 'upto',        
        type: 'text',
        default: '',
    },
];

export const imageClassifyData = {
	create: generalForm,
    index: generalForm,
	namespace: '/classify',
    manage: 'manage_classify',
    createNew: false,
    createFormServer:true,
    
}