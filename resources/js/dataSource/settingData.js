import {configData} from './configData';
import {basic} from '../helpers';

const generalForm = [
	{
        name: 'interval',
        label: 'Interval',
        type: 'text',
        rules: {
            required: true
        },
    },
    {
        name: 'estimate_finish_day_count',
        label: 'Estimate finish day count',
        type: 'text',
    },
    {
        name: 'show_subdocs',
        label: 'Show sub documents for cards',
        type: 'checkbox-single',
        rules: { 'required': false },
    },
    
];

export const settingData = {
	create: generalForm,
	namespace: '/settings',
    manage: 'manage_settings',
    createNew: false,
    createFormServer:true,
}