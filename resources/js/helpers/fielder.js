import { basic, defaultFields, validator } from './';

export class Fielder {

	// merge fields with its default, set id etc.
	getFormFields(formName, fields){

		let newObj = {};

		for(let key in fields){
			
			if (!fields.hasOwnProperty(key)) continue;
			let defaultFieldsDup = defaultFields();
			
			if(defaultFieldsDup[fields[key]['type']]){
				newObj[fields[key]['name']] = Object.assign(defaultFieldsDup[fields[key]['type']], fields[key]);
				newObj[fields[key]['name']]['id'] = formName + '_' + fields[key]['name'];
				newObj[fields[key]['name']]['formName'] = formName;
				newObj[fields[key]['name']]['value'] = this.getAnyDefaultValue(newObj[fields[key]['name']]);
			}
			
		}

		return newObj;
	}

	// Set form data into a field
	setFormDataField(fields, formdata = {}, setDefault = true){
		for (var key in fields) {
		    if (!fields.hasOwnProperty(key)) continue;

		    //fields[key]['value'] = (!!formdata[key])? formdata[key] : this.getAnyDefaultValue(fields[key]);

		    if(fields[key]['type'] == 'select-auto'){
		    	fields[key]['displayValue'] = this.getDisplayValue(fields[key]['name'], formdata);
		    }


		    if(typeof formdata[key] != "undefined"){
		    	fields[key]['value'] = formdata[key];
		    } else {
		    	if(setDefault){
		    		fields[key]['value'] = this.getAnyDefaultValue(fields[key]);
		    	}
		    }

		    

		    // Options of checkbox, radio are may be from server

		    if(
		    	typeof formdata['options'] != "undefined"
		    	&& typeof formdata['options'][key] != "undefined"
		    	)
		    {
		    	fields[key]['options'] = basic.labelValueConvert(formdata['options'][key], 'name', 'id');
		    }

		    if(
		    	typeof formdata['selected'] != "undefined"
		    	&& typeof formdata['selected'][key] != "undefined"
		    	)
		    {

		    	fields[key]['value'] = formdata['selected'][key];
		    }
		}

		return fields;

	}

	


	getDisplayValue(name, formdata) {

		switch(name){
			case 'head_id':
				return formdata.head ? basic.displayName(formdata.head) : '';
				break;

			default:
				return formdata[name]? formdata[name]: '';

		}

	}

	// set errors from server side
	setNullValue(fields){
		
		for (var key in fields) {
		    if (!fields.hasOwnProperty(key)) continue;
		    
		    fields[key]['value'] = '';

		    if(fields[key]['type'] == 'select-auto'){
		    	fields[key]['displayValue'] = '';
		    }
		}

		return fields;
	}

	// set errors from server side
	setServerErrors(fields, errors){
		errors = this.getFirstErrors(errors);
		for (var key in fields) {
		    if (!fields.hasOwnProperty(key)) continue;
		    
		    fields[key]['error'] = (!!errors[key])? errors[key] : '';
		}

		return fields;
	}


	// get the first value in error array as error
	getFirstErrors(obj){
		let results = {};
		for(let key in obj){
			if (!obj.hasOwnProperty(key)) continue;

			results[key] = obj[key][0]? obj[key][0] : ''
		}
		return results;
	}

	// to extract formdata from fields of a form for toSubmit
	extractFormDataField(fields) {
		let result =  {};
		for (var key in fields) {
		    if (!fields.hasOwnProperty(key)) continue;
		    
		    if(fields[key]['toSubmit'] == true){
			    let obj = fields[key];
			    result[key] = obj.value;
		    }
		    
		}

		return result;
	}

	// test purpose
	considerOnly(fields, names){
		let results = {};
		for (let key in fields) {
		    if (!fields.hasOwnProperty(key)) continue;
			
			if(names.indexOf(fields[key]['name']) !== -1){
				results[key] = fields[key];
			}
		}
		return results;
	}

	getAnyDefaultValue(field){
        // empty select and field is not changed
        if(typeof field.default != "undefined"){
            return field.default;
        } else {
            return (field.varType == 'string') ? '' : []; // do not use null
        }    
    }

    isToSubmitContainFile(toSubmit){
    	for(let key in toSubmit){
    		if(
    			toSubmit[key] != null 
    			&& typeof toSubmit[key]['size'] != 'undefined'
    		){
    			return true;    			
    		}
    	}

    	return false;
    }

    setFormDataForFile(toSubmit){

    	if(this.isToSubmitContainFile(toSubmit)){
	        const formData = new FormData();
	        for(let key in toSubmit){
	          formData.append( key,  toSubmit[key] );
	        }
	        toSubmit = formData;
	    }

	    return toSubmit;
    }

    extractIdToSubmit(toSubmit){
    	let idLessFormData = {};
    	let id = 0;
    	for(let key in toSubmit){
    		if(key == 'id'){
          		id = toSubmit[key];
          		continue;
    		}
    		idLessFormData[key] = toSubmit[key];
        }

        return {'id': id, 'formData': idLessFormData};
    }

}

export const fielder = new Fielder;