import axios from 'axios';
import { appData, basic } from './';

class Request {

	api = '/api';
	
	endpoint(endpoint){
		return this.api + endpoint;
	}

	headers(){
		return { 'Authorization' : 'Bearer ' + basic.hasToken() }
	}

	// map the parameters for shorten
	encodeUrlParams(params){
		
		let target = {};
		
	    for (let key in params) {
	        if (!params.hasOwnProperty(key)) continue;

	        if(appData.urlParams[key]){
	    		target[appData.urlParams[key]] = params[key]
	        }else{
	        	target[key] = params[key]
	        }
	    }

	    return target;
	}

	index(endpoint, params = {}){
		return axios({
		  method: 'get',
		  url: this.endpoint(endpoint),
		  headers: this.headers(),
		  params: this.encodeUrlParams(params),
		}).then(res => {
			console.log("Success");
			console.log(res.data.results);
			return res.data.results;
		}).catch(err => {
			console.log("Error");
			this.handleUnauthorizedAccess(err);			
		});
	}

	show(endpoint, params = {}){
		return axios({
		  method: 'get',
		  url: this.endpoint(endpoint),
		  headers: this.headers(),
		  params: this.encodeUrlParams(params),
		}).then(res => {
			console.log("Success");
			console.log(res.data.results);
			return res.data.results;
		}).catch(err => {
			console.log("Error");
			this.handleUnauthorizedAccess(err);			
		});
	}

	store(endpoint, data){
		return axios({
		  method: 'post',
		  url: this.endpoint(endpoint),
		  headers: this.headers(),
		  'data': data
		}).then(res => {
			console.log("Success");
			console.log(res.data.results);
			return res.data.results;
		}).catch(error => {
			console.log("Error");
            console.log(error);
        });
	}

	update(endpoint, data){
		return axios({
		  method: 'put',
		  url: this.endpoint(endpoint),
		  headers: this.headers(),
		  'data': data
		}).then(res => {
			console.log("Success");
			console.log(res.data.results);
			return res.data.results;
		}).catch(error => {
			console.log("Error");
            console.log(error);
        });
	}

	delete(endpoint, data){
		return axios({
		  method: 'delete',
		  url: this.endpoint(endpoint),
		  headers: this.headers(),
		  'data': data
		}).catch(err => {
			this.handleUnauthorizedAccess(err);			
		});
	}

	stock(endpoint, params = {}){		
        return axios({
		  method: 'get',
		  url: this.endpoint(endpoint),
		  params: this.encodeUrlParams(params),
		}).then(response => {
			return response.data
		}).catch(error => {
            console.log(error);
        });
	}

	get(endpoint, params = {}){
		return axios({
		  method: 'get',
		  url: this.endpoint(endpoint),
		  headers: this.headers(),
		  params: this.encodeUrlParams(params),
		}).then(res => {
			console.log("Success");
			console.log(res.data.results);
			return res.data.results;
		}).catch(err => {
			console.log("Error");
		});
	}

	post(endpoint, data = {}){
		return axios({
			method: 'post',
			url: this.endpoint(endpoint),
			headers: this.headers(),
			data: data
		});
	}

	getNoAuth(endpoint, params = {}){
		return axios({
			method: 'get',
			url: this.endpoint(endpoint),
			params: this.encodeUrlParams(params),
		}).then(res => {
			return res.data;
		});
	}

	postNoAuth(endpoint, data = {}){
        return axios({
			method: 'post',
			url: this.endpoint(endpoint),
			data: data
		});
	}

	handleUnauthorizedAccess(err){
		if(err.response.status == 401){
			localStorage.clear();
			basic.redirectToLoginPage();
		}
	}
}

export const request = new Request;