import { basic, manipulation } from './';

class Role {

	init(){
		
		this.auth = manipulation.getShared('auth');
		this.caps = this.auth.caps? this.auth.caps: [];
	}

	cap(cap){
		this.init();
		
		if(this.caps.indexOf(cap) !== -1){
			//has capability
			return true;
		}

		return false;

	}

	isset(){
		this.init();
		return basic.objectSize(this.auth);
	}

}

export const role = new Role;