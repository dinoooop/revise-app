export const appData = {
	landingPage: '/departments',
	loginPage: '/login',
	tokenExpiryTime: 60, // miniutes

	authPublicPages: ['/login', '/register'],

	urlParams: {
		sortOrder: 'so',
		sortBy: 'sb',
	},
	alerts: {
		delete: {
			title: 'Confirm Delete',
	      	message: 'Are you sure delete this item',
		},
		deleteBulk: {
			title: 'Confirm Delete',
	      	message: 'Are you sure delete these items',
		},
		noItemsSelected: {
			title: 'Alert',
	      	message: 'No items selected!',
		}
	}
}