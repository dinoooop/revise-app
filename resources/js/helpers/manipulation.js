import store from '../store/store';
import { basic } from './';

export class manipulation {

	static getShared(param){
		const shared = store.getState().shared;
		if(param){
			if(typeof shared[param] !== 'undefined'){
				return shared[param];
			}else{
				console.error(`shared attr ${param} not exist`);
			}
		} else {
			return shared;
		}
	}

	static getCurrentPage(){
		return this.getShared('currentPage');
	}

	static getSortValues(column){
		let sortValues = this.getShared('sortValues');

		if(sortValues[column]){
			sortValues[column] = (sortValues[column]) ? 0 : 1
		} else {
			sortValues[column] = 1;
		}

		return sortValues;

	}

	static getSortClass(column){
		const sortValues = this.getShared('sortValues');

		if(typeof sortValues[column] !== 'undefined'){
			if(sortValues[column] == 1){
				return 'sort-up';
			} else {
				return 'sort-down';
			}
		}

		return 'sort-dot';

	}

	static getSelectedItems(id){
		const selectedItems = this.getShared('selectedItems');
		return (id)? basic.toggleArrayItem(selectedItems, id) : selectedItems;
	}

		

	static setPageLoader(dispatch, show){
		if(typeof show == 'undefined'){
			const loader = this.getShared('loader');
			dispatch({type: 'loader', payload: !loader});
		} else {
			dispatch({type: 'loader', payload: show});
		}
		
	}

	static getIdsFromItems(){
		const items = this.getShared('items');
		let results = [];
		for (let index in items) {
			if(items[index]['id']) results.push(items[index]['id']);
    	}
    	return results;
	}

	static refineSelectedIds(){
		let ids = this.getShared('selectedItems');
		return basic.arrDiff(ids, ['all']);
	}

	static addFieldToFields(field){
		let fields = this.getShared('fields');
		fields[field.name] = field;
		return fields;
	}

	static addFieldValueToFields(key, value){
		let fields = this.getShared('fields');

		if(typeof fields[key] == "undefined"){
			let newField = {
				name: key,
				type: 'hidden',
				value: value,
				toSubmit: true,
			};

			fields[key] = newField;

		} else {
			fields[key]['value'] = value;
		}
		
		return fields;
	}

	static removeField(removekey){

		let fields = this.getShared('fields');
		let results = {};

		for (let key in fields) {
		    if (!fields.hasOwnProperty(key)) continue;
			
			if(key != removekey){
				results[key] = fields[key];
			}
		}
		return results;
		
	}

	static deleteItem(id){
		let items = this.getShared('items');
		return items.filter(item => {
            return item.id !== id;
        });
	}

	


}