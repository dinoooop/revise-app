import { basic, manipulation } from './';

class Validator {

	/**
	*
	* While input on change space will not show if trim is true
	* Trim true require true only at form submit
	* formName requires only when validate multiple form from same page
	*/
	validate(fields, trim = true, formName = false){

		let isSingleField = false;

		if(typeof fields.type != 'undefined'){
			isSingleField = true;
		}

		this.trim = trim;

		if(isSingleField){
			// validation code for single field
			this.field = fields;
			this.init();
			return this.field;

		} else {
			// validation code for multiple fields
			if(formName){
				this.fields = this.getFieldsOfForm(fields, formName);
			} else {
				this.fields = fields;
			}


			for(let key in this.fields){
				this.field = this.fields[key];
				this.init();
			}

		}

		return this.fields;

	}

	init(){
		this.setValues();
		this.field.error = this.anyError();
		
	}

	getFieldsOfForm(fields, formName){
		let target = {};

		for (let key in fields) {
		    if (!fields.hasOwnProperty(key)) continue;

		    if(fields[key]['formName'] == formName){

		    	target[key] = fields[key];
		    	
		    }
		}

		return target;
	}

	// trim the values, if select set default, etc
	setValues(){
		
		// trim all strings

		if(
			this.trim
			&& typeof this.field.value == 'string'
			&& this.field.type !== 'date'
		){
			this.field.value = this.field.value.trim();
		}

	}

	anyError(){
		let {rules, value} = this.field;
		let error = "";

		if(value == null){
			value = '';
		}

		if(this.field.type == "select-auto") {
			error = this.getSelectAutoExtraError();
		} else if(rules && rules.required && value.length == 0) {
			error =  "Field is required";
        } else if (rules && value != '' && rules.min && rules.min > value.length){
            error = "Minimum " + rules.min + " charecters required";
        } else if (rules && rules.max && rules.max < value.length) {
            error = "Maximum " + rules.max + " charecters are allowed";
        } else if (this.field.type == "email" && !this.isValidEmail(value)) {
        	error = "Invalid email";
        } else if (this.field.name == "confirm_password" && !this.isPasswordMatch()) {
        	error = "Confirm password not matching";
        } 

        return error;
	}

	getSelectAutoExtraError(){
		let error = '';

		if(
			this.field.rules 
			&& this.field.rules.required 
			&& this.field.displayValue == ''
		){
			error =  "Field is required";
		}else if(this.field.displayValue !== '' && !this.field.createNewOption){
			if(
				this.field.displayValue.length > 1
				&& this.field.countOption == 0
			){
				error =  "Invalid input";
			}
			
		}

		return error;
	}

	isPasswordMatch(){
		const fields = manipulation.getShared('fields');
		return fields.password && fields.password.value === this.field.value;
	}

	isValidEmail(email){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(email) == false) {
            return false;
        }

        return true;
	}

	checkSelect(){
		let {rules, value} = this.field;
		let error = "";
		if(rules.required && value.length == 0) {
			if(this.field.default !== ''){
				this.field.value
			}
			error =  "Field is required";
		}
	}

	// check any errors in whole fields
	hasErrors(){
		let error = false;
		for(let key in this.fields){
			if (!this.fields.hasOwnProperty(key)) continue;

			if(this.fields[key]['error'] && this.fields[key]['error'] != ''){
				error = true;
			}
		}
		return error;
	}

	skipTypeFromValidation(fields){

		if(typeof fields.type != 'undefined'){

			return fields;

		}

		let target = {};

		for (let key in fields) {
			
		    if (!fields.hasOwnProperty(key)) continue;

			if(typeof fields[key]['type'] != "undefined"){
	    		target[key] = fields[key];
	    	}		    
		}

		return target;

	}

	
}

export const validator = new Validator;