import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { appData, role } from './';

export class basic {

	static objectSize(obj){
	    var size = 0, key;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) size++;
	    }

	    return size;
	}

	static arrDiff (a1, a2) {

	    var a = [], diff = [];

	    return a1.filter(item => {
	    	var index = a2.indexOf(item);
			if (index !== -1) {
			  // found item in a2
			  return false;
			}

			return true;

	    });

	   
	}

	static removeItemFromArray(array, item) {
		
		var index = array.indexOf(item);
		if (index > -1) {
		  array.splice(index, 1);
		}
		
		return array;
	}

	static toggleArrayItem(array, item) {

		if(isNaN(item) === false){
			// the item is number
			item = parseInt(item);

		}

		var index = array.indexOf(item);
		array = array.length == 0? [] : array;
		
		if (index > -1) {
		  	array.splice(index, 1);
		}else{
			array.push(item);
		}
		return array;
	}


	static cloneObj(src) {
	  let target = {};
	  for (let prop in src) {
	    if (src.hasOwnProperty(prop)) {
	      target[prop] = src[prop];
	    }
	  }
	  return target;
	}


	static displayName(obj){

		if(!obj){
			return '';
		}

		if(obj.first_name && obj.last_name){
			return obj.first_name + ' ' + obj.last_name;
		}

		return '';
	}

	
	static labelValueConvert(array, label, value) {

		let results = [];
		for (var key in array) {
			let row = {};
			row['label'] = array[key][label];
            row['value'] = array[key][value];
            results.push(row);
		}

		return results;

	}

	// Convert ['hi', 'hello'] to 
	// [{lable: 'hi', value:'hi'}, {lable: 'hello', value:'hello'}]
	static labelValueOneArray(array){
		let results = [];

		for (var key in array) {
			let row = {};
			row['label'] = array[key];
            row['value'] = array[key];
            results.push(row);
		}

		return results;
	}



	// Convert following to label value pairs
	// { 'vocal': 1, 'text': 2 }

	static labelValueConvertObj(Obj) {

		let results = [];
		for (var key in Obj) {
			let row = {};
			row['label'] = key;
            row['value'] = Obj[key];
            results.push(row);
		}

		return results;

	}

	static confirm(alertType, yes, no){
		const alert = appData.alerts[alertType];
		confirmAlert({
	      title: alert.title,
	      message: alert.message,
	      buttons: [
	        {
	          label: 'Yes',
	          onClick: () => yes()
	        },
	        {
	          label: 'No',
	          onClick: () => no? no(): {}
	        }
	      ]
	    });
	}

	static alert(alertType, yes){
		const alert = appData.alerts[alertType];
		confirmAlert({
	      title: alert.title,
	      message: alert.message,
	      buttons: [
	        {
	          label: 'OK',
	          onClick: () => yes? yes(): {}
	        }
	      ]
	    });
	}

	static redirectToLandingPage(){
	    window.location.href = appData.landingPage;
	}

	static isPublicPage(){
		return appData.publicPages.indexOf(window.location.pathname) !== -1;
	}

	static redirectToLoginPage(){
		if(window.location.pathname !== '/login'){
	    	window.location.href = appData.loginPage;		
		}
	}

	static isAuthPage(){
		return appData.authPages.indexOf(window.location.pathname) !== -1;
	}

	static isAuthPublicPage(){
		return appData.authPublicPages.indexOf(window.location.pathname) !== -1;
	}

	static hasToken(){
		const keeplogin = parseInt(localStorage.getItem('keeplogin'));
		const currentTime = Date.now();

		if(!localStorage.getItem('lastTokenTime')){
			localStorage.setItem('lastTokenTime', currentTime);
			return localStorage.getItem('token');
		} else if(keeplogin == 1){
			return localStorage.getItem('token');
		} else {
			const lastTokenTime = parseInt(localStorage.getItem('lastTokenTime'));
			const diffTime = parseInt((currentTime - lastTokenTime)/1000);
			const thresholdTime = 60 * appData.tokenExpiryTime;

			if(diffTime > thresholdTime){
				localStorage.clear();
				return null;
			}else{
				localStorage.setItem('lastTokenTime', currentTime);
				return localStorage.getItem('token');
			}
		}
	}

	static setDataSourceForCaps(dataSource){

		dataSource.dmSet = true;

		if(dataSource.createNew){			
			dataSource.createNew = role.cap(dataSource.manage);
		}

		// Modify indexTableProps for caps

		let indexTableProps;

		if(typeof dataSource.indexTableProps == "undefined"){
			return dataSource;
		} else {
			indexTableProps = dataSource.indexTableProps;
		}

		indexTableProps.columnCount = indexTableProps.columns.length;

		if(indexTableProps.bulkAction){
			indexTableProps.columnCount++;
			indexTableProps.bulkAction = role.cap(dataSource.manage);
		}

		if(indexTableProps.actions){
			indexTableProps.columnCount++;
			indexTableProps.actions = role.cap(dataSource.manage);
		}

		
		return dataSource;

	}


	static getStatusId(status){
		let results = appData.status.filter(item => {
			return item.value == status;
		});

		return results[0] ? results[0].id : null;
	}

	static selectFromArrayObject(array, name, value){
		let results = array.filter(item => {
			return item[name] == value;
		});

		return results[0] ? results[0] : null;
	}

	static updateObjInArray(array, name, value, newItem){
		for(let key in array){
			if(array[key][name] == value){
				array[key] = newItem;
				break;
			}
		}

		return array;
	}

	static isImage(param){

		if(typeof  param == "undefined"){
			return false;

		}
		
		const imgExt = ['.jpg', '.png', '.jpeg'];

		for(let key in imgExt){
			if(param.indexOf(imgExt[key]) != -1){
				return true;
			}
		}

		return false;
	}

	static isObject(object) {
	  return typeof object === 'object' && object !== null;
	}
}