import React from 'react';
import { Link } from "react-router-dom";
import { basic, defaultFields, appData } from './';

export class display {

	static columnValue(name, item, namespace) {

		switch(name){
			case 'text':
				return item.name;
				break;

			case 'name':
				return <Link to={`${namespace}/show/${item.id}`} >{item.name}</Link>;
				break;

			case 'full_name':
				return basic.displayName(item);

			case 'designation_id':
				return item.designation ? item.designation.name : '';

			case 'department_id':
				return item.department ? item.department.name : '';

			case 'status':
				return this.status(item.status);

			case 'head_id':
				return item.head ? basic.displayName(item.head) : '';

			default:
				return item[name]? item[name]: '';

		}

	}


	static navClass(navlink){

		if(window.location.pathname == navlink){
			return 'nav-item active';
		}

		return 'nav-item';
	}

	static status(id){
		return appData.status[id]? appData.status[id]['label']: id;
	}

}
