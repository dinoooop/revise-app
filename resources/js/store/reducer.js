import { combineReducers } from 'redux';
import sharedReducer from './sharedReducer';

export default combineReducers({
  shared: sharedReducer
});