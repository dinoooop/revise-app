import {basic} from "../helpers";

const initialState = {
    fields: {},
    toSubmit: {},
    response: {},
    formError: {},
    dataSource: {},
    items: [],
    pageCount: 0,
    currentPage: 1,
    sortValues: {},
    loader: false,
    noItemMsg: '',
    selectedItems: [],
    auth: {},
};


export default function (state = initialState, action) {

    const { payload, type } = action;
    let holder = {};

    if(typeof state[type] !== "undefined"){

        if(Array.isArray(payload)){
            holder[type] = [].concat(payload);
            return {...state, ...holder};
        } else if(typeof payload === 'object') {
            holder[type] = Object.assign({}, payload);
            return {...state, ...holder};
        } else {
            holder[type] = payload;
            return {...state, ...holder};
        }

        
    }

    if(type.indexOf('@') == -1){
        console.error(`Action type ${type} not found`);
    }

    return state;

}