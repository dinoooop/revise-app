import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import FormTester from './screens/formTester/FormTester';
import Tag from './screens/tags/Tag';
import Source from './screens/sources/Source';
import Subject from './screens/subjects/Subject';
import Card from './screens/cards/Card';
import Review from './screens/review/Review';
import SMC from './screens/smc/SMC';
import Profile from './screens/profiles/Profile';
import Classify from './screens/classify/Classify';
import Setting from './screens/settings/Setting';
import Performance from './screens/performances/Performance';
import Statistics from './screens/generals/Statistics';
import ReviewHistoryView from './screens/generals/ReviewHistoryView';
import ReviewLeft from './screens/generals/ReviewLeft';
import ClassifyModuleList from './screens/classify/ClassifyModuleList';
import MathsClassify from './screens/generals/MathsClassify';
import OtherLinks from './screens/generals/OtherLinks';

class AppRouter extends Component {

	constructor(props){
		super(props);
		this.state = {
			loginScreen: false
		}
	}

	componentDidMount = () => { }

	render() {
	    return (
			<Router>                    
		    	<Switch>
		    		<Route path='/form-tester' component={FormTester} />
		    		<Route path='/subjects' component={Subject} />
		    		<Route path='/cards' component={Card} />
		    		<Route path='/tags' component={Tag} />
		    		<Route path='/sources' component={Source} />
		    		<Route path='/smc' component={SMC} />
		    		<Route path='/profiles' component={Profile} />
		    		<Route path='/review/:review_mode' component={Review} />		    		
		    		<Route path='/classify/:module' component={Classify} />
		    		<Route path='/classify' component={ClassifyModuleList} />
		    		<Route path='/settings' component={Setting} />
		    		<Route path='/review-left' component={ReviewLeft} />
		    		<Route path='/performances' component={Performance} />
		    		<Route path='/statistics' component={Statistics} />
		    		<Route path='/review-history' component={ReviewHistoryView} />
		    		<Route path='/maths-classify' component={MathsClassify} />
		    		<Route path='/others' component={OtherLinks} />
		    		<Route path='/' component={Profile} />
			    </Switch>
			</Router>
	    );
	}
}

export default connect(state => ({
    
}), {
	
})(AppRouter);