import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { display } from "../../helpers";

export const ListNav = props => (
	<li className={display.navClass(props.href)}>
        <Link className="nav-link" to={props.href}>
            <i className={props.icon}></i>
            <span className="menu-title">{props.title}</span>
        </Link>
    </li>
);
    
