import React, { Component } from 'react';
import { appRouterContext } from '../../helpers';

export default class TopNav extends Component {
    
    static contextType = appRouterContext;

    render() {
        return (
            <nav className="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div className="navbar-brand-wrapper d-flex justify-content-center">
                    <div className="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
                        <a className="navbar-brand brand-logo" href="/">REVAPP</a>
                        <a className="navbar-brand brand-logo-mini" href="index.html">E</a>
                        
                        <button className="navbar-toggler align-self-center" type="button" onClick={this.props.onClickToggler}>
                            <span className="fas fa-bars"></span>
                        </button>
                    </div>
                </div>

                <div className="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                    <ul className="navbar-nav navbar-nav-right">
                        
                        <li className="nav-item dropdown mr-4">
                            <a className="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center notification-dropdown"
                                id="notificationDropdown" href="#" data-toggle="dropdown">
                                <i className="fas fa-poll"></i>
                                <span className="count"></span>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right navbar-dropdown"
                                aria-labelledby="notificationDropdown">
                                <p className="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                                <a className="dropdown-item">
                                    <div className="item-thumbnail">
                                        <div className="item-icon bg-success">
                                            <i className="mdi mdi-information mx-0"></i>
                                        </div>
                                    </div>
                                    <div className="item-content">
                                        <h6 className="font-weight-normal">Application Error</h6>
                                        <p className="font-weight-light small-text mb-0 text-muted">Just now</p>
                                    </div>
                                </a>
                                <a className="dropdown-item">
                                    <div className="item-thumbnail">
                                        <div className="item-icon bg-warning">
                                            <i className="mdi mdi-settings mx-0"></i>
                                        </div>
                                    </div>
                                    <div className="item-content">
                                        <h6 className="font-weight-normal">Settings</h6>
                                        <p className="font-weight-light small-text mb-0 text-muted">Private message</p>
                                    </div>
                                </a>
                                <a className="dropdown-item">
                                    <div className="item-thumbnail">
                                        <div className="item-icon bg-info">
                                            <i className="mdi mdi-account-box mx-0"></i>
                                        </div>
                                    </div>
                                    <div className="item-content">
                                        <h6 className="font-weight-normal">New user registration</h6>
                                        <p className="font-weight-light small-text mb-0 text-muted">2 days ago</p>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li className="nav-item nav-profile dropdown">
                            <a className="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                                <img src="/images/profile-pic.png" alt="profile" />
                                <span className="nav-profile-name">Dinoop</span>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                                <a className="dropdown-item">
                                    <i className="fas fa-tools text-primary"></i>
                                    Settings
                                </a>
                                <a className="dropdown-item">
                                    <i className="fas fa-sign-out-alt text-primary"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                    <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                        onClick={this.props.onClickToggler}>
                        <span className="fas fa-bars"></span>
                    </button>
                </div>
            </nav>
        );
    }
}