import React, { Component } from 'react';
import { ListNav } from './Mini';
import { role } from '../../helpers';

export default class SideNav extends Component {

    render() {
        return (
            <nav className={this.props.sideNavDisplayClass} id="sidebar">
                <ul className="nav">
                    <ListNav title="Profiles" icon="fas fa-user-tie" href="/profiles" />
                    <ListNav title="Left" icon="fas fa-arrow-circle-left" href="/review-left" />
                    <ListNav title="Review FY" icon="fas fa-marker" href="/review/FY" />
                    <ListNav title="Review RT" icon="fas fa-pencil-alt" href="/review/RT" />
                    <ListNav title="Review NC" icon="fas fa-pizza-slice" href="/review/NC" />
                    <ListNav title="Review History" icon="fas fa-history" href="/review-history" />
                    <ListNav title="Statistics" icon="fas fa-chart-pie" href="/statistics" />
                    <ListNav title="Performance (All)" icon="fas fa-running" href="/performances" />
                    
                    <ListNav title="Cards" icon="fas fa-credit-card" href="/cards" />
                    
                    {/*<ListNav title="Form Tester" icon="fas fa-building" href="/form-tester" />*/}

                    <ListNav title="Others" icon="fas fa-mug-hot" href="/others" />
                </ul>
            </nav>
        );
    }
}