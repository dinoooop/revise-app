import React, { Component } from 'react';
import { Route, Link } from "react-router-dom";
import Normal from '../layouts/Normal';
import { appData } from '../../helpers';

export default function PageForbidden(props){        
    return (
        <Normal>
            <div className="forbidden">
            	<h1>403.</h1>
            	<h3>Forbidden!</h3>
            	<p>Go back to <Link to={appData.landingPage}>home</Link></p>
            </div>
        </Normal>
    );
}