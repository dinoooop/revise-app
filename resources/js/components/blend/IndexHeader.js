import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import Element from '../formFields/Element';
import { role } from '../../helpers';

class IndexHeader extends Component {
	
	render(){
		return(
			<div className="table-main-actions">
            	<h3>{this.props.heading}</h3>
              	{
	              	this.props.dataSource.createNew &&
	            	<Link to={this.props.newButtonUrl} className="btn btn-primary mr-2 new">New</Link>
          		}
            </div>
		);
	}	
}


export default connect(state => ({
  fields: state.shared.fields,
  dataSource: state.shared.dataSource,
}), {})(IndexHeader);