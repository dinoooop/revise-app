import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner';

class PageLoader extends Component {
  componentDidMount(){  
    
  }

  render() {
  
    return(
      <div>
      {
        this.props.loader
        &&
        <div className="page-loader">
          <Loader 
            type="Triangle"
            color="#00BFFF"
            height="100" 
            width="100"
            />
        </div>
      }
      </div>
    );      
  }
}
export default connect(state => ({
    loader: state.shared.loader,
}), {})(PageLoader);