import React, { Component } from 'react';
import { Route, Link } from "react-router-dom";
import Normal from '../layouts/Normal';
import { appData } from '../../helpers';

export default function Page404(props){        
    return (
        <Normal>
            <div className="forbidden">
            	<h1>404.</h1>
            	<h3>Not Found!</h3>
            	<p>Go back to <Link to={appData.landingPage}>home</Link></p>
            </div>
        </Normal>
    );
}