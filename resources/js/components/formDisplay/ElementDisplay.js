import React, { Component } from 'react';
import {InputDisplay, SelectAutoDisplay} from './Mini';

export default class ElementDisplay extends Component {

    render() {
        const { field } = this.props;
        
        return (
            <div>
                { field && field.type == 'text' && <InputDisplay field={field} /> }
                { field && field.type == 'select-auto' && <SelectAutoDisplay field={field} /> }
            </div>
        );
    }
    
}
