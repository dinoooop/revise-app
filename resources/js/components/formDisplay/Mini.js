import React from 'react';

export const InputDisplay = (props) => (
	<div className="row show-box">
        <div className="col-sm label">{props.field.label}</div>
        <div className="col-sm">: {props.field.value}</div>
    </div>
);

export const SelectAutoDisplay = (props) => (
	<div className="row show-box">
        <div className="col-sm label">{props.field.label}</div>
        <div className="col-sm">: {props.field.displayValue}</div>
    </div>
);