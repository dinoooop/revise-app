import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class LabelValueTable extends Component {

 	render() {
    const {items} = this.props;
		const tableBody = items.map(item => {
  		return(
  			<tr key={item.label}>
          <td>{item.label}</td>
          <td>{item.value}</td>
        </tr>
  		);
  	});

    return (
		    
  		<div className="table-responsive">
          <table className="table table-striped">
            <tbody>
              { tableBody }
            </tbody>
          </table>
      </div>
	      	
    );
	}
}
