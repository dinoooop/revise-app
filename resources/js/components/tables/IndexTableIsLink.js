import React, { Component } from 'react';

export default class IndexTableIsLink extends Component {

    handleOnClick = e => {
        this.props.handleOnClick();
    }

    render() {

        const {condition, trueLabel, falseLabel} = this.props
        const label = condition ? trueLabel : falseLabel; 
        const buttonClassName = condition ?  'btn-success' : 'btn-outline-success'; 
               
        return (
            <React.Fragment>
                <button type="button" onClick={this.handleOnClick} className={'btn btn-fw ' + buttonClassName}>{label}</button>
            </React.Fragment>
        );

    }

}
