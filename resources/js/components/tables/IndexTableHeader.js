import React, { Component } from 'react';
import { connect } from 'react-redux';
import { manipulation } from '../../helpers';
import CheckboxTable from '../formFields/CheckboxTable';
import { pageContext, role } from '../../helpers';

class IndexTableHeader extends Component {

  static contextType = pageContext;

  render() {
  	const { indexTableProps } = this.props.dataSource;

  	const tableHeader = indexTableProps.columns.map((item, key) => {      
      
        return(
          <React.Fragment key={key}>
            {
              item.sort ?
              <th  onClick={() => this.context.handleSorting(item.name)} className={manipulation.getSortClass(item.name)}>{item.label}</th>
              :
              <th >{item.label}</th>
            }
          </React.Fragment>
        );
      
    });

    return (
      	<tr>
	      { indexTableProps.bulkAction && <th className="th-checkbox-table"><CheckboxTable value="all" /></th> }
	      { tableHeader }
	      { indexTableProps.actions && <th>Actions</th> }
	    </tr>
    );
  }
}

export default connect(state => ({
    dataSource: state.shared.dataSource,
    sortValues: state.shared.sortValues,
}), {})(IndexTableHeader);