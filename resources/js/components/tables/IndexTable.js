import React, { Component } from 'react';
import { connect } from 'react-redux';
import 'react-confirm-alert/src/react-confirm-alert.css';
import Element from '../formFields/Element';
import IndexTableHeader from './IndexTableHeader';
import IndexTableBody from './IndexTableBody';
import { pageContext, role } from '../../helpers';

class IndexTable extends Component {

  static contextType = pageContext;

  render() {

    const { fields } = this.props;
    const { indexTableProps } = this.props.dataSource;
    
    return (
      <React.Fragment>

        <div className="row">
          <div className="col-md">
              {
                indexTableProps.bulkAction &&
                <Element field={fields.bulkaction} onChange={(e) => this.context.handleBulkaction(e)} />
              }
          </div>
          <div className="col-md"></div>
          <div className="col-md">
              {
                indexTableProps.search &&
                <div className="search">
                  <Element field={fields.search} onChange={(e) => this.context.handleSearchItem(e)} />
                </div>
              }
              
          </div>
        </div>

        <div className="table-responsive">
            <table className="table table-striped">
              <thead>
                <IndexTableHeader />
              </thead>
              <tbody>
                <IndexTableBody />
              </tbody>
            </table>
        </div>
        
      </React.Fragment>
    );
  }
}

export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {})(IndexTable);
