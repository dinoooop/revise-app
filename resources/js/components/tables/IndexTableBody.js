import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { display, pageContext, role } from '../../helpers';
import CheckboxTable from '../formFields/CheckboxTable';
import IndexTableIsLink from './IndexTableIsLink';

class IndexTableBody extends Component {

  static contextType = pageContext;
  
  render() {
  	const { items, fields } = this.props;
    const { indexTableProps, namespace } = this.props.dataSource;

  	const body = items.map(item => {  
        
        return(
          <tr key={item.id}>
            {
              indexTableProps.bulkAction
              && <td><CheckboxTable value={item.id} /></td>
            }
            
            {
              indexTableProps.columns.map(column => {
                return(
                  <td key={column.name}>{display.columnValue(column.name, item, namespace)}</td>
                );
              })
            }
            {
              indexTableProps.actions &&
              <td>
                {
                  indexTableProps.availActions.source_copy &&
                  item.source.id &&
                  <i onClick={() => this.context.sourceSwitch(item.id, item.source.id)}>
                    {item.source.name}
                  </i>
                }
                {
                  indexTableProps.availActions.edit &&
                  <Link to={`${namespace}/edit/${item.id}`} ><i className="fas fa-edit"></i></Link>
                }
                {
                  indexTableProps.availActions.deleteItem &&
                  <i className="fas fa-trash-alt button-delete" onClick={() => this.context.handleDelete(item.id)}></i>
                }
                {
                  indexTableProps.availActions.activate &&                  
                  <IndexTableIsLink condition={item.is_active} trueLabel="Activated" falseLabel="Activate" handleOnClick={() => this.context.handleOnActivate(item.id)}/>
                }
                
              </td>
            }
          </tr>
        );
      
    });

    return (
      	<React.Fragment>
        
            {
              this.props.noItemMsg 
              ? 
              <tr>
                <td colSpan={indexTableProps.columnCount}>{this.props.noItemMsg}</td>
              </tr>
              :
              body
            }
        </React.Fragment>
    );
  }
}

export default connect(state => ({
    items: state.shared.items,
    noItemMsg: state.shared.noItemMsg,
    dataSource: state.shared.dataSource,
}), {})(IndexTableBody);