import React, { Component } from 'react';
import { connect } from 'react-redux';
import { basic } from '../../helpers';

class ListLink extends Component {
	state = {
		current: 0
	}

	handleClick = id => {
		this.setState({classNameActive: "red"});
		this.props.onClick(id);
	}

 	render() {

  		const listBody = this.props.items.map(item => {
	  		return(
	  			<li key={item.id} className={this.state.classNameActive} onClick={() => this.handleClick(item.id)}>{item.name}</li>
	  		);
	  	});

	    return (
		    
      		<ul className="list-group">
				{listBody}
			</ul>
	      	
	    );
  	}
}

export default connect(state => ({
    fields: state.shared.fields,
}), {})(ListLink);