import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator } from '../../helpers';
import {Label} from './Mini';
import sharedAction from '../../actions/sharedAction';

const actions = new sharedAction;

class FileUpload extends Component {

    name = this.props.inField['name'];
    field = {};

    handleOnChange = e => {
        let file = e.target.files[0];
        console.log(file);
        this.field.value = file;
        this.field = validator.validate(this.field, false);
        this.props.setOneField(this.field);
        if(this.field.onChange){
            this.props.onChangeElement(e);
        }
    }

    render() {

        this.field = this.props.fields[this.name];
        const error = this.field.error;
        const errorClass = error ? 'has-danger' : '';
        const value = this.field.value;

        return (
            <div className={'form-group ' + errorClass}>
                {this.field.showLabel && <Label field={this.field} />}
                <input type="file"
                    className="form-control"
                    id={this.field.id}
                    
                    onChange={this.handleOnChange}
                />
                <div className="color-red">{error}</div>
            </div>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), { 
    setOneField: (field) => actions.setOneField(field),
 })(FileUpload);
