import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator, basic } from '../../helpers';
import {Label} from './Mini';
import sharedAction from '../../actions/sharedAction';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const actions = new sharedAction;

class DateInput extends Component {

    name = this.props.inField['name'];
    field = {};

    handleOnChange = date => {
        this.field.value = date;
        this.field.changed = true;
        this.field = validator.validate(this.field);
        this.props.setOneField(this.field);
        if(this.field.onChange){
            this.props.onChangeElement(e);
        }
    }

    render() {

        this.field = this.props.fields[this.name];
        const error = this.field.error;
        const errorClass = error ? 'has-danger' : '';

        return (
            <div className={'form-group ' + errorClass}>
                <Label field={this.field} />

                <div>
                    <DatePicker
                        className="form-control"
                        selected={this.field.value}
                        onChange={this.handleOnChange}
                    />
                </div>
                <div className="color-red">{error}</div>
            </div> 
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), { 
    setOneField: (field) => actions.setOneField(field),
})(DateInput);