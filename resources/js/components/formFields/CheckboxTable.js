import React, { Component } from 'react';
import { connect } from 'react-redux';
import sharedAction from '../../actions/sharedAction';

const actions = new sharedAction;

class CheckboxTable extends Component {

    handleOnChange = e => {
        const value = e.target.value;
        if(value == 'all'){
            this.props.setAllItemsSelected();
        } else {
            this.props.setSelectedItems(e.target.value);
        }
    }

    render() {
        
        const checked = (this.props.selectedItems.indexOf(this.props.value) !== -1) ? 'checked' : '';
        const {value} = this.props;
    
        return (
            <div className='checkbox checkbox-table'>
                <label className="form-check-label-single">
                    <input type="checkbox" className="form-check-input" value={value} onChange={this.handleOnChange} checked={checked} />
                    <span className="cr"><i className="cr-icon fas fa-check-circle"></i></span>
                </label>
            </div>
        );

    }

}

export default connect(state => ({
    selectedItems: state.shared.selectedItems,
}), {
  setSelectedItems: id => actions.setSelectedItems(id),
  setAllItemsSelected: () => actions.setAllItemsSelected(),
})(CheckboxTable);