import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator } from '../../helpers';
import { Label } from './Mini';
import sharedAction from '../../actions/sharedAction';
import stockService from '../../services/stockService';

const actions = new sharedAction;


class Select extends Component {

    name = this.props.inField['name'];
    field = {};

    componentDidMount(){
        if(!this.field.options || this.field.options.length == 0){
            stockService[this.field.stockOption]()
                .then(response => {
                    console.log("Elixer", response.results);
                    this.field.options = response.results;
                    this.props.setOneField(this.field);
                });
        }
    }

    handleOnChange = e => {
        this.field.value = e.target.value;
        this.field.changed = true;
        this.field = validator.validate(this.field);
        this.props.setOneField(this.field);
        if(this.field.onChange){
            this.props.onChangeElement(e);
        }
    }

    render() {

        
        this.field = this.props.fields[this.name];
        
        let {error, value} = this.field;
        const errorClass = error ? 'has-danger' : '';
        value = (value == "" && !this.field.changed) ? this.field.default : value;
        
        const options = this.field.options.map(option => {
            return <option key={option.value}  value={option.value}>{option.label}</option>
        });

        return (
            <div className={'form-group ' + errorClass}>
                
                {this.field.showLabel && <Label field={this.field} />}
                
                <select 
                    className="form-control" 
                    id={this.field.id}
                    onChange={this.handleOnChange}
                    value={value}
                >
                  {!!this.field.nullOption && <option value="">--select--</option>}
                  {options}
                </select>
                <div className="color-red">{error}</div>
            </div>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), {
  setOneField: (field) => actions.setOneField(field),
})(Select);