import React, { Component } from 'react';

export const Label = (props) => (
	<label htmlFor={props.field.id}>
        {props.field.label} 
        { 
            props.field.rules 
            && props.field.rules.required 
            && <span className="color-red"> &lowast; </span>
        }
    </label>
);

export const DisplayValue = (props) => (
	<div className="form-group">
		<label htmlFor={props.inField.id}>{props.inField.label}</label>
		<span>: {props.inField.value}</span>
	</div>
);