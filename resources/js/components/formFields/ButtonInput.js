import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator, basic } from '../../helpers';
import {Label} from './Mini';
import sharedAction from '../../actions/sharedAction';

const actions = new sharedAction;

class ButtonInput extends Component {

    name = this.props.inField['name'];
    field = {};

    handleOnClick = e => {
        //console.log(this.field.value);
        this.fieldhidden = this.props.fields[this.field.hiddenName];
        this.fieldhidden.value = this.field.value;
        this.fieldhidden = validator.validate(this.fieldhidden);
        this.props.setOneField(this.fieldhidden);
        if(this.field.onChange){
            this.props.onChangeElement(e);
        }
    }

    render() {

        this.field = this.props.fields[this.name];
        return (
            <div className={'form-group checkbox'}>
                <button onClick={this.handleOnClick} className="btn btn-primary mr-2">{this.field.label}</button>
            </div>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), { 
    setOneField: (field) => actions.setOneField(field),
})(ButtonInput);