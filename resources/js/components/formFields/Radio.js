import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator, basic } from '../../helpers';
import {Label} from './Mini';
import sharedAction from '../../actions/sharedAction';

const actions = new sharedAction;

class Radio extends Component {

    name = this.props.inField['name'];
    field = {};

    handleOnChange = e => {
        if(this.field.value == e.target.value){
            this.field.value = '';
        } else {
            this.field.value = e.target.value;
        }
        this.field.changed = true;
        this.field = validator.validate(this.field);

        this.props.setOneField(this.field);
        if(this.field.onChange){
            this.props.onChangeElement(e);
        }
    }

    render() {

        this.field = this.props.fields[this.name];
        const error = this.field.error;
        const errorClass = error ? 'has-danger' : '';
        
        let checked = "";
        const options = this.field.options.map(option => {
            checked = (option.value == this.field.value) ? 'checked': '';
            return (
                <div key={option.value} className="form-check">
                    <label className="form-check-label">
                        <input type="checkbox" className="form-check-input" value={option.value} onChange={this.handleOnChange}  checked={checked}/>
                        <span className="cr"><i className="cr-icon fas fa-check-circle"></i></span>
                        {option.label}
                    </label>
                </div>
            );
        });

        return (
            <div className={'form-group checkbox ' + errorClass}>
                <Label field={this.field} />
                
                {options}

                <div className="color-red">{error}</div>
            </div>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), { 
    setOneField: (field) => actions.setOneField(field),
})(Radio);