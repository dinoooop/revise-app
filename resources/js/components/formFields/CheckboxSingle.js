import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator } from '../../helpers';
import sharedAction from '../../actions/sharedAction';

const actions = new sharedAction;

class CheckboxSingle extends Component {

    name = this.props.inField['name'];
    field = {};

    handleOnChange = e => {
        this.field.value = e.target.value;
        this.field = validator.validate(this.field);
        this.props.setOneField(this.field);
        if(this.field.onChange){
            this.props.onChangeElement(e);
        }
    }

    render() {

        this.field = this.props.fields[this.name];
        
        const error = this.field.error;
        const errorClass = error? 'has-danger' : '';
        const checked = this.field.value == 1 ? 'checked' : '';
        const value = (this.field.value == 1) ? 0 : 1;
    
        return (
            <div className={'form-group checkbox ' + errorClass}>
                <div className="form-check p-0">
                    <label className="form-check-label form-check-label-single">
                        <input type="checkbox" className="form-check-input" value={value} onChange={this.handleOnChange} checked={checked} />
                        <span className="cr"><i className="cr-icon fas fa-check-circle"></i></span>
                        {this.field.label}
                    </label>
                </div>
                <div className="color-red">{error}</div>
            </div>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), {
  setOneField: (field) => actions.setOneField(field),
})(CheckboxSingle);