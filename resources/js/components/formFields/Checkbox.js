import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator, basic } from '../../helpers';
import {Label} from './Mini';
import sharedAction from '../../actions/sharedAction';

const actions = new sharedAction;

class Checkbox extends Component {

    name = this.props.inField['name'];
    field = {};

    handleOnChange = e => {
        this.field.value = basic.toggleArrayItem(this.field.value, e.target.value);
        this.field = validator.validate(this.field, false);
        this.props.setOneField(this.field);
        console.log('this.field.onChange');
        console.log(this.field.onChange);
        if(this.field.onChange){
            console.log("heee");
            this.props.onChangeElement(e);
        }
    }

    render() {
        this.field = this.props.fields[this.name];
        const error = this.field.error;
        const errorClass = error? 'has-danger' : '';
        
        let checked = "";
        const options = this.field.options.map(option => {
            checked = (this.field.value.indexOf(option.value) !== -1) ? 'checked': '';
            return (
                <div key={option.value} className="form-check">
                    <label className="form-check-label">
                        <input type="checkbox" className="form-check-input" value={option.value} onChange={this.handleOnChange} checked={checked} />
                        <span className="cr"><i className="cr-icon fas fa-check-circle"></i></span>
                        {option.label}
                    </label>
                </div>
                );
        });


        
        return (
            <div className={'form-group checkbox ' + errorClass}>
                <Label field={this.field} />
                  
                {options}
                
                <div className="color-red">{error}</div>
            </div>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), { 
    setOneField: (field) => actions.setOneField(field),
})(Checkbox);