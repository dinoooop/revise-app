import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator } from '../../helpers';
import { Label } from './Mini';
import sharedAction from '../../actions/sharedAction';
import stockService from '../../services/stockService';

const actions = new sharedAction;

class SelectCreate extends Component {

    name = this.props.inField['name'];
    field = {};

    textField = {
        id: this.props.inField.id,
        type: 'text',
        name: 'new_' + this.props.inField.name,
        label: this.props.inField.label,
        value: '',
        rules: this.props.inField.rules,
        error: ""
    };

    state = {
        showTexInput: false,
    };

    componentDidMount(){
        if(!this.field.options || this.field.options.length == 0){
            stockService[this.field.stockOption]()
                .then(response => {
                    console.log("Elixer", response.results);
                    this.field.options = response.results;
                    this.props.setOneField(this.field);
                });
        }
    }

    handleOnChangeInput = e => {
        this.textField.value = e.target.value;
        this.textField.changed = true;
        this.textField = validator.validate(this.textField);
        this.props.setOneField(this.textField);
        if(this.textField.onChange){
            this.props.onChangeElement(e);
        }
    }

    closeInputField = () => {
        this.field.haveTextInput = false;
        this.setState({showTexInput: false});
    }

    handleOnChangeSelect = e => {
        if(e.target.value == "select-create-new"){
            this.field.haveTextInput = true;
            this.props.setOneField(this.field);
            this.setState({showTexInput: true});
        }else{
            this.field.value = e.target.value;
            this.field.changed = true;
            this.field = validator.validate(this.field);
            this.props.setOneField(this.field);
            if(this.field.onChange){
                this.props.onChangeElement(e);
            }
        }
    }

    render() {

        this.field = this.props.fields[this.name];
        const {showTexInput} = this.state;
        
        let error, value = '';
        if(showTexInput){
            error = this.textField.error;
            value = this.textField.value;
        } else {
            error = this.field.error;
            value = this.field.value;
        }
        
        const errorClass = error ? 'has-danger' : '';
                
        const options = this.field.options.map(option => {
            return <option key={option.value}  value={option.value}>{option.label}</option>
        });

        return (
            <div className={'form-group ' + errorClass}>
                <Label field={this.field} />
                
                {
                    showTexInput ?
                     <div className="input-group mb-2">
                     
                        <input type="text"
                            className="form-control"
                            id={this.field.id}
                            placeholder={this.field.label}
                            value={value}
                            onChange={this.handleOnChangeInput}
                        />
                        <div className="input-group-append">
                          <button type="button" className="btn btn-outline-primary" onClick={this.closeInputField}>X</button>
                        </div>
                    </div>
                    :
                    <select 
                        className="form-control" 
                        id={this.field.id}
                        onChange={this.handleOnChangeSelect}
                        value={value}
                    >
                      {!!this.field.nullOption && <option value="">--select--</option>}
                      {options}
                      <option value="select-create-new"> Create New {this.field.label} [+] </option>

                    </select>
                }
                
                <div className="color-red">{error}</div>
            </div>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), {
  setOneField: (field) => actions.setOneField(field),
})(SelectCreate);