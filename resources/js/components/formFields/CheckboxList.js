import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator, basic } from '../../helpers';
import {Label} from './Mini';
import sharedAction from '../../actions/sharedAction';
import stockService from '../../services/stockService';

const actions = new sharedAction;

class CheckboxList extends Component {

    constructor(props){

        super(props);

        this.state = {
          value: '',
          options: [],
        };

        this.name = this.props.inField['name'];
        this.field = {};
    }

    componentDidMount(){
        this.loadOptions();
    }

    handleOnChange = e => {
        console.log(e.target.value);
        this.field.value = basic.toggleArrayItem(this.field.value, e.target.value);
        console.log('value');
        console.log(this.field.value);
        this.field = validator.validate(this.field, false);
        this.props.setOneField(this.field);
        if(this.field.onChange){
            this.props.onChangeElement(e);
        }
    }

    loadOptions = () => {

        stockService[this.field.stockOption]()
            .then(response => {

                console.log('response.results');
                console.log(response.results);
                
                this.setState({
                  options: response.results
                });

                
                
            })
            .catch(error => {
                console.log(error);
            });
        
    };

    render() {
        this.field = this.props.fields[this.name];
        const error = this.field.error;
        const errorClass = error? 'has-danger' : '';
        
        let checked = "";
        const options = this.state.options.map(option => {
            checked = (this.field.value.indexOf(option.value) !== -1) ? 'checked': '';
            return (
                <div key={option.value} className="form-check">
                    <label className="form-check-label">
                        <input type="checkbox" className="form-check-input" value={option.value} onChange={this.handleOnChange} checked={checked} />
                        <span className="cr"><i className="cr-icon fas fa-check-circle"></i></span>
                        {option.label}
                    </label>
                </div>
                );
        });


        
        return (
            <div className={'form-group checkbox ' + errorClass}>
                <Label field={this.field} />
                  
                {options}
                
                <div className="color-red">{error}</div>
            </div>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), { 
    setOneField: (field) => actions.setOneField(field),
})(CheckboxList);