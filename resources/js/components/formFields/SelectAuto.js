import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator } from '../../helpers';
import stockService from '../../services/stockService';
import axios from 'axios';
import Autosuggest from 'react-autosuggest';
import { Label } from './Mini';
import sharedAction from '../../actions/sharedAction';

const actions = new sharedAction;

class SelectAuto extends Component {

    constructor(props){

        super(props);

        this.state = {
          value: '',
          suggestions: [],
        };

        this.name = this.props.inField['name'];
        this.field = {};

        this.newField = {
            name: 'new_' + this.field.name,
            label: this.field.label,
            formName: this.field.formName,
            type: 'text',
            value: '',
            rules: this.field.rules
        };

        if(this.field.formName == 'edit' && this.field.displayValue !== ''){
            this.existingOption = {label: this.field.displayValue, value: this.field.value};
            this.field.countOption = 1;
        }
    }

    componentDidMount(){
        
    }

    // display changes in input
    onChange = (event, { newValue }) => {

       
        this.field.displayValue = newValue;
        this.props.setOneField(this.field);
        
        if(newValue == ''){
            this.field.value = '';
        }

        this.field = validator.validate(this.field);
        this.props.setOneField(this.field);

        if(!this.field.oneDimOption){
            this.newField.value = newValue;
            this.props.setOneField(this.newField);
        }

        if(this.field.onChange){
            this.props.onChangeElement(event);
        }
        
    }

    // when selected 
    getSuggestionValue = suggestion => {
        this.field.value = this.field.oneDimOption ? suggestion : suggestion.value;
        this.field = validator.validate(this.field);
        this.props.setOneField(this.field);
        return this.displayItem(suggestion);
    }

    // request to server
    onSuggestionsFetchRequested = ({ value }) => {

        stockService[this.field.stockOption](value)
            .then(response => {
                
                if(
                    this.existingOption 
                    && this.existingOption
                    .label.toLowerCase()
                    .indexOf(value.toLowerCase()) !== -1
                ){
                    response.results.push(this.existingOption);
                }

                this.setState({
                  suggestions: response.results
                });

                this.field.countOption = response.results.length;

                if(
                    this.field.countOption == 0 
                    && !this.field.createNewOption
                ){
                    // Field is not allow to create new item
                    // Show error item is not from the list

                    this.field = validator.validate(this.field);
                    this.props.setOneField(this.field);

                }

                if(
                    this.field.countOption == 0 
                    && this.field.createNewOption
                ) {

                    // The field allow to create new item

                    if(this.field.oneDimOption){
                        this.field.value = value;
                        this.field = validator.validate(this.field);    
                        this.props.setOneField(this.field);
                    }else{
                        
                        // set other to null
                        this.field.value = '';
                        this.props.setOneField(this.field);
                    }

                }
                
            })
            .catch(error => {
                console.log(error);
            });
        
    };


    onSuggestionsClearRequested = () => {
        this.setState({
          suggestions: []
        });
    };

    displayItem = suggestion => {
        return this.field.oneDimOption ? suggestion : suggestion.label;
    }

    renderSuggestion = suggestion => (
        <div>
            {this.displayItem(suggestion)}
        </div>
    );

    render() {

        this.field = this.props.fields[this.name];

        const { value } = this.state;
        const suggestions = this.state.suggestions ? this.state.suggestions : [];
        let {error} = this.field;
        const errorClass = error ? 'has-danger' : '';
        let displayValue = this.field.displayValue? this.field.displayValue : '';
        
        // Autosuggest will pass through all these props to the input.
        const inputProps = {
          placeholder: this.field.label,
          value: displayValue,
          onChange: this.onChange
        };

        return (
        
            <div className={'form-group select-auto ' + errorClass}>
                <Label field={this.field} />
                <Autosuggest
                    suggestions={suggestions}
                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                    getSuggestionValue={this.getSuggestionValue}
                    renderSuggestion={this.renderSuggestion}
                    inputProps={inputProps}
                />
                <div className="color-red">{error}</div>
            </div>
        
        );
    }
}

export default connect(state => ({
    fields: state.shared.fields
}), { 
    setOneField: (field) => actions.setOneField(field),
})(SelectAuto);
