import React, { Component } from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';
import { validator, basic } from '../../helpers';
import { Label } from './Mini';
import sharedAction from '../../actions/sharedAction';
import stockService from '../../services/stockService';

const actions = new sharedAction;


class SelectMulti extends Component {

    name = this.props.inField['name'];
    field = {};
    state = {
        displayVal: []
    };

    componentDidMount(){
        
        if(!this.field.options || this.field.options.length == 0){
            stockService[this.field.stockOption]()
                .then(response => {
                    console.log("Elixer", response.results);
                    this.field.options = response.results;
                    this.props.setOneField(this.field);
                });
        }
    }

    handleOnChange = selectedOption => {
        this.setState({displayVal: selectedOption});
        this.field.value = selectedOption.map(item => {
            return item.value;
        });
        this.field.changed = true;
        this.field = validator.validate(this.field);
        this.props.setOneField(this.field);
        if(this.field.onChange){
            this.props.onChangeElement(e);
        }
    }

    render() {

        this.field = this.props.fields[this.name];
        
        let {error, options} = this.field;
        const errorClass = error ? 'has-danger' : '';
        const { displayVal } = this.state;
        
        return (
            <div className={'form-group ' + errorClass}>
                
                {this.field.showLabel && <Label field={this.field} />}
                
                <Select
                    value={displayVal}
                    onChange={this.handleOnChange}
                    options={options}
                    isMulti={true}
                    isSearchable={true}
                  />
                
                <div className="color-red">{error}</div>
            </div>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), {
  setOneField: (field) => actions.setOneField(field),
})(SelectMulti);