import React, { Component } from 'react';
import Input from './Input';
import Textarea from './Textarea';
import SelectAuto from './SelectAuto';
import Select from './Select';
import SelectCreate from './SelectCreate';
import SelectMulti from './SelectMulti';
import CheckboxSingle from './CheckboxSingle';
import Checkbox from './Checkbox';
import Radio from './Radio';
import DateInput from './DateInput';
import CheckboxList from './CheckboxList';
import ButtonInput from './ButtonInput';
import ButtonSelfInput from './ButtonSelfInput';
import FileUpload from './FileUpload';
import { DisplayValue } from './Mini';

export default class Element extends Component {

    onChangeElement = (e) => {
        this.props.onChange(e);
    }

    render() {

        const { field } = this.props;
        
        return (
            <div>
                { field && field.type == 'text' && <Input inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'textarea' && <Textarea inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'email' && <Input inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'password' && <Input inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'select' && <Select inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'select-auto' && <SelectAuto inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'select-multi' && <SelectMulti inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'select-create' && <SelectCreate inField={field} onChangeElement={this.onChangeElement} /> }    
                { field && field.type == 'checkbox-single' && <CheckboxSingle inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'checkbox' && <Checkbox inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'radio' && <Radio inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'date' && <DateInput inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'checkbox-list' && <CheckboxList inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'button-input' && <ButtonInput inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'button-self-input' && <ButtonSelfInput inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'display-value' && <DisplayValue inField={field} onChangeElement={this.onChangeElement} /> }
                { field && field.type == 'file-upload' && <FileUpload inField={field} onChangeElement={this.onChangeElement} /> }
            </div>
        );

    }

}
