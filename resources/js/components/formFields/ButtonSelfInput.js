import React, { Component } from 'react';
import { connect } from 'react-redux';
import { validator, basic } from '../../helpers';
import {Label} from './Mini';
import sharedAction from '../../actions/sharedAction';

const actions = new sharedAction;

class ButtonSelfInput extends Component {

    name = this.props.inField['name'];
    field = {};

    handleOnClick = e => {
        if(!this.field.changed){
            this.field.value = true;
            this.field.label = this.field.labelChangeOnClick;
            this.field.changed = true;
        }else {
            this.field.value = false;
            this.field.label = this.field.labelAtFirstTime;
            this.field.changed = false;
        }
        
        this.field = validator.validate(this.field);
        this.props.setOneField(this.field);
        if(this.field.onChange){
            this.props.onChangeElement(e);
        }
    }

    render() {

        this.field = this.props.fields[this.name];
        const error = this.field.error;
        const errorClass = error ? 'has-danger' : '';

         if(this.field.changed){
            this.field.value = true;
            this.field.label = this.field.labelChangeOnClick;
        } else {
            this.field.value = false;
            this.field.label = this.field.labelAtFirstTime;
        }
        
        return (
            <React.Fragment>
            {
                this.field.style == 'button' &&
                <div className={'form-group checkbox ' + errorClass}>
                    <button onClick={this.handleOnClick} className="btn btn-primary mr-2">{this.field.label}</button>
                    <div className="color-red">{error}</div>
                </div>
            }

            {
                this.field.style == 'text' &&
                <button type="button" onClick={this.handleOnClick} class="btn btn-link btn-fw">{this.field.label}</button>                
            }
            </React.Fragment>
        );

    }

}

export default connect(state => ({
    fields: state.shared.fields
}), { 
    setOneField: (field) => actions.setOneField(field),
})(ButtonSelfInput);