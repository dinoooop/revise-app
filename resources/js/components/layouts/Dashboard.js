import React, { Component } from 'react';
import TopNav from '../navs/TopNav';
import SideNav from '../navs/SideNav';

export default class Dashboard extends Component {

    state = {
        sideNavDisplay: true,
        sideNavClass: 'sidebar',
        mainPanel: 'main-panel main-panel-left',
    }

    onClickTogglerHandler = () => {

        if(this.state.sideNavDisplay){
            this.setState({
                sideNavDisplay: false,
                mainPanel: 'main-panel',
            });
        } else {
            this.setState({
                sideNavDisplay: true,
                mainPanel: 'main-panel main-panel-left',
            });
        }

    }
    
    render() {
        return (
            <div className="container-scroller">
                
                <TopNav onClickToggler={this.onClickTogglerHandler} />
                <div className="container-fluid page-body-wrapper">
                    { this.state.sideNavDisplay && <SideNav sideNavDisplayClass={this.state.sideNavClass} /> }
                    <div className={this.state.mainPanel}>
                        <div className="content-wrapper">
                          {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}