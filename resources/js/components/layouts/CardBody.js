import React from 'react';

export default function CardBody(props) {
	let gridClass = props.gridClass ? props.gridClass : 'col-lg-6';
	gridClass = gridClass + ' grid-margin stretch-card';

	return (
		
	        <div className={gridClass}>
	            <div className="card">
	                <div className="card-body">

	                	{
	                		props.cardTitle 
	                		&& <h4 className="card-title">{props.cardTitle}</h4>
	                	}

	                	{ props.children }

	                </div>
	            </div>
	        </div>
	    
	);	
}