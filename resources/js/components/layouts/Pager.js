import React, { Component } from 'react';
import { connect } from 'react-redux';
import TopNav from '../navs/TopNav';
import SideNav from '../navs/SideNav';
import Dashboard from './Dashboard';
import { basic, role, appRouterContext } from '../../helpers';
import loginAction from '../../actions/loginAction';
import PageLoader from '../blend/PageLoader';

const actions = new loginAction;

class Pager extends Component {

    componentDidMount = () => {

    }

    
    render() {
        return (
            <React.Fragment>
                { 
                    role.isset() && 
                    role.cap('manage_departments') ?
                        <Dashboard>
                            {this.props.children}
                        </Dashboard>
                        :
                        <p>Forbiden</p>
                }
            </React.Fragment>
        );
    }
}

export default connect(state => ({
    auth: state.shared.auth,
    loader: state.shared.loader,
}), {

})(Pager);