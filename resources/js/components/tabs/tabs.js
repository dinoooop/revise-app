import React, { Component } from 'react';
import { connect } from 'react-redux';
import { basic } from '../../helpers';

class Tabs extends Component {
	state = {
		current: 0
	}

	handleClick = id => {
		console.log(id);
		this.setState({current: id});
	}

 	render() {


	  	let listGroupClass = '';
  		const tabsHeader = this.props.header.map(item => {
	  		if(item.id == this.state.current){
	  			listGroupClass = "list-group-item active";
	  		}else{
	  			listGroupClass = "list-group-item";
	  		}
	  		return(
	  			<li key={item.id} className={listGroupClass} onClick={() => this.handleClick(item.id)}>{item.title}</li>
	  		);
  		});

  		const tabBody = this.props.children.map((child, i) => {
	  		if(this.state.current == i){
	  			return child;
	  		}
	  	});


	    return (
		    <div className="row">
		      	<div className="col-md-3 col-sm-4">
		      		<ul className="list-group">
						{tabsHeader}
					</ul>
		      	</div>
		      	<div className="col-md-9 col-sm-8">
		      		{ tabBody }
		      	</div>
	      	</div>
		  
	    );
  	}
}

export default connect(state => ({
    fields: state.shared.fields,
}), {})(Tabs);