import React, { Component } from 'react';

export default class ReviewLeftTable extends Component {

 	render() {
    
		const tableBody = this.props.items.map(item => {
  		return(
  			<tr key={item.id}>
          <td>{ item.review_date }</td>
          <td>{ item.fy }</td>
          <td>{ item.rt }</td>
        </tr>
  		);
  	});

    return (
		    
  		<div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                  <th>Review Date</th>
                  <th>FY</th>
                  <th>RT</th>
              </tr>
            </thead>
            <tbody>
              { tableBody }
            </tbody>
          </table>
      </div>
	      	
    );
	}
}

