import React, { Component } from 'react';
import { configData } from "../dataSource/configData";
import { basic } from "../helpers";
import parse from 'html-react-parser';

export default class DisplayCard extends Component {

 	render() {

    const {card} = this.props;
    
    const isImage = basic.isImage(card);

    return (
		    
  		<React.Fragment>
                    
        {
          isImage &&
          <img src={card} className="cardImage" />
          
        }
        
        {
          isImage == false &&
          <h2>{parse(card)}</h2>
          
        }

      </React.Fragment>
	      	
    );
	}
}

