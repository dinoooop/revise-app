import React, { Component } from 'react';
import { Link } from "react-router-dom";


// This table used to check what was the last card reviewed 
// how many interval it had taken.

export default class ReviewHistoryViewTable extends Component {

 	render() {
    const {items} = this.props;
		const tableBody = items.map(item => {
  		return(
  			<tr key={item.id}>
          <td>{item.id}</td>
          <td><Link to={`cards/edit/${item.card_id}`} >{item.card_id}</Link></td>
          <td>{item.review_date}</td>
          <td>{item.act_interval}</td>
          <td>{item.result}</td>
        </tr>
  		);
  	});

    return (
		    
  		<div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>id</th>
                <th>card id</th>
                <th>Review Date</th>
                <th>Act. Interval</th>
                <th>Result</th>
              </tr>
            </thead>
            <tbody>
              { tableBody }
            </tbody>
          </table>
      </div>
	      	
    );
	}
}
