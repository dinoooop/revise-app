import React, { Component } from 'react';
import { configData } from "../dataSource/configData";
import { basic } from "../helpers";
import LabelValueTable from '../components/tables/LabelValueTable';
import CardBody from '../components/layouts/CardBody';

export default class StatisticsLevelList extends Component {

    constructor(props){
        super(props);
        this.key = 0;
        this.state = {
            displayImg: ''
        };
    }

    

 	render() {    
        let i = 0;
        let heading = '';
        const levelList = this.props.items.map(level => {

            if(i == 0){
                heading = "All";
            } else {
                heading = "Level " + i;
            }

            i++;

            return(
                <CardBody key={i} cardTitle={heading}>
                        <h3>Status</h3>
                        <LabelValueTable items={level.status} />
                        
                        <h3>Today</h3>
                        <LabelValueTable items={level.today} />

                        <h3>Stage</h3>
                        <LabelValueTable items={level.stage} />
                        
                        <h3>Estimate</h3>
                        <LabelValueTable items={level.other} />

                        
                    </CardBody>
            );
        });

        return (
      	 <div className="row">
              {levelList}
          </div>    	      	
        );
	}
}
