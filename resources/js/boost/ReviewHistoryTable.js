import React, { Component } from 'react';
import { configData } from '../dataSource/configData';


// Used to display review history of specific card
export default class ReviewHistoryTable extends Component {

 	render() {
		const tableBody = this.props.items.map(item => {
			let className = (item.result == configData.failed) ? 'color-red' : '';
  		return(
  			<tr key={item.id}>
          <td className={className}>{ item.review_date }</td>
          <td>{ item.act_interval }</td>
          <td>{ item.result }</td>
        </tr>
  		);
  	});

    return (
		    
  		<div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                  <th>RD</th>
                  <th>Interval</th>
                  <th>Result</th>
              </tr>
            </thead>
            <tbody>
              { tableBody }
            </tbody>
          </table>
      </div>
	      	
    );
	}
}

