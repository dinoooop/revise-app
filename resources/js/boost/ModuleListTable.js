import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class ModuleListTable extends Component {

 	render() {
    const {items} = this.props;
		const tableBody = items.map(item => {
  		return(
  			<tr key={item.name}>
          <td><Link to={`/classify/${item.name}`} >{item.name}</Link></td>
          <td>{item.count}</td>
        </tr>
  		);
  	});

    return (
  		<div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                  <th>Name</th>
                  <th>Count</th>
              </tr>
            </thead>
            <tbody>
              { tableBody }
            </tbody>
          </table>
      </div>
	      	
    );
	}
}

