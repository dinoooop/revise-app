import React, { Component } from 'react';
import { configData } from "../dataSource/configData";
import { basic } from "../helpers";

export default class SubCard extends Component {

    constructor(props){
        super(props);
        this.key = 0;
        this.state = {
            displayImg: ''
        };
    }

    componentDidMount(){
        this.setImage(this.key);
    }

    handleOnClickNext = () => {
        this.key++;
        console.log(this.key);
        if(!this.setImage(this.key)){
            // No images available
            this.props.handleOnClickNext();
        }
    }

    setImage = (key) => {
        if(typeof this.props.subdocs[key] != "undefined" ){
            const displayImg = this.props.subdocs[key];
            this.setState({displayImg : displayImg});
            return true;
        } else {
            return false;
        }
    }

 	render() {    
        const {displayImg} = this.state;
        return (
      		<React.Fragment>            
              <div className="mrb-20">
                  <img src={displayImg} className="cardImage" />
                  <button type="button" onClick={this.handleOnClickNext} className="btn btn-primary mr-2">Next</button>
              </div>
          </React.Fragment>    	      	
        );
	}
}
