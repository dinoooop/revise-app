import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store/store';
import AppRouter from './AppRouter';

export default class Main extends Component {
    render() {
        return (
            <Provider store={store}>
                <AppRouter />                
            </Provider>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<Main />, document.getElementById('app'));
}
