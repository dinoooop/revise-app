import { validator, fielder, manipulation, basic, role } from '../helpers';


export default class sharedAction {

  df = (type, payload) => {
    return {type: type, payload: payload};
  }

   // requestType : store, update, create, edit, delete
  requestItem = (requestType, params = true, success) => dispatch => {

    let toSubmit = [];
    let fields = manipulation.getShared('fields');
    let error = false;

    // validation require requestTypes
    const valReqRequestType = ['store', 'update'];

    if(valReqRequestType.includes(requestType)){

      const validatedFields = validator.validate(fields);

      if(validator.hasErrors()){
        console.log('Error in validation');
        dispatch(this.df('fields', validatedFields));
        error = true;
      }

    }
    
    if(!error){

      if(params === true){
        // If require any id, set it from screen using seFieldValue
        toSubmit = fielder.extractFormDataField(fields);
        toSubmit = fielder.setFormDataForFile(toSubmit);
      }
      
      dispatch(this.df('toSubmit', toSubmit));
      
      manipulation.setPageLoader(dispatch);
      this.service[requestType](toSubmit)
        .then(res => {

          manipulation.setPageLoader(dispatch);
          fields = fielder.setFormDataField(fields, res, false);
          dispatch(this.df('response', res));
          dispatch(this.df('fields', fields));

          switch(requestType){
            case 'index':
              // Index is load by laravel paginate
              if(typeof res.current_page != "undefined"){
                dispatch(this.df('items', res.data));
                dispatch(this.df('pageCount', res.last_page));
              } else {
                dispatch(this.df('items', res));
              }
              
              if(res.length == 0){
                dispatch(this.df('noItemMsg', 'No items found'));
              } else {
                dispatch(this.df('noItemMsg', ''));
              }
              break;

            case 'delete':
              dispatch(this.df('items', manipulation.deleteItem(fields.id.value)));

          }
          success(res);

        })
        .catch(err => {

          manipulation.setPageLoader(dispatch);
          if(err.response){
            console.log('serverErrors');
            fields = fielder.setServerErrors(fields, err.response.data);
            dispatch(this.df('fields', fields));
          }

        });
    }
  }

  setOneField = field => dispatch => {
    const fields = manipulation.addFieldToFields(field);
    dispatch(this.df('fields', fields));
  };

  setFieldValue = (key, value) => dispatch => {
    const fields = manipulation.addFieldValueToFields(key, value);
    dispatch(this.df('fields', fields));
  }

  removeField = key => dispatch => {
    const fields = manipulation.removeField(key);
    dispatch(this.df('fields', fields));
  }

  setSelectedItems = id => dispatch => {
    dispatch(this.df('selectedItems', manipulation.getSelectedItems(id)));
  };

  setAllItemsSelected = () => dispatch => {
    let selectedIds = manipulation.getSelectedItems();
    selectedIds = (selectedIds.length >= manipulation.getIdsFromItems().length) ? [] : manipulation.getIdsFromItems().concat(['all']);
    dispatch(this.df('selectedItems', selectedIds));
  };

  setDataSource = (formName) => dispatch => {

    let dataSource = this.service.dataSource;
    //dataSource = basic.setDataSourceForCaps(dataSource);
    dispatch(this.df('dataSource', dataSource));

    if(typeof formName != "undefined"){
      const fields = fielder.getFormFields(formName, dataSource[formName]);
      dispatch(this.df('fields', fields));
    }

  }

  setTestFields = (fields) => dispatch => {
      dispatch(this.df('fields', fields));
  }

  // If no items in current page auto shift to prev page
  setItemsAfterBulkAction = (dispatch, page) => {
    dispatch(this.df('selectedItems', []));
    this.service.index({page: page}).then(res => {
      
      dispatch(this.df('items', res));
      dispatch(this.df('pageCount', res.last_page));

      if(res.length == 0) {
        if(page > 1){
          this.setItemsAfterBulkAction(dispatch, --page);
        }
      }
    });
  };

  sortItem = column => dispatch => {
    const sort = manipulation.getSortValues(column);
    dispatch(this.df('sortValues', sort));
    return {sortOrder: sort[column], sortBy: column};
  };

  executeBulkAction = action => dispatch => {    
    const selectedIds = manipulation.refineSelectedIds();    
    if(action == 'delete'){
      this.service.delete(0, {ids: selectedIds}).then(res => {
        this.setItemsAfterBulkAction(dispatch, manipulation.getCurrentPage());
      });
    } else if(action == 'active' || action == 'inactive'){
      
      let toSubmit = {
        ids: selectedIds,
        status: basic.getStatusId(action)
      }

      this.service.update(0, toSubmit)
        .then(res => {
          this.setItemsAfterBulkAction(dispatch, manipulation.getCurrentPage());
        })
        .catch(err => {              
            if(err.response){
              console.log(err.response);
            }
        });
      }
  }

  indexPage = () => {
    return this.service.namespace;
  }

  resetStoreForItems = () => dispatch => {
    dispatch(this.df('items', []));
    dispatch(this.df('pageCount', 0));
    dispatch(this.df('currentPage', 1));
    dispatch(this.df('sortValues', {}));
    dispatch(this.df('selectedItems', []));
    dispatch(this.df('fields', {}));
  }

  authorize = (dispatch, success) => {

    if(!role.isset()){
      this.service.authUser().then(res => {
        dispatch({
          type: 'auth',
          payload: res,
        });
        success(res);
      });
    }else {
      success({});
    }
  }

}