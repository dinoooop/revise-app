import settingService from '../services/settingService';
import sharedAction from './sharedAction';

export default class settingAction extends sharedAction {

	constructor(){
		super();
		this.service = new settingService;
	}
	
}