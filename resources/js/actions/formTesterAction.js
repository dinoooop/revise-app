import formTesterService from '../services/formTesterService';
import sharedAction from './sharedAction';

export default class formTesterAction extends sharedAction {

	constructor(){
		super();
		this.service = new formTesterService;
	}
	
}