import tagService from '../services/tagService';
import sharedAction from './sharedAction';

export default class tagAction extends sharedAction {

	constructor(){
		super();
		this.service = new tagService;
	}
	
}