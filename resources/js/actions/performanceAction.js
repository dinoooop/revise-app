import performanceService from '../services/performanceService';
import sharedAction from './sharedAction';

export default class performanceAction extends sharedAction {

	constructor(){
		super();
		this.service = new performanceService;
	}
	
}