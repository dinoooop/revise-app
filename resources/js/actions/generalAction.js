import generalService from '../services/generalService';
import sharedAction from './sharedAction';

export default class generalAction extends sharedAction {

	constructor(){
		super();
		this.service = new generalService;
	}
	
}