import imageClassifyService from '../services/imageClassifyService';
import sharedAction from './sharedAction';

export default class imageClassifyAction extends sharedAction {

	constructor(){
		super();
		this.service = new imageClassifyService;
	}
	
}