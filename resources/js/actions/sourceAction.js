import sourceService from '../services/sourceService';
import sharedAction from './sharedAction';

export default class sourceAction extends sharedAction {

	constructor(){
		super();
		this.service = new sourceService;
	}
	
}