import reviewService from '../services/reviewService';
import sharedAction from './sharedAction';

export default class reviewAction extends sharedAction {

	constructor(){
		super();
		this.service = new reviewService;
	}
	
}