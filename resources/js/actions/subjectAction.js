import subjectService from '../services/subjectService';
import sharedAction from './sharedAction';

export default class subjectAction extends sharedAction {

	constructor(){
		super();
		this.service = new subjectService;
	}
	
}