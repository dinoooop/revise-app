import { validator, fielder, manipulation, basic, role } from '../helpers';
import smcService from '../services/smcService';
import sharedAction from './sharedAction';

export default class smcAction extends sharedAction {

	constructor(){
		super();
		this.service = new smcService;
	}

    // Set Last clicked radio button for edit
    setLastRadio = (param) => dispatch => {
        let optionItem = [];
        let fields = manipulation.getShared('fields');
        switch(param){
            case 'subject':
                fields.id_edit.value = fields.subject_id.value;
                optionItem = basic.selectFromArrayObject(fields.subject_id.options, 'value', fields.subject_id.value);
                fields.name_edit.value = optionItem.label;
                fields.smctype.value = param;
                break;

            case 'module':
                fields.id_edit.value = fields.module_id.value;
                optionItem = basic.selectFromArrayObject(fields.module_id.options, 'value', fields.module_id.value);
                fields.name_edit.value = optionItem.label;
                fields.smctype.value = param;
                break;

            case 'chapter':
                fields.id_edit.value = fields.chapter_id.value;
                optionItem = basic.selectFromArrayObject(fields.chapter_id.options, 'value', fields.chapter_id.value);
                fields.name_edit.value = optionItem.label;
                fields.smctype.value = param;
                break;
        }

        dispatch(this.df('fields', fields));
    }

    viewAfterSubmitAdd = (item) => dispatch => {

        let fields = manipulation.getShared('fields');
        
        switch(fields.smctype.value){
            case 'subject':
                fields.subject_id.options = fields.subject_id.options.concat(basic.labelValueConvert([item], 'name', 'id'));
                break;

            case 'module':
                fields.module_id.options = fields.module_id.options.concat(basic.labelValueConvert([item], 'name', 'id'));
                break;

            case 'chapter':
                fields.chapter_id.options = fields.chapter_id.options.concat(basic.labelValueConvert([item], 'name', 'id'));
                break;
        }

        fields.name.value = '';
        dispatch(this.df('fields', fields));
    }

    viewAfterSubmitEdit = (item) => dispatch => {

        let fields = manipulation.getShared('fields');
        let options = [];
        let updatedOptionItem = [];
        
        switch(fields.smctype.value){
            case 'subject':
                options = fields.subject_id.options;
                updatedOptionItem = basic.labelValueConvert([item], 'name', 'id');
                fields.subject_id.options = basic.updateObjInArray(options, 'value', item.id, updatedOptionItem[0]);
                break;

            case 'module':
                options = fields.module_id.options;
                updatedOptionItem = basic.labelValueConvert([item], 'name', 'id');
                fields.module_id.options = basic.updateObjInArray(options, 'value', item.id, updatedOptionItem[0]);
                break;

            case 'chapter':
                options = fields.chapter_id.options;
                updatedOptionItem = basic.labelValueConvert([item], 'name', 'id');
                fields.chapter_id.options = basic.updateObjInArray(options, 'value', item.id, updatedOptionItem[0]);
                break;
        }

        fields.name_edit.value = '';
        fields.id_edit.value = '';
        dispatch(this.df('fields', fields));
    }

	
	
}