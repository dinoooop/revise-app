import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Dashboard from '../../components/layouts/Dashboard';
import IndexCard from './Index';
import CreateCard from './Create';
import EditCard from './Edit';
import CsvImport from './CsvImport';

export default function Card(props){
    const fields = props.fields;
    const { url } = props.match;
      
    return (
        <React.Fragment>
            <Route exact path={`${url}`} component={IndexCard} />
            <Route path={`${url}/create`} component={CreateCard} />
            <Route path={`${url}/edit/:id`} component={EditCard} />
            <Route path={`${url}/csv-import`} component={CsvImport} />
        </React.Fragment>
    );
}