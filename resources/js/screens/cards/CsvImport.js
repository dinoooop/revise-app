import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import cardAction from '../../actions/cardAction';

const actions = new cardAction();

class CsvImport extends Component {

    componentDidMount(){

        this.props.setDataSource('csvImport');
        this.props.setFieldValue('csv_import', '');

    }

  

    handleOnSubmit = e => {
        e.preventDefault();
        this.props.requestItem('store', true, (res) => { });
    }


    redirectToIndex = () => {
        this.props.history.push(this.props.dataSource.namespace);
    }

    render() {
        
        let { fields } = this.props;
        
        return (
            
                    <Dashboard>
                    {
                        fields.message &&
                        fields.message.value != '' &&
                        <CardBody cardTitle="Message">
                            <p>{fields.message.value}</p>
                        </CardBody>
                    }
                    {
                        fields.csv_import &&
                        <CardBody cardTitle="Import File">
                            <p className="color-red">Set profile before import</p>
                            <Element field={fields.csv_import} />
                            <Element field={fields.matrix_que} />
                            <Element field={fields.processing} />
                            <Element field={fields.voc} />
                            
                            <button type="submit" onClick={this.handleOnSubmit} className="btn btn-primary mr-2">Submit</button>
                            
                        </CardBody>
                    }

                        <CardBody cardTitle="Validating Column" gridClass="col-lg-6">
                            <ul>
                                <li>que</li>
                                <li>ans</li>
                                <li>source</li>
                                <li>subject</li>
                                <li>module</li>
                                <li>chapter</li>
                                <li>imp</li>
                                <li>confused</li>
                                <li> --- If Processing ---</li>
                                <li>mis</li>
                                <li>sid</li>
                                <li> --- If Voc ---</li>
                                <li>mal</li>
                                <li>eng</li>
                                <li>usage</li>
                            </ul>
                        </CardBody>
                    </Dashboard>
                  
        );
    }
}


export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    setFieldValue: actions.setFieldValue,
})(CsvImport);
