import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import cardAction from '../../actions/cardAction';
import { role } from '../../helpers';
import DisplayCard from '../../boost/DisplayCard';
import ReviewHistoryTable from '../../boost/ReviewHistoryTable';

const actions = new cardAction();

class Edit extends Component {

    componentDidMount = () => {
        this.id = this.props.match.params.id;
        this.props.setDataSource('edit');
        this.props.setFieldValue('id', this.id);
        this.props.requestItem('edit');
    }

    handleOnSubmit = e => {
        e.preventDefault();
        this.props.requestItem('update', true, (res) => {
            this.redirectToIndex()
        });
    }

    handleOnClickCheckbox = e => {
        e.preventDefault();
        this.props.setFieldValue('action', 'changed');
        this.props.requestItem('edit');
    }

    redirectToIndex = () => {
        this.props.history.push(this.props.dataSource.namespace);
    }

    render() {

        const fields =  this.props.fields;

        return (
            <Dashboard onKeyPress={this.onKeyPress}>   
            {
                fields.que &&
                <React.Fragment>
                    <div className="row">
                        <CardBody cardTitle="Edit Card" gridClass="col-lg-12">
                            <Element field={fields.que} />
                            <Element field={fields.ans} />
                        </CardBody>
                    </div>

                    <div className="row">
                        <CardBody cardTitle="History" gridClass="col-lg-6">
                            <ReviewHistoryTable items={fields.history.value} />
                        </CardBody>
                        <CardBody cardTitle="Submit" gridClass="col-lg-6">
                            <button type="submit" onClick={this.handleOnSubmit} className="btn btn-primary mr-2">Submit</button>
                            <button type="button" onClick={this.redirectToIndex} className="btn btn-light">Cancel</button>
                        </CardBody>
                    </div>

                    <div className="row">
                        <CardBody cardTitle="Subjects" gridClass="col-md-4">
                            <Element field={fields.subject_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Modules" gridClass="col-md-4">
                            <Element field={fields.module_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Chapters" gridClass="col-md-4">
                            <Element field={fields.chapter_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                    </div>

                    <div className="row">
                        <CardBody cardTitle="Ans Type" gridClass="col-md-4">
                            <Element field={fields.ans_type} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Sources" gridClass="col-md-4">
                            <Element field={fields.source_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Tags" gridClass="col-md-4">
                            <Element field={fields.tag_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                    </div>

                    <div className="row">
                        <CardBody cardTitle="Stage" gridClass="col-md-4">
                            <Element field={fields.stage} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Level" gridClass="col-md-4">
                            <Element field={fields.level} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Status" gridClass="col-md-4">
                            <Element field={fields.status} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                    </div>

                </React.Fragment>
            }
            </Dashboard>
        );
    }
}

export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    setFieldValue: actions.setFieldValue,
})(Edit);