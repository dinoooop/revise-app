import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import reviewAction from '../../actions/reviewAction';
import ReviewHistoryTable from '../../boost/ReviewHistoryTable';
import { role } from '../../helpers';
import {configData} from '../../dataSource/configData';
import DisplayCard from '../../boost/DisplayCard';
import SubCard from '../../boost/SubCard';


const actions = new reviewAction();

class Review extends Component {

    constructor(props){
        super(props);
        this.state = {
            currentCard: 'Q',
            showModule: false,
        };

    }

    componentDidMount = () => {
        document.addEventListener('keydown',this.keydownHandler);
        
        this.review_mode = this.props.match.params.review_mode;
        this.props.setDataSource('create');
        this.loadNewQuestion();
        
    }

    loadNewQuestion = () => {
        this.props.setDataSource('create');
        this.props.setFieldValue('review_mode', this.review_mode);
        this.props.requestItem('create');
    }

    handleOnClickShowModule = () => {
        this.setState({showModule : !this.state.showModule});
    }

    keydownHandler = e => {
        console.log('E which');
        console.log(e.which);

        if(e.which === 13){
            // Press Enter 
            if(this.review_mode == 'NC'){
                this.props.setFieldValue('result', null);
                this.handleOnClickNext();
            }

            if(
                this.review_mode == 'RT' 
                || this.review_mode == 'FY'
            ){
                this.props.setFieldValue('result', configData.success);
                this.handleOnClickNext();
            }
        } else if(e.which === 83){
            // Press S
            this.props.setFieldValue('result', configData.success);
            this.handleOnClickNext();
        } else if(e.which === 70){
            // Press F
            this.props.setFieldValue('result', configData.failed);
            this.handleOnClickNext();
        } else if(e.which === 75){
            // Press K
            this.props.setFieldValue('stage', configData.stage.KW);
            this.props.setFieldValue('result', configData.success);
            this.handleOnClickNext();
        } else if(e.which === 69){
            // Press E for edit
            let id = this.props.fields.card_id.value;
            window.open(configData.host + '/cards/edit/' + id, '_blank');
        } else if(e.which === 81){
            // Press Q for change last one as failed
            this.props.setFieldValue('action', 'failed-last');
            this.props.requestItem('update', true, res => {
                this.props.removeField('action');
            });
        } else if(e.which === 87){
            // Press W for change last one as success
            this.props.setFieldValue('action', 'success-last');
            this.props.requestItem('update', true, res => {
                this.props.removeField('action');
            });
        }   
    }

    handleOnClickNext = () => {
        
        let {currentCard } = this.state;
        let subdocs_count  = this.props.fields.subdocs_count.value;
        let error_value  = this.props.fields.error.value;


        // Auto redirect to FY RT NC
        if(error_value){

            if(this.review_mode == 'FY'){
                this.next_review_mode = 'RT';
            }

            if(this.review_mode == 'RT'){
                this.next_review_mode = 'NC';
            }
            
            window.location.href = this.props.dataSource.namespace + '/' + this.next_review_mode;
            
        }

        if(currentCard == 'Q'){
            currentCard = 'A';
        } else if(currentCard == 'A') {

             if(subdocs_count == 0){
                currentCard = 'Q';
                this.props.setFieldValue('id', this.props.fields.card.value.id);
                this.props.requestItem('update', true, res => {
                    this.props.removeField('id');
                    this.loadNewQuestion();
                });
            } else {
                currentCard = 'Sub';
            }

        } else if(currentCard == 'Sub') {
            
            currentCard = 'Q';
            this.props.setFieldValue('id', this.props.fields.card.value.id);
            this.props.requestItem('update', true, res => {
                this.props.removeField('id');
                this.loadNewQuestion();
            });
            
        }

        this.setState({currentCard});

        if(this.state.showModule){
            this.setState({showModule : !this.state.showModule});
        }
    }



    componentDidUpdate = () => {
        if(this.review_mode != this.props.match.params.review_mode){
            this.componentDidMount();
        }
    }

    componentWillUnmount(){
        document.removeEventListener('keydown',this.keydownHandler);
        this.props.resetStoreForItems();
    }


    render() {

        const {fields} =  this.props;
        const {currentCard, showModule} = this.state;

        return (
            <Dashboard>
            {
                fields.error &&
                fields.error.value &&
                <div className="row">
                    <CardBody cardTitle="Message" gridClass="col-lg-12">
                        <p>{fields.errorMsg.value}</p>
                    </CardBody>
                </div>
            }

            {
                typeof fields.card != "undefined" &&
                typeof fields.card.value.id != "undefined" &&
                <React.Fragment>
                {
                    currentCard == 'Q' &&
                    <div className="row">
                        <CardBody cardTitle="Question" gridClass="col-lg-12">
                            <DisplayCard card={fields.card.value.que} />
                            <button type="button" onClick={this.handleOnClickNext} className="btn btn-primary mr-2">Next</button>
                            <button type="button" onClick={this.handleOnClickShowModule} className="btn btn-primary mr-2">Show Module</button>
                            {
                                showModule &&
                                <button type="button" className="btn btn-primary mr-2">{fields.card.value.module.name}</button>
                            }
                        </CardBody>
                    </div>
                }

                {
                    currentCard == 'A' &&
                    <div className="row">
                        <CardBody cardTitle="Answer" gridClass="col-lg-12">
                            <DisplayCard card={fields.card.value.que} />
                            <DisplayCard card={fields.card.value.ans} />
                        </CardBody>
                        
                    </div>
                }

                {
                    currentCard == 'Sub' &&
                    <div className="row">
                        <CardBody cardTitle="Sub Card" gridClass="col-lg-12">
                            <SubCard subdocs={fields.subdocs_list.value} handleOnClickNext={this.handleOnClickNext} />
                            
                        </CardBody>
                    </div>
                }

                {
                    (currentCard == 'A' || currentCard == 'Sub' ) &&
                    <div className="row">
                        <CardBody cardTitle="" gridClass="col-lg-4">
                            <Element field={fields.stage} />
                            <Element field={fields.result} />
                            <Element field={fields.status} />
                            <Element field={fields.tag_id} />
                        </CardBody>
                        <CardBody cardTitle="" gridClass="col-lg-4">
                            <ReviewHistoryTable items={fields.history.value} />
                            <Element field={fields.append_txt} />
                        </CardBody>
                        <CardBody cardTitle="" gridClass="col-lg-4">                            
                            <Element field={fields.review_after} />
                            <Element field={fields.review_count} />
                            <Element field={fields.card_count} />
                            <Element field={fields.card_id} />
                            <a href={"/cards/edit/" + fields.card_id.value} className="btn btn-light mr-2" target="_blank">Edit</a>
                            <button type="button" onClick={this.handleOnClickNext} className="btn btn-primary mr-2">Next</button>
                        </CardBody>
                    </div>
                }

                
                </React.Fragment>
            }

            
            </Dashboard>
        );
    }
}

export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    setFieldValue: actions.setFieldValue,
    removeField: actions.removeField,
    resetStoreForItems: actions.resetStoreForItems,
})(Review);
