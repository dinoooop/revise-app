import React, { Component } from 'react';
import { connect } from 'react-redux';
import { basic } from '../../helpers';
import CardBody from '../../components/layouts/CardBody';
import Tabs from '../../components/tabs/Tabs';
import Address from './Address';

class TabTester extends Component {

  render() {
  	const header = [
	  	{
	  		title: "home",
	  		id: 0,
	  	},
	  	{
	  		title: "Profile",
	  		id: 1,
	  	},
	  	{
	  		title: "Address",
	  		id: 2,
	  	}
  	];

    return (
      <CardBody cardTitle="Tab tester" gridClass="col-md-12">
      	<Tabs header={header}>
			<div className="inner-tab-content">
      			<p> Home date Lorem ipsum dolor site amet</p>
      		</div>

  			<div className="inner-tab-content">
      			<p>Cillum ad ut irure tempor velit nostrud occaecat ullamco aliqua anim Lorem sint. Veniam sint duis incididunt do esse magna mollit excepteur laborum qui. Id id reprehenderit sit est eu aliqua occaecat quis et velit excepteur laborum mollit dolore eiusmod. Ipsum dolor in occaecat commodo et voluptate minim reprehenderit mollit pariatur. Deserunt non laborum enim et cillum eu deserunt excepteur ea incididunt minim occaecat.</p>
      		</div>

      		<div className="inner-tab-content">
      			<Address />
      		</div>
      	</Tabs>
      </CardBody>
    );
  }
}

export default connect(state => ({
    fields: state.shared.fields,
}), {})(TabTester);