import React, { Component } from 'react';
import { connect } from 'react-redux';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import sharedAction from '../../actions/sharedAction';
import { fielder, validator, basic } from '../../helpers';
import { formTester } from '../../dataSource/formTester';

const actions = new sharedAction();

class Address extends Component {

    componentDidMount(){
        let fields = fielder.getFormFields('create', formTester);
        fields = fielder.considerOnly(fields, ['name', 'country']);
        this.props.setTestFields(fields);        
    }

    handleOnSubmit = e => {
        e.preventDefault();
        validator.validate(this.props.fields);
        this.props.setTestFields(validator.fields); 
    }

    onKeyPress = e => {
        if (e.which === 13 /* Enter */) {
          e.preventDefault();
        }
    }

    render() {
        
        let { fields } = this.props;
        
        return (
   
                <div className="col-md-6">
                    <form onKeyPress={this.onKeyPress} onSubmit={this.handleOnSubmit}>
                        <Element field={fields.name} />                    
                        <Element field={fields.country} />                    
                        <button type="submit" className="btn btn-primary mr-2">Submit</button>                    
                        <button onClick={this.redirectToIndex} className="btn btn-light">Cancel</button>
                    </form>
                </div>

        );
    }
}


export default connect(state => ({
    fields: state.shared.fields,
}), {
    setTestFields: actions.setTestFields
})(Address);