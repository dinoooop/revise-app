import React, { Component } from 'react';
import { connect } from 'react-redux';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import formTesterAction from '../../actions/formTesterAction';
import { fielder, validator, basic } from '../../helpers';
import { formTesterData } from '../../dataSource/formTesterData';
import axios from 'axios';

const actions = new formTesterAction();

class Create extends Component {

    componentDidMount(){
        this.props.setDataSource('create');
    }

    handleOnSubmit = e => {
        e.preventDefault();
        this.props.requestItem('store', true, () => { });
    }

    onKeyPress = e => {
        if (e.which === 13 /* Enter */) {
          e.preventDefault();
        }
    }

    render() {
        
        let { fields } = this.props;
        
        return (
            <CardBody cardTitle="Form tester">
                <form onKeyPress={this.onKeyPress} onSubmit={this.handleOnSubmit}>
                    <Element field={fields.csv_import} />
                    <Element field={fields.name} />
                    <button type="submit" className="btn btn-primary mr-2">Submit</button>
                    <button onClick={this.redirectToIndex} className="btn btn-light">Cancel</button>
                </form>
            </CardBody>
        );
    }
}


export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
})(Create);