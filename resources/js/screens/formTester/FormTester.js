import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Dashboard from '../../components/layouts/Dashboard';
import CreateFieldTester from './Create';
//import TabTester from './TabTester';


export default function FormTester(props){
    const fields = props.fields;
    const { url } = props.match;
        
    return (
        <Dashboard>
            <Route exact path={`${url}`} component={CreateFieldTester} />
            
        </Dashboard>
    );
}