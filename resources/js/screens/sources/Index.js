import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import IndexHeader from '../../components/blend/IndexHeader';
import IndexTable from '../../components/tables/IndexTable';
import ReactPaginate from 'react-paginate';
import CardBody from '../../components/layouts/CardBody';
import { basic, pageContext, role } from '../../helpers';
import sourceAction from '../../actions/sourceAction';

const actions = new sourceAction();

class Index extends Component {

  constructor (props){    
    super(props);
    this.partialActions = {
      handleDelete: this.handleDelete,
      handleSorting: this.handleSorting,
      handleSearchItem: this.handleSearchItem,      
    }
  }

  componentDidMount = () => {
    this.props.setDataSource('index');
    this.props.requestItem('index', false);
  }

  handleDelete = (id) => {
    this.props.setFieldValue('id', id);
    basic.confirm('delete', () => {
      this.props.requestItem('delete');
    });    
  }

  handlePageChange = (e) => {
    let page = e.selected + 1;
    this.props.setFieldValue('page', page);
    this.props.requestItem('index');
  }

  handleSorting = name => {
    const sort = this.props.sortItem(name);
    this.props.setFieldValue('so', sort.sortOrder);
    this.props.setFieldValue('sb', sort.sortBy);
    this.props.requestItem('index');
  }

  handleSearchItem = e => {
    this.props.setFieldValue('search', e.target.value);
    this.props.requestItem('index');
  }

  componentWillUnmount(){
    this.props.resetStoreForItems();
  }

  render() {
      console.log("hi");
      
      const fields =  this.props.fields;
      const { indexTableProps } = this.props.dataSource;

     
      return (
              
                <Dashboard>
                  <pageContext.Provider value={this.partialActions}>
                    <CardBody gridClass="col-lg-12">

                      <IndexHeader heading="Sources" newButtonUrl="/sources/create" capNew="manage_sources" />

                      <div className="clearfix"></div>

                      {
                        indexTableProps && <IndexTable /> 
                      }

                      {
                        (this.props.pageCount > 1 )
                        && 
                        <ReactPaginate 
                          pageCount={this.props.pageCount}
                          marginPagesDisplayed={3}
                          pageRangeDisplayed={3}
                          initialPage={0}
                          containerClassName="pagination"
                          onPageChange={this.handlePageChange}
                        />
                      }
                      
                    </CardBody>
                  </pageContext.Provider>
                </Dashboard>                
      );
  }
}

export default connect(state => ({
  dataSource: state.shared.dataSource,
  fields: state.shared.fields,
  items: state.shared.items,
  pageCount: state.shared.pageCount,
  selectedItems: state.shared.selectedItems,
}), {
  setDataSource: actions.setDataSource,
  requestItem: actions.requestItem,
  setFieldValue: actions.setFieldValue,
  resetStoreForItems: actions.resetStoreForItems,
})(Index);