import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import sourceAction from '../../actions/sourceAction';
import { role } from '../../helpers';

const actions = new sourceAction();

class Edit extends Component {

    componentDidMount = () => {
        this.id = this.props.match.params.id;
        this.props.setDataSource('edit');
        this.props.setFieldValue('id', this.id);
        this.props.requestItem('edit');
    }

    handleOnSubmit = e => {
        e.preventDefault();
        this.props.requestItem('update', true, (res) => {
            this.redirectToIndex()
        });
    }

    onKeyPress = e => {
        if (e.which === 13 /* Enter */) {
          e.preventDefault();
        }
    }

    redirectToIndex = () => {
        this.props.history.push(this.props.dataSource.namespace);
    }

    render() {
        const fields =  this.props.fields;

        console.log(fields);

        return (
            <React.Fragment>
                {
                    fields.name &&
                    <React.Fragment>
                            
                            <Dashboard>   
                                <CardBody cardTitle="Edit Tag">
                                    <form onKeyPress={this.onKeyPress} onSubmit={this.handleOnSubmit}>
                                        <Element field={fields.name} />                                                            
                                        <button type="submit" className="btn btn-primary mr-2">Submit</button>
                                        <button type="button" onClick={this.redirectToIndex} className="btn btn-light">Cancel</button>
                                    </form>
                                </CardBody>                                
                            </Dashboard>
                            
                        
                    </React.Fragment>
                }
            </React.Fragment>




        );
    }
}

export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    setFieldValue: actions.setFieldValue,
})(Edit);