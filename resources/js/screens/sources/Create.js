import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import sourceAction from '../../actions/sourceAction';

const actions = new sourceAction();

class Create extends Component {

    componentDidMount(){
        this.props.setDataSource('create');
        this.props.requestItem('create', false);
    }

    handleOnSubmit = e => {
        e.preventDefault();
        this.props.requestItem('store', true, () => {
            this.redirectToIndex();
        });
    }

    onKeyPress = e => {
        if (e.which === 13 /* Enter */) {
          e.preventDefault();
        }
    }

    redirectToIndex = () => {
        this.props.history.push(this.props.dataSource.namespace);
    }

    render() {
        
        let { fields } = this.props;
        
        return (
            <React.Fragment>
              {
                this.props.dataSource.dmSet &&
                <React.Fragment>
                  
                    <Dashboard>   
                        <CardBody cardTitle="Create Tags">
                            <form onKeyPress={this.onKeyPress} onSubmit={this.handleOnSubmit}>
                                <Element field={fields.name} />
                                <Element field={fields.head_id} />
                                
                                <button type="submit" className="btn btn-primary mr-2">Submit</button>                    
                                <button onClick={this.redirectToIndex} className="btn btn-light">Cancel</button>
                            </form>
                        </CardBody>
                    </Dashboard>
                  
                </React.Fragment>
              }
            </React.Fragment>
        );
    }
}


export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
})(Create);
