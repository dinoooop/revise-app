import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Dashboard from '../../components/layouts/Dashboard';
import IndexSource from './Index';
import CreateSource from './Create';
import EditSource from './Edit';


export default function Source(props){
    const fields = props.fields;
    const { url } = props.match;
      
    return (
        <React.Fragment>
            <Route exact path={`${url}`} component={IndexSource} />
            <Route path={`${url}/create`} component={CreateSource} />
            <Route path={`${url}/edit/:id`} component={EditSource} />            
        </React.Fragment>
    );
}