import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Dashboard from '../../components/layouts/Dashboard';
import IndexTag from './Index';
import CreateTag from './Create';
import EditTag from './Edit';


export default function Tag(props){
    const fields = props.fields;
    const { url } = props.match;
      
    return (
        <React.Fragment>
            <Route exact path={`${url}`} component={IndexTag} />
            <Route path={`${url}/create`} component={CreateTag} />
            <Route path={`${url}/edit/:id`} component={EditTag} />            
        </React.Fragment>
    );
}