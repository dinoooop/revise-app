import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import IndexHeader from '../../components/blend/IndexHeader';
import IndexTable from '../../components/tables/IndexTable';
import ReactPaginate from 'react-paginate';
import CardBody from '../../components/layouts/CardBody';
import { basic, pageContext, role } from '../../helpers';
import profileAction from '../../actions/profileAction';

const actions = new profileAction();

class Index extends Component {

  constructor (props){    
    super(props);
    this.partialActions = {
      handleDelete: this.handleDelete,
      handleOnActivate: this.handleOnActivate,
    }
  }

  componentDidMount = () => {
    this.props.setDataSource('index');
    this.props.requestItem('index', false);
  }

  handleDelete = (id) => {
    this.props.setFieldValue('id', id);
    basic.confirm('delete', () => {
      this.props.requestItem('delete');
    });    
  }

  handleOnActivate = (id) => {
    
    this.props.setFieldValue('is_active', true);
    this.props.setFieldValue('id', id);

    this.props.requestItem('update', true, (res) => {
      this.props.requestItem('index');
    });
  }

  componentWillUnmount(){
    this.props.resetStoreForItems();
  }

  render() {

      console.log("Narrow");
      
      const fields =  this.props.fields;
      const { indexTableProps } = this.props.dataSource;
     
      return (
                <Dashboard>
                  <pageContext.Provider value={this.partialActions}>
                    <CardBody gridClass="col-lg-12">

                      <IndexHeader heading="Profiles" newButtonUrl="/profiles/create" capNew="manage_profiles" />

                      <div className="clearfix"></div>

                      {
                        indexTableProps && <IndexTable /> 
                      }

                      {
                        (this.props.pageCount > 1 )
                        && 
                        <ReactPaginate 
                          pageCount={this.props.pageCount}
                          marginPagesDisplayed={3}
                          pageRangeDisplayed={3}
                          initialPage={0}
                          containerClassName="pagination"
                          onPageChange={this.handlePageChange}
                        />
                      }
                      
                    </CardBody>
                  </pageContext.Provider>
                </Dashboard>                
      );
  }
}

export default connect(state => ({
  dataSource: state.shared.dataSource,
  fields: state.shared.fields,
  items: state.shared.items,
  pageCount: state.shared.pageCount,
  
}), {
  setDataSource: actions.setDataSource,
  requestItem: actions.requestItem,
  setFieldValue: actions.setFieldValue,
  resetStoreForItems: actions.resetStoreForItems,
})(Index);