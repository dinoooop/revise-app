import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import profileAction from '../../actions/profileAction';
import { role } from '../../helpers';

const actions = new profileAction();

class Edit extends Component {

    constructor(props){
        super(props);
        this.state = {
            card_count: 0,
        };
    }

    componentDidMount = () => {
        this.id = this.props.match.params.id;
        this.props.setDataSource('edit');
        this.props.setFieldValue('id', this.id);
        this.props.requestItem('edit');
    }

    handleOnClickCheckbox = e => {
        e.preventDefault();
        this.props.setFieldValue('action', 'changed');
        this.props.requestItem('edit');
    }

    handleOnSubmit = () => {
        this.props.requestItem('update', true, (res) => {
            this.redirectToIndex()
        });
    }

    redirectToIndex = () => {
        this.props.history.push(this.props.dataSource.namespace);
    }

    render() {
        const { fields } = this.props;

        return (
            <Dashboard>
            {
                fields.name &&
                <React.Fragment>

                    <div className="row">
                        <CardBody cardTitle="Edit">
                            <Element field={fields.name} />
                            <Element field={fields.card_count} />
                            <Element field={fields.interval} />
                            <Element field={fields.nc_gap} onChange={this.handleOnClickCheckbox} />
                            <button type="button" onClick={this.handleOnSubmit} className="btn btn-primary">Submit</button>
                        </CardBody>
                    </div>
                    <div className="row">
                        <CardBody cardTitle="Subjects" gridClass="col-md-4 grid-margin stretch-card">
                            <Element field={fields.subject_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Modules" gridClass="col-md-4 grid-margin stretch-card">
                            <Element field={fields.module_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Chapters" gridClass="col-md-4 grid-margin stretch-card">
                            <Element field={fields.chapter_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                    </div>

                    <div className="row">
                        <CardBody cardTitle="Ans Type" gridClass="col-md-4 grid-margin stretch-card">
                            <Element field={fields.ans_type} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Sources" gridClass="col-md-4 grid-margin stretch-card">
                            <Element field={fields.source_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Tags" gridClass="col-md-4 grid-margin stretch-card">
                            <Element field={fields.tag_id} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                    </div>

                    <div className="row">
                        <CardBody cardTitle="Stage" gridClass="col-md-4 grid-margin stretch-card">
                            <Element field={fields.stage} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Level" gridClass="col-md-4">
                            <Element field={fields.level} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                        <CardBody cardTitle="Status" gridClass="col-md-4 grid-margin stretch-card">
                            <Element field={fields.status} onChange={this.handleOnClickCheckbox} />
                        </CardBody>
                    </div>

                    
                </React.Fragment>
            }
            </Dashboard>
        );
    }
}

export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    setFieldValue: actions.setFieldValue,
})(Edit);