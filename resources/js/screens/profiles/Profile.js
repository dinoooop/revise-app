import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Dashboard from '../../components/layouts/Dashboard';
import IndexProfile from './Index';
import CreateProfile from './Create';
import EditProfile from './Edit';

export default function Profile(props){
    const fields = props.fields;
    const { url } = props.match;
      
    return (
        <React.Fragment>
            <Route exact path={`${url}`} component={IndexProfile} />
            <Route path={`${url}/create`} component={CreateProfile} />
            <Route path={`${url}/edit/:id`} component={EditProfile} />
        </React.Fragment>
    );
}