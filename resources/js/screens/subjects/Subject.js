import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Dashboard from '../../components/layouts/Dashboard';
import IndexSubject from './Index';
import CreateSubject from './Create';
import EditSubject from './Edit';


export default function Subject(props){
    const fields = props.fields;
    const { url } = props.match;
      
    return (
        <React.Fragment>
            <Route exact path={`${url}`} component={IndexSubject} />
            <Route path={`${url}/create`} component={CreateSubject} />
            <Route path={`${url}/edit/:id`} component={EditSubject} />            
        </React.Fragment>
    );
}