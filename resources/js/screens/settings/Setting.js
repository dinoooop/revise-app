import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import settingAction from '../../actions/settingAction';

const actions = new settingAction();

class Setting extends Component {

    componentDidMount(){
        this.props.setDataSource('create');
        this.props.requestItem('create', false);
    }

    handleOnSubmit = e => {
        e.preventDefault();
        this.props.requestItem('store');
    }

    render() {
        
        let { fields } = this.props;

        return (
            <Dashboard>   
            {
                fields.interval &&
                <form onKeyPress={this.onKeyPress} onSubmit={this.handleOnSubmit}>
                    <div className="row">
                        <CardBody cardTitle="Settings" gridClass="col-lg-6">
                            <Element field={fields.interval} />
                            <Element field={fields.estimate_finish_day_count} />
                            <Element field={fields.show_subdocs} />
                            <button type="submit" className="btn btn-primary mr-2">Submit</button>
                        </CardBody>
                        
                    </div>
                </form>                
            }
            </Dashboard>
        );
    }
}


export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
})(Setting);
