import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import imageClassifyAction from '../../actions/imageClassifyAction';
import ModuleListTable from '../../boost/ModuleListTable';

const actions = new imageClassifyAction();

class ClassifyModuleList extends Component {

    componentDidMount(){
        this.props.setDataSource('create');
        this.props.requestItem('create', false);
    }

    render() {
        
        let { fields } = this.props;

   
        
        return (
            
            <Dashboard>
         
            {
                fields.module &&
                <CardBody cardTitle="Modules" gridClass="col-lg-6">
                    <div className="row">
                        <p className="color-red">Set profile before classify</p>
                        <ModuleListTable items={fields.module_list.value} />
                    </div>
                </CardBody>
            }

            </Dashboard>

        );
    }
}

export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    requestItem: actions.requestItem,
})(ClassifyModuleList);
