import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import imageClassifyAction from '../../actions/imageClassifyAction';
import {configData} from '../../dataSource/configData';

const actions = new imageClassifyAction();

class Classify extends Component {

    constructor(props){
        super(props);
        this.lastCardType = 'A';
    }

    componentDidMount = () => {
        document.addEventListener('keydown',this.keydownHandler);
        this.module = this.props.match.params.module;
        this.props.setDataSource('create');
        this.props.setFieldValue('module', this.module);
        this.props.setFieldValue('action', 'first');
        this.props.requestItem('create');
    }

    handleOnNext = e => {
        this.props.setFieldValue('action', 'next');
        this.props.requestItem('create');
    }

    handleOnPrev = e => {
        this.props.setFieldValue('action', 'prev');
        this.props.requestItem('create');
    }

    handleOnUpto = e => {

        this.props.setFieldValue('action', 'fast');
        this.props.requestItem('store', true, () => {
            this.props.setDataSource('create');
            this.props.setFieldValue('module', this.module);
            this.props.setFieldValue('action', 'first');
            this.props.requestItem('create');
        });

    }

    handleOnSubmit = e => {
        e.preventDefault();

        this.props.requestItem('store', true, () => {
            this.props.setDataSource('create');
            this.props.setFieldValue('module', this.module);
            this.props.setFieldValue('action', 'first');
            this.props.requestItem('create');
        });
    }

    keydownHandler = e => {
        
        console.log(e.which);

        if(e.which === 81){
            // Q
            this.props.setFieldValue('image_type', configData.imgType.que);
            this.handleOnSubmit(e);
        } else if(e.which === 65){
            // A
            this.props.setFieldValue('image_type', configData.imgType.ans);
            this.handleOnSubmit(e);
        } else if(e.which === 83 || e.which === 96){
            // S Or 0
            this.props.setFieldValue('image_type', configData.imgType.sub);
            this.handleOnSubmit(e);

        } else if(e.which === 13){
            // Enter 

            if(this.lastCardType == 'Q'){
                // this will be ans
                this.props.setFieldValue('image_type', configData.imgType.ans);
                this.lastCardType = 'A';
                this.handleOnSubmit(e);
            } else if(this.lastCardType == 'A'){
                this.props.setFieldValue('image_type', configData.imgType.que);
                this.lastCardType = 'Q';
                this.handleOnSubmit(e);
            }

        }
    }


    render() {
        
        let { fields } = this.props;
        
        return (
            
            <Dashboard>
            {
                fields.error_server &&
                fields.error_server.value &&
                <div className="row">
                    <CardBody cardTitle="Message" gridClass="col-lg-12">
                        <p>{fields.error_server_msg.value}</p>
                    </CardBody>
                </div>
            }

            {
                fields.image_url &&
                <React.Fragment>
                    <div className="row">
                        <CardBody cardTitle="Image to classify" gridClass="col-lg-12">
                            <div className="mrb-20">
                            <img src={fields.image_url.value} className="cardImage" />
                            </div>
                        </CardBody>
                    </div>

                    <div className="row">
                        <CardBody cardTitle="Options" gridClass="col-lg-6">
                            <Element field={fields.image_type} />                                    
                            <Element field={fields.tag_id} />
                        </CardBody>
                        <CardBody cardTitle="Ans Type" gridClass="col-lg-6">
                            <Element field={fields.ans_type} />
                            
                            <button type="button" onClick={this.handleOnSubmit} className="btn btn-primary mr-2">Submit</button>
                            {
                                fields.next_prev_active.value.prev &&
                                <button type="button" onClick={this.handleOnPrev} className="btn btn-info mr-2">Prev</button>
                            }
                            {
                                fields.next_prev_active.value.next &&
                                <button type="button" onClick={this.handleOnNext} className="btn btn-info mr-2">Next</button>
                            }
                        </CardBody>
                    </div>

                    <div className="row">
                        <CardBody cardTitle="Options" gridClass="col-lg-6">
                            <Element field={fields.upto} />
                            <button type="button" onClick={this.handleOnUpto} className="btn btn-info mr-2">Submit</button>
                        </CardBody>
                    </div>
                    
                </React.Fragment>
            }

            </Dashboard>
                  

        );
    }
}


export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    setFieldValue: actions.setFieldValue,
})(Classify);
