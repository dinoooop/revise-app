import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import {configData} from '../../dataSource/configData';
import generalAction from '../../actions/generalAction';

const actions = new generalAction();

class MathsClassify extends Component {

    constructor(props){
        super(props);
        this.lastImgType = 'A';
    }

    componentDidMount = () => {
        document.addEventListener('keydown',this.keydownHandler);
        this.props.setDataSource('maths_classify');
        this.props.requestItem('index');
    }

    handleOnSubmit = e => {
        e.preventDefault();

        this.props.requestItem('store', true, () => {
            this.props.setDataSource('maths_classify');
            this.props.requestItem('index');
        });
    }

    keydownHandler = e => {
        
        // console.log(e.which);

        if(e.which === 81){
            // Press Q
            this.lastImgType == 'Q';
            this.props.setFieldValue('image_type', configData.imgType.que);
            this.handleOnSubmit(e);
        } else if(e.which === 65){
            // Press A
            this.lastImgType == 'A';
            this.props.setFieldValue('image_type', configData.imgType.ans);
            this.handleOnSubmit(e);
        } else if(e.which === 83){
            // Press S
            this.lastImgType == 'S';
            this.props.setFieldValue('image_type', configData.imgType.sub);
            this.handleOnSubmit(e);
        } else if (e.which === 13){
            // Press Enter
            if(this.lastImgType == 'A' || this.lastImgType == 'S'){
                this.lastImgType == 'Q';
                this.props.setFieldValue('image_type', configData.imgType.que);
                this.handleOnSubmit(e);
            } else {
                this.lastImgType == 'A';
                this.props.setFieldValue('image_type', configData.imgType.ans);
            }
        }
    }


    render() {
        
        let { fields } = this.props;
        
        return (
            
            <Dashboard>
            {
                fields.error_server &&
                fields.error_server.value &&
                <div className="row">
                    <CardBody cardTitle="Message" gridClass="col-lg-12">
                        <p>{fields.error_server_msg.value}</p>
                    </CardBody>
                </div>
            }

            {
                fields.image_url &&
                <React.Fragment>
                    <div className="row">
                        <CardBody cardTitle="Image to classify" gridClass="col-lg-12">
                            <div className="mrb-20">
                            <img src={fields.image_url.value} className="cardImage" />
                            </div>
                        </CardBody>
                    </div>
                </React.Fragment>
            }

            </Dashboard>
                  

        );
    }
}


export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    setFieldValue: actions.setFieldValue,
})(MathsClassify);
