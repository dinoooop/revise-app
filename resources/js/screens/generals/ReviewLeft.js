import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import generalAction from '../../actions/generalAction';
import LabelValueTable from '../../components/tables/LabelValueTable';
import ReviewLeftTable from '../../boost/ReviewLeftTable';

const actions = new generalAction();

class ReviewLeft extends Component {

    componentDidMount(){
        this.props.setDataSource('review_left');
        this.props.requestItem('index');
    }

    componentWillUnmount(){
        this.props.resetStoreForItems();
    }

    render() {
        
        let { fields } = this.props;
        
        return (
                
            <React.Fragment>
              
                <Dashboard>
                    <div className="row">
                    {
                        fields.items &&
                        <CardBody cardTitle="Review Left">
                            <ReviewLeftTable items={fields.items.value} />
                        </CardBody>
                    }
                    </div>
                </Dashboard>
              
            </React.Fragment>
            
        );
    }
}


export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    resetStoreForItems: actions.resetStoreForItems,
})(ReviewLeft);
