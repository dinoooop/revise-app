import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import generalAction from '../../actions/generalAction';
import LabelValueTable from '../../components/tables/LabelValueTable';
import { ListNav } from '../../components/navs/Mini';

const actions = new generalAction();

class OtherLinks extends Component {

    componentDidMount(){
        
    }

    componentWillUnmount(){
        
    }

    render() {
        
        
        
        return (
                
            <React.Fragment>
              
                <Dashboard>   
                
                    <CardBody cardTitle="Other Links" gridClass="col-lg-6">
                    <nav>
                        <ul className="nav">
                            <ListNav title="Sources" icon="fas fa-book-open" href="/sources" />
                            <ListNav title="Tags" icon="fas fa-tags" href="/tags" />
                            <ListNav title="Maths Classify" icon="fas fa-divide" href="/maths-classify" />
                            <ListNav title="Import" icon="fas fa-file-import" href="/cards/csv-import" />
                            <ListNav title="Classify" icon="fas fa-divide" href="/classify" />
                            <ListNav title="SMC" icon="fas fa-th-large" href="/smc" />
                            <ListNav title="Settings" icon="fas fa-cogs" href="/settings" />
                        </ul>
                        </nav>
                    </CardBody>
                
                </Dashboard>
              
            </React.Fragment>
            
        );
    }
}


export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    resetStoreForItems: actions.resetStoreForItems,
})(OtherLinks);
