import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import CardBody from '../../components/layouts/CardBody';
import generalAction from '../../actions/generalAction';
import ReviewHistoryViewTable from '../../boost/ReviewHistoryViewTable';
import StatisticsLevelList from '../../boost/StatisticsLevelList';

const actions = new generalAction();

class ReviewHistoryView extends Component {

    componentDidMount(){
        this.props.setDataSource('review_history_view');
        this.props.requestItem('index');
    }

    componentWillUnmount(){
        this.props.resetStoreForItems();
    }

    render() {
        
        let { fields } = this.props;
    
        return (
                
            <React.Fragment>
              
                <Dashboard>   
                {
                    
                    fields.histories &&
                    fields.histories.value &&
                    <CardBody gridClass="col-lg-12">
                        <ReviewHistoryViewTable items={fields.histories.value} />
                    </CardBody>
                    
                }
                </Dashboard>
              
            </React.Fragment>
            
        );
    }
}


export default connect(state => ({
    dataSource: state.shared.dataSource,
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    resetStoreForItems: actions.resetStoreForItems,
})(ReviewHistoryView);
