import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dashboard from '../../components/layouts/Dashboard';
import PageForbidden from '../../components/blend/PageForbidden';
import Element from '../../components/formFields/Element';
import ListLink from '../../components/lists/ListLink';
import CardBody from '../../components/layouts/CardBody';
import smcAction from '../../actions/smcAction';

const actions = new smcAction();

class Create extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            showCreate: false,
        };
    }

    componentDidMount(){
        this.props.setDataSource('create');
        this.props.requestItem('create', false);
    }

    handleOnClickCheckboxList = (param) => {
        this.props.requestItem('create');
        this.props.setLastRadio(param);
    }

    handleOnSubmitAdd = e => {
        e.preventDefault();
        
        this.props.requestItem('store', true, res => {
            this.props.viewAfterSubmitAdd(res.new);
        });
        
    }

    handleOnSubmitEdit = e => {
        e.preventDefault();

        this.props.setFieldValue('id', this.props.fields.id_edit.value);
        this.props.requestItem('update', true, res => {
            this.props.viewAfterSubmitEdit(res.new);
        });

    }

    handleOnClickAdd = () => {
        this.setState({ showCreate: true });
    }

    handleOnCancel = e => {
        e.preventDefault();
        this.setState({ showCreate: false });
    }

    render() {

        const { fields } = this.props;
        const { showCreate } = this.state;

        return (
            <Dashboard>   

                {
                    fields.subject_id &&
                    <div className="row">
                        <CardBody cardTitle="Subject" gridClass="col-md-4">
                            <Element field={fields.subject_id} onChange={() => this.handleOnClickCheckboxList('subject')} />
                            <Element field={fields.subject_button} onChange={this.handleOnClickAdd} />
                        </CardBody>

                        <CardBody cardTitle="Module" gridClass="col-md-4">
                            <Element field={fields.module_id} onChange={() => this.handleOnClickCheckboxList('module')} />
                            <Element field={fields.module_button} onChange={this.handleOnClickAdd} />
                        </CardBody>

                        <CardBody cardTitle="Chapter" gridClass="col-md-4">
                            <Element field={fields.chapter_id} onChange={() => this.handleOnClickCheckboxList('chapter')} />
                            <Element field={fields.chapter_button} onChange={this.handleOnClickAdd} />
                        </CardBody>           
                    </div>
                }

                <div className="row">
                    <CardBody cardTitle="Edit" gridClass="col-md-6">
                        <Element field={fields.name_edit} />
                        <Element field={fields.submit_edit} onChange={this.handleOnSubmitEdit} />
                    </CardBody>
                    {
                        showCreate &&    
                        <CardBody cardTitle="create" gridClass="col-md-6">
                            <Element field={fields.name} />
                            <Element field={fields.submit_add} onChange={this.handleOnSubmitAdd} />
                            <button onClick={this.handleOnCancel} className="btn btn-light">Cancel</button>
                        </CardBody>
                    }
                </div>

            </Dashboard>
        );
    }
}

export default connect(state => ({
    fields: state.shared.fields,
}), {
    setDataSource: actions.setDataSource,
    requestItem: actions.requestItem,
    setFieldValue: actions.setFieldValue,
    viewAfterSubmitAdd: actions.viewAfterSubmitAdd,
    viewAfterSubmitEdit: actions.viewAfterSubmitEdit,
    setLastRadio: actions.setLastRadio,
})(Create);


