import React, { Component } from 'react';
import { Route } from "react-router-dom";
import Dashboard from '../../components/layouts/Dashboard';
import CreateSMC from './Create';

export default function SMC(props){
    const fields = props.fields;
    const { url } = props.match;
      
    return (
        <React.Fragment>
            <Route exact path={`${url}`} component={CreateSMC} />
        </React.Fragment>
    );
}