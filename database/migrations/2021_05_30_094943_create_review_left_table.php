<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewLeftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_left', function (Blueprint $table) {
            $table->increments('id');
            $table->date('review_date');
            $table->integer('fy');
            $table->integer('rt');
            $table->integer('sy');
            $table->integer('st');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_left');
    }
}
