<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->text('que');
            $table->text('ans');
            $table->tinyInteger('stage')->default(1);
            $table->boolean('is_fy')->nullable();
            $table->date('next_review_date')->nullable();
            $table->tinyInteger('last_interval_order')->nullable();
            $table->tinyInteger('level')->default(1);
            $table->tinyInteger('status')->default(1);
            $table->integer('subject_id');
            $table->integer('module_id');
            $table->integer('chapter_id')->nullable();
            $table->integer('source_id')->nullable();
            $table->tinyInteger('ans_type')->default(1);
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
