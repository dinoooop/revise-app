<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMathsClassifyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maths_classify', function (Blueprint $table) {
            $table->increments('id');
            $table->text('filename');
            $table->tinyInteger('is_classified')->nullable();
            $table->text('filepath');
            $table->integer('subject_id');
            $table->integer('module_id');
            $table->integer('chapter_id')->nullable();
            $table->tinyInteger('ans_type')->default(1);
            $table->integer('source_id')->nullable();
            $table->tinyInteger('stage')->default(1);
            $table->tinyInteger('level')->default(1);
            $table->tinyInteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maths_classify');
    }
}
