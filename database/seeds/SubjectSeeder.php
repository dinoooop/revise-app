<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Helpers\Factory\Stock;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach (Stock::subjects() as $value) {
	        DB::table('subjects')->insert([
	            'name' => $value,
	        ]);
	    }

        foreach (Stock::modules() as $key => $value) {
            foreach ($value as $val) {
                DB::table('modules')->insert([
                    'subject_id' => $key,
                    'name' => $val,
                ]);
            }
        }

        foreach (Stock::chapters() as $key => $value) {
            foreach ($value as $val) {
                DB::table('chapters')->insert([
                    'module_id' => $key,
                    'name' => $val,
                ]);
            }
        }

        foreach (Stock::tags() as $key => $value) {
            DB::table('tags')->insert($value);
        }

        foreach (Stock::options() as $key => $value) {
            DB::table('options')->insert($value);
        }

        foreach (Stock::sources() as $value) {
            DB::table('sources')->insert([
                'name' => $value,
            ]);
        }
    }
}
