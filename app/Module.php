<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
	protected $fillable = [
		'name',
		'subject_id',
	];

	public $timestamps = false;
}
