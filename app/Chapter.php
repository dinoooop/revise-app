<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $fillable = [
    	'name',
		'module_id',
	];

	public $timestamps = false;
}
