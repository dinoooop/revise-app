<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Facades\App\Helpers\Load\FirstData;
use Facades\App\Helpers\CardManager;
use Facades\App\Helpers\PostponeManager;


class BasicCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:basic {type?} {arg1?} {arg2?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert cards from xlsx';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        if($type == 'reset'){

            // load Basic data
            FirstData::dropTables();
            FirstData::createTables();
            FirstData::setBasicData();

        } else if($type == 'cards') {

            // Load Basic data + Dummy cards
            FirstData::dropTables();
            FirstData::createTables();
            FirstData::setBasicData();
            FirstData::setDummyCards();

        } else if($type == 'option'){

            // reset only option table
            FirstData::resetOptions();

        } else if($type == 'backup'){

            // backup DB
            $bkpfilename = $this->argument('arg1');
            FirstData::backup($bkpfilename);

        } else if($type == 'restore'){

            // Restore last backup
            $latestSqlFile = $this->argument('arg1');
            FirstData::restoreLastBackup($latestSqlFile);

        } else if($type == 'five'){
            
            // Delete sql backups and maintaine last five
            FirstData::lastFive();
        } else if($type == 'custom'){
            
            // Delete sql backups and maintaine last five
            FirstData::custom();
        } else if($type == 'postpone'){

            // postpone the card for n days
            $days = $this->argument('arg1');
            $proflie_ids = $this->argument('arg2');

            if(is_null($days)){
                $days = 1;
            }

            PostponeManager::init($days, $proflie_ids);
            
        }
        
        
    }
}
