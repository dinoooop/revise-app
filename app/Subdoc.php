<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subdoc extends Model
{
    protected $fillable = ['card_id', 'filename'];
}
