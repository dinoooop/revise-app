<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
    	'option_key',
		'option_value',
    ];

    public $timestamps = false;

    public static function get($optionKey){
    	$option = self::where('option_key', $optionKey)->first();
    	$value = isset($option->option_value) ? $option->option_value : null;

        if(is_null($value)){
            return $value;
        }

        $data = @unserialize($value);
        return ($data !== false) ? $data : $value;
    }

    public static function set($optionKey, $optionValue){
        
        if(is_array($optionValue)){
            $optionValue = serialize($optionValue);
        }

    	$option = self::where('option_key', $optionKey)->first();

        if(!isset($option->option_value)){
            self::create([
                'option_key' => $optionKey,
                'option_value' => $optionValue
            ]);
        } else {
        	$option->option_value = $optionValue;
        	$option->save();
        }

    }
}
