<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewLeft extends Model
{
    protected $table = 'review_left';
    protected $fillable = [
    	'review_date',
		'fy',
		'rt',
	];
	public $timestamps = false;

}
