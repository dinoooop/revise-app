<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
    	'name',
		'description',
    ];

    public $timestamps = false;

    public function cards()
	{
		return $this->belongsToMany('App\Card');
	}
}
