<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Helper;

class ReviewHistory extends Model
{
    protected $table = 'review_history';

    protected $fillable = ['card_id', 'review_date', 'act_interval', 'result'];

    public function tags()
	{
		return $this->belongsToMany('App\Tag', 'card_tag', 'card_id', 'tag_id');
	}
    
    
}
