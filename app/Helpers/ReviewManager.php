<?php 

namespace App\Helpers;

use App\Option;
use App\Card;
use App\ReviewHistory;
use App\ReviewLeft;
use App\Helpers\Factory\Stock;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use App\Helpers\Chief;

// Facades

class ReviewManager extends Chief {

	function __construct(){
		parent::__construct();
		$this->interval = $this->profile->interval;
	}

	// Handle
	// return boolean
	// needs to update 3 tables
	// cards, review_history, review_left
	// request >> status, tag_id, stage, result, review_mode
	public function cardReviewed($id, $request){
		$this->card_id = $id;
		$this->request = $request;

		if($this->request->status == 'buried'){
			// Skip this card
			return false;
		}

		$this->card = Card::find($this->card_id);
		
		$this->card->tags()->sync($this->request->tag_id);
		

		// suspend the question
		if($this->request->status == Stock::status('suspend')){
			$this->card->update(['status' => $this->request->status]);
			return true;
		}

		if($this->request->append_txt != ''){
            $this->card->que = trim($this->card->que) . ' ' . $this->request->append_txt;
            $this->card->save();
        }
        
		$this->setReviewHistory();
		$this->setStage();
		$this->setStatus();
		$this->setIsFy();
		$this->setNextReviewDate();
		$this->countDown();
		
		$this->card->update([
            'stage' => $this->card->stage,
            'status' => $this->card->status,
            'level' => $this->card->level,
            'is_fy' => $this->card->is_fy,
            'last_interval_order'  => $this->card->last_interval_order,
            'next_review_date'  => $this->card->next_review_date,
            'last_review_date'  => $this->card->last_review_date,
        ]);

        $this->insertReviewHistory();
		return true;
	}

	// Undo last review as success/failed 
	// (Accidentally pressed Enter/F Key)
	public function undoLastReview($request){

		$this->request = $request;

		// identify the prev state of card frome review history
		// revert table cards for prev state only column [level, last_interval_order]
		// arrange new request
		// create new review history update card

		$history = ReviewHistory::latest()->first();
		$this->card_id = $history->card_id;

		
		$this->card = Card::find($this->card_id);

		if($history->review_date !== $this->today){
			exit();
		}

		if(is_null($this->card->last_interval_order)){
			$this->card->level = $this->card->level - 1;
			$this->card->last_interval_order = count($this->interval) - 1;
		} else {
			$this->card->last_interval_order = $this->card->last_interval_order - 1;
		}

		$this->card->save();
		$history->delete();

		// Review as F/S
		if($this->request->action == 'failed-last'){
			$this->request->result = Stock::result('failed');
		} else {
			$this->request->result = Stock::result('success');
		}

		// Hard to find what was prev stage so
		// keep current value and re-excute the code for result = failed/success,
		// that will update stage as normal workflow
		$this->request->stage = $this->card->stage;

		$this->setReviewHistory();
		$this->setStage();
		$this->setIsFy();
		$this->setNextReviewDate();
		
		$this->card->update([
            'stage' => $this->card->stage,
            'level' => $this->card->level,
            'is_fy' => $this->card->is_fy,
            'last_interval_order'  => $this->card->last_interval_order,
            'next_review_date'  => $this->card->next_review_date,
        ]);

        $this->insertReviewHistory();
		return true;


	}

	public function insertReviewHistory(){
		$reviewHistory = ReviewHistory::where('card_id', $this->card_id)
				->where('review_date', $this->today)
				->first();

		if($reviewHistory){

			$reviewHistory->update([
	            'result' => $this->result
			]);

		} else {
			ReviewHistory::create([
	            'card_id' => $this->card_id,
	            'review_date'  => $this->today,
	            'act_interval' => $this->act_interval,
	            'result' => $this->result
        	]);
		}
    }

	public function setStage(){

		if(!isset($this->request->stage)){
			// This may be removing KW for card
			if($this->card->stage == Stock::stage('KW')){
				$this->card->stage = Stock::stage('D1');
			}
		}

		if($this->request->stage == Stock::stage('KW')){
			$this->card->stage = $this->request->stage;
		}


		if(is_null($this->result)){
			return false;
		}

		if($this->act_interval < 60){
			return false;
		}

		if($this->act_interval >= 60 && $this->act_interval < 120){
			$this->stage_N('S60', 'F60', 'H60', 'E60');
		}

		if($this->act_interval >= 120){
			$this->stage_N('S120', 'F120', 'H120', 'E120');
		}

	}

	public function stage_N($success, $failed, $hard, $easy){
		if(
			$this->card->stage == Stock::stage('KW') 
			&& $this->result == Stock::result('success')
		){
			// reviewing KW card after 60 days as success
			// not need any upgradation in stage
			return false;
		}

		// Upgrade to S60
		$SCount = $FCount = 0;
		if($this->result == Stock::result('success')){
			$SCount++;
			$this->card->stage = Stock::stage($success);
		} else {
			$FCount++;
			$this->card->stage = Stock::stage($failed);
		}

		// Check any chance for upgrade to E60

		$last_n_sfcount = Stock::general('last_n_sfcount');

		// e.g last_n_sfcount is four 
		// Slice last three history and include current review too 
		// i.e consiering last four reviews
		$slice = $last_n_sfcount - 1;

		$lastNReviewHistories = $this->card
			->reviewHistory
			->where('act_interval', '>=', Stock::general('effective_recall_interval'))
			->sortByDesc('review_date')
			->slice(0, $slice)
			->all();

		
		foreach ($lastNReviewHistories as $key => $value) {

			if($value->result == Stock::result('success')){
				$SCount++;
			}

			if($value->result == Stock::result('failed')){
				$FCount++;
			}

		}

		if($SCount == $last_n_sfcount){
			$this->card->stage = Stock::stage($easy);
		}

		if($FCount == $last_n_sfcount){
			$this->card->stage = Stock::stage($hard);
		}

	}

	public function setStatus(){

		if(isset($this->request->status)){
			$this->card->status = $this->request->status;
		}

	}

	public function setNextReviewDate(){

		$this->card->last_review_date = $this->today;

		if($this->result == Stock::result('failed')){
			// No change in last_interval_order 
			// Update next_review_date to very next day
			$this->card->next_review_date = Helper::addTodayWith(1);
			return;
		}

		// result is success or null - do following

        $newIntervalOrder = is_null($this->card->last_interval_order) ? 0 : $this->card->last_interval_order + 1;
        
        if(isset($this->interval[$newIntervalOrder])){
        	$interval = $this->interval[$newIntervalOrder];
        	$this->card->next_review_date = Helper::addTodayWith($interval);
        	$this->card->last_interval_order = $newIntervalOrder;
        } else {
        	// upgrade the question to next level
        	$this->card->level = $this->card->level + 1;
        	$this->card->next_review_date = null;
        	$this->card->last_interval_order = null;
        }


        
    }

    /**
    *
    * Set val required in table review_history after review
    */
    public function setReviewHistory(){

    	$this->history = $this->card
    		->reviewHistory
    		->sortByDesc('review_date')
    		->first();
    	
        if(is_null($this->history)){
        	// at first time
            $this->act_interval = null;
            $this->result = null;
        } else {
            $this->act_interval = Helper::dateDiff($this->today, $this->history->review_date);

            // By default result is success
            // while using mouse forget to check success
            $this->result = isset($this->request->result)? $this->request->result : Stock::result('success');
        }

        if($this->request->stage == Stock::stage('KW')){
        	// result should be success
        	// otherwise card will count in new cards
			$this->result = Stock::result('success');
		}
    }

    public function setIsFy(){
    	if(isset($this->result) && $this->result == Stock::result('failed')){
			$this->card->is_fy = 1;
		} else {
			$this->card->is_fy = null;
		}
    }

    public function countDown(){

    	if($this->request->review_mode == 'NC'){
    		return false;
    	}

    	$review = ReviewLeft::where([
    		'review_date' => $this->today,
    	])->first();

    	if(!isset($review->fy)){
    		return false;
    	}

    	if($this->request->review_mode == 'FY'){
    		$review->fy--;
    	} else if($this->request->review_mode == 'RT')  {
    		$review->rt--;
    	}

    	$review->save();
    	
    }

}
