<?php 

namespace App\Helpers;

use App\Card;
use App\Subject;
use App\Module;
use App\Chapter;
use App\Source;
use App\Subdoc;
use App\Option;
use App\Tag;
use App\MathsClassify;
use App\Helpers\Factory\Stock;
use Illuminate\Support\Facades\Storage;

// One time use class
// Must completed
// Todo - Hard, Medium

class MathsClassifyManager {
	
	function __construct(){

		$this->saveDirUrl = Storage::url('classify-img-save');

		// Config data
		$this->mathDir = Storage::path('public/classify-img-save/maths');
		$this->subject = 'maths';
		$this->subject_id = 1;
		$this->ans_type = Stock::ansType('write');
		$this->source_id = 8;
		$this->level = 1;
		$this->status = Stock::status('active');

		// Config
		// H60 - completed
		$this->selected_stage = Stock::stage('F60'); 
		$this->selected_stage_name = 'hard';
		$this->selected_tag_id = $this->getTagId('hard');
		
	}

	public function init(){

		$modules = glob($this->mathDir . '/*');

		foreach ($modules as $key => $module) {

			$this->setModule($module);
			
		}
	}

	public function setModule($module){
		$this->module = basename($module);
		$chapters = glob($module . '/*');

		foreach ($chapters as $key => $chapter) {
			$this->setChapter($chapter);
		}
	
	}

	public function setChapter($chapter){

		$this->chapter = basename($chapter);
		$stages = glob($chapter . '/*');

		foreach ($stages as $key => $stage) {
			
			$this->setStage($stage);
			
		}
	
	}

	public function setStage($stage){
		$this->stage = basename($stage);
		$this->stage_id = $this->getStageId(basename($stage));
		if(!$this->stage){
			return;
		}

		$files = glob($stage . '/*');

		foreach ($files as $key => $file) {
			if($this->stage == $this->selected_stage_name){
				$this->filename = basename($file);
				$this->setValues();
				echo $this->insert() . ' ';
			}
		}
	
	}

	public function setValues(){
		$this->filepath = "{$this->subject}/{$this->module}/{$this->chapter}/{$this->stage}/{$this->filename}";

		$chapter = Chapter::where('name', $this->chapter)->first();

		if($chapter){
			$this->chapter_id = $chapter->id;
			$this->module_id = $chapter->module_id;
		} else {
			$module = Module::where('name', $this->module)->first();

			if($module){

				$this->module_id = $module->id;
				Chapter::create([
					'name' => $this->chapter,
					'module_id' => $this->module_id,
				]);

			} else {
				// No Module, Chapter 

				$module = Module::create([
					'name' => $this->module,
				]);

				$this->module_id = $module->id;
				

				$chapter = Chapter::create([
					'name' => $this->chapter,
					'module_id' => $this->module_id,
				]);

				$this->chapter_id = $chapter->id;

			}
		}
	}

	public function insert(){

		$row = [];

		$row['filename'] = $this->filename;
		$row['filepath'] = $this->filepath;
		$row['subject_id'] = $this->subject_id;
		$row['module_id'] = $this->module_id;
		$row['chapter_id'] = $this->chapter_id;
		$row['ans_type'] = $this->ans_type;
		$row['source_id'] = $this->source_id;
		$row['stage'] = $this->stage_id;
		$row['level'] = $this->level;
		$row['status'] = $this->status;

		return MathsClassify::create($row)->id;
	
	}


	public function getStageId($stage){
		$stage = strtolower($stage);

		$stages = [
			'must' => Stock::stage('H60'),
			'hard' => Stock::stage('F60'),
			'medium' => Stock::stage('S60'),
			'easy' => Stock::stage('E60'),
		];

		return isset($stages[$stage])? $stages[$stage] : false;
	}


	public function getFirst(){
		$mathCard = MathsClassify::where('stage', $this->selected_stage)
			->where('is_classified', null)
			->orderBy('filename', 'asc')
			->first();

		if(!$mathCard){
			$data['error_server'] = true;
            $data['error_server_msg'] = "No images available for this stage";
            return $data;
		}

		$mathCard->image_url = Storage::url('classify-img-save/' . $mathCard->filepath);
		return $mathCard;
	
	}


	// handle - store the file table cards
	public function move($request){


		$this->request = $request;
		$mathCard = MathsClassify::find($this->request->id);

		switch ($this->request->image_type) {
			case 'que':
				$data = $mathCard->toArray();

				// set the stage to default
				$data['stage'] = Stock::stage('D1');

				$data['que'] = $mathCard->filepath;
				$data['ans'] = 'sample text';

				$card = Card::create($data);
				$card->tags()->sync([$this->selected_tag_id]);
				Option::set('image_last_card_id', $card->id);
				break;

			case 'ans':
				$id = Option::get('image_last_card_id');
				$card = Card::find($id);
				$card->ans = $mathCard->filepath;
				$card->save();
				break;

			case 'sub':
				$row = [];
				$row['card_id'] = Option::get('image_last_card_id');
				$row['filename'] = $mathCard->filepath;
				Subdoc::create($row);
				break;
		}

		$mathCard->is_classified = 1;
		$mathCard->save();

		return true;
		
	}


	// Search from console
	public function nonConsiderDir(){

		$impStages = ['easy', 'hard', 'medium', 'must'];
		$taken = $path = $count = [];

		$modules = glob($this->mathDir . '/*');
		foreach ($modules as $key => $module) {
			$chapters = glob($module . '/*');
			foreach ($chapters as $key => $chapter) {
				$stages = glob($chapter . '/*');
				foreach ($stages as $key => $stagePath) {
					$stage = basename($stagePath);
					if($stage == 'stock'){
						$files = glob($stagePath . '/*');
						if(count($files) > 0){

							$count[] = count($files);
							$countpath[]= $stagePath;
						}

					}
					if(!in_array($stage, $impStages) && !in_array($stage, $taken)){
						$taken[] = $stage;
						$path[]= $stagePath;
					}
				}
			}
		}

		print_r($countpath);
	
	}


	public function getTagId($name){
		return Tag::where('name', $name)->first()->id;
    }

}
