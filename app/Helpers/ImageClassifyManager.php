<?php 

namespace App\Helpers;

use App\Helpers\Helper;
use App\Helpers\Serve;
use App\Card;
use App\Module;
use App\Chapter;
use App\Subdoc;
use App\Option;
use App\Helpers\Factory\Stock;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class ImageClassifyManager  {
	
	function __construct(){
		$this->inputDir = Storage::path('public/classify-img-input');
		$this->inputDirUrl = Storage::url('classify-img-input');
		$this->saveDir = Storage::path('public/classify-img-save');
		$this->ignoreDir = Storage::path('public/classify-img-ignore');
		$this->noteDir = Storage::path('public/classify-img-note');
        $profileSettings = Serve::getCurrentProfileSettings()->settings;
        $this->profileSettings = Serve::sortSettings($profileSettings);
	}

	

	public function setRequest($request){
		$this->request = $request;
		if(isset($request->image_url)){
			$this->request->current = $request->image_url;
		}
	}

	public function getFirst(){

		$data = [];

		$this->setInputDir();
		$this->setImageList();

		if(isset($this->imgList[0])){
			$data['image_url'] = $this->getUrl($this->imgList[0]);
			$data['next_prev_active'] = $this->nextPrevIsActive($data['image_url']);
		} else {
			$data['error_server'] = true;
            $data['error_server_msg'] = "No images in input directory";
		}

		return $data;
	}


	public function getNext(){

		$data = [];

		$this->setInputDir();
		$this->setImageList();

		$index = $this->getIndex($this->request->current);
		$img = $this->imgList[$index + 1];

		$data['image_url'] = $this->getUrl($img);
		$data['next_prev_active'] = $this->nextPrevIsActive($img);
		return $data;
	}

	public function getPrev(){

		$data = [];

		$this->setInputDir();
		$this->setImageList();

		$index = $this->getIndex($this->request->current);
		$img = $this->imgList[$index - 1];

		$data['image_url'] = $this->getUrl($img);
		$data['next_prev_active'] = $this->nextPrevIsActive($img);
		return $data;
	}

	public function getModuleList(){

		$data = [];

        $glob = glob($this->inputDir . '/*');
        $modules = [];
        foreach ($glob as $key => $value) {
        	if(is_dir($value)){
            	$row['name'] = basename($value);
            	$row['path'] = $value;
            	$row['count'] = count(glob($value . '/*'));
            	$modules[] = $row;
        	}
        }
        
		$data['module_list'] = $modules;
        return $data;
	
	}

	public function fastInsert(){

		$data = [];

		$this->setInputDir();
		$this->setImageList();

		foreach ($this->imgList as $key => $value) {

			
			$this->request->current = $value;
			$this->request->image_type = ($key % 2 == 0) ? 'que' : 'ans';

			$data[] = $this->saveFile(false);
			
			if($this->request->upto == basename($value)){
				break;
			}
		}

		return $data;
	
	}

	public function saveFile($set = true){
		
		$result = null;

		if($set === true){
			// when the function work in loop not need to run this
			$this->setInputDir();
			$this->setImageList();
		}

		if(!isset($this->request->image_type)){
			return $result;
		}
		
        $this->lastCardId = Option::get('image_last_card_id');
		$this->filename = $this->getPath($this->request->current);

		$row = [];

		switch ($this->request->image_type) {
			case 'que':
				$this->moveFile();
				
				$row['que'] = $this->newFilename; // Que OR Ans
				$row['ans'] = 'Sample Text';
				
				$row = array_merge($this->profileSettings, $row);

				$row['status'] = $row['status']?? Stock::status('active');
				$row['ans_type'] = $row['ans_type']?? Stock::ansType('vocal');

				$card = Card::create($row);
				$this->setLastId($card->id);

				if(isset($this->request->tag_id)){
					$card->tags()->sync($this->request->tag_id);
				}

				$result = $card->id;
				break;

			case 'ans':
				$this->moveFile();
				$row['ans'] = $this->newFilename;
				Card::find($this->lastCardId)->update($row);
				$result = $this->lastCardId;
				break;

			case 'sub':
				$this->moveFile();
				$row['card_id'] = Option::get('image_last_card_id');
				$row['filename'] = $this->newFilename;
				$result = Subdoc::create($row)->id;
				break;

			case 'no_answer':
				// Set the question itself be the answer
				$this->copyQueFile();
				$row['ans'] = $this->newFilename;
				$this->card->update($row);
				$result = $this->lastCardId;
				break;

			case 'ignore':
				$result = rename(
					$this->filename, 
					$this->ignoreDir . '/' . basename($this->filename)
				);
				break;

			case 'note':
				$result = $this->moveNoteFile();
				break;
			
		}

		return $result;
		
	}




	public function moveNoteFile(){
		$fileName = basename($this->filename);
        $file = $this->inputDir . '/' . $fileName;
        
        $module = $this->getCurrentModule();
        $chapter = $this->getCurrentChapter();
        
        $module = str_replace(' ', '-', strtolower($module));
        $chapter = str_replace(' ', '-', strtolower($chapter));

        $newPath = "public/classify-img-note/{$module}/{$chapter}";
        Storage::makeDirectory($newPath);
        $newPath = Storage::path($newPath);

        $newFileName = Helper::dtfName($fileName);
        $newFile = $newPath . '/' . $newFileName;
        
        return rename($file, $newFile);
	
	}


	public function getCurrentModule(){
		$module = Module::find($this->profileSettings['module_id']);
		return isset($module) ? $module->name : null;
	}

	public function getCurrentChapter(){
		$chapter = Chapter::find($this->profileSettings['chapter_id']);
		return isset($chapter) ? $chapter->name : null;
	}


	public function setImageList(){		 
		$this->imgList = glob($this->inputDir . "/*");
	}

	public function getUrl($img){
		return $this->inputDirUrl . '/' . basename($img);
	}

	public function getPath($img){
		return $this->inputDir . '/' . basename($img);
	}

	public function getIndex($file){
		$file = basename($file);
		foreach ($this->imgList as $key => $value) {
			if($file == basename($value)){
				return $key;
			}
		}
		return false;
	}

	public function nextPrevIsActive($img){
		$index = $this->getIndex($img);
		$data['next'] = isset($this->imgList[$index + 1]);
		$data['prev'] = isset($this->imgList[$index - 1]);
		return $data;
	}

	public function setInputDir(){
		$this->inputDir = $this->inputDir . '/' . $this->request->module;
		$this->inputDirUrl = $this->inputDirUrl . '/' . $this->request->module;
	}
	
	public function requestFirst($path = false){
		$data = [];

		$glob = glob($this->inputDir . "/*");

		if(!isset($glob[0])){
			$data['error_server'] = true;
            $data['error_server_msg'] = 'No images in input directory';
			return $data;
		}

		if($path){
			return $glob[0];
		} else {
			$data['image_url'] = $this->inputDirUrl 
			. '/' . basename($glob[0]);
			return $data;
		}
		
	}

	public function setLastId($id){
		Option::set('image_last_card_id', $id);
	}

	public function moveFile(){
		
		$this->newFilename = Helper::dtfName($this->filename);
        
		rename(
			$this->filename, 
			$this->saveDir . '/' . $this->newFilename
		);
	}

	public function copyQueFile(){
		$this->card = Card::find($this->lastCardId);
		$this->filename = $this->saveDir . '/' . $this->card->que;
		$this->newFilename = Helper::dtfName($this->filename);

		copy(
			$this->filename, 
			$this->saveDir . '/' . $this->newFilename
		);
	}

}
