<?php

namespace App\Helpers;

use App\Card;
use App\Profile;
use Illuminate\Support\Facades\DB;
use App\Helpers\CardDirector;

// Facedes

class PostponeManager extends CardDirector {

	function __construct(){
		parent::__construct();
	}

	public function init($days, $proflie_id = null){

		$this->days = $days;
		$this->proflie_id = $proflie_id;

		if(!is_null($proflie_id)){
			$this->byProfile();
		}else {
			$this->all();
		}

	}

	// postpone by profile
	public function byProfile(){

		if(strpos($this->proflie_id, ',') !== false){
			$proflie_ids = explode(',', $this->proflie_id);
			foreach ($proflie_ids as $key => $value) {
				$this->bySingleProfile($value);
			}
		} else {
			$this->bySingleProfile($this->proflie_id);
		}

	}

	public function bySingleProfile($id){
		$profile = Profile::find($id);

        if(isset($profile->settings)){
            $this->settings = unserialize($profile->settings);
        } else {
        	exit("Profile not found");
        }

        $this->count = [];

        $this->query = Card::query();
        $this->where = $this->settings;
        $this->applyBasicWhere();
        $this->query->whereNotNull('next_review_date');
        $this->query->chunkById(50, function ($cards) {

        	foreach ($cards as $key => $card) {
                $data['next_review_date'] = date(
                    'Y-m-d', 
                    strtotime($card->next_review_date . "{$this->days} days")
                );

                $this->count[] = Card::find($card->id)->update($data);
            }

        });

        $count = count($this->count);
        echo "Total {$count} cards have postponed for profile {$id}. \n";
	}

	public function all(){

        $this->count = [];

        DB::table('cards')
            ->whereNotNull('next_review_date')
            ->chunkById(50, function ($cards) {
                foreach ($cards as $key => $card) {
                    $data['next_review_date'] = date(
                        'Y-m-d', 
                        strtotime($card->next_review_date . "{$this->days} days")
                    );
                    $this->count[] = Card::find($card->id)->update($data);
                }
            });

        $count = count($this->count);
        echo "Total {$count} cards have postponed. \n";
    
    }

}