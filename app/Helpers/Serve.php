<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Factory\Basic\Mixed;
use App\Helpers\Factory\Stock;
use App\Helpers\Helper;
use App\Option;
use App\Profile;
use App\Card;

// Helper Functions only for Revapp

class Serve
{
    
    public static function getCurrentProfileSettings($id = null){
        // Profile settings contains following data
        // return [
        //     'source_id' => [],
        //     'subject_id' => [2],
        //     'module_id' => [],
        //     'chapter_id' => [],
        //     'stage' => [],
        //     'status' => [],
        //     'ans_type' => [],
        //     'tag_id' => [],
        // ];

        // actual code

        if(is_null($id)){
            $profile = Profile::where('is_active', 1)->first();
        } else {
            $profile = Profile::find($id);
        }
        
        if(isset($profile->settings)){
            
            $profile->settings = unserialize($profile->settings);

            $profile->display_interval = is_null($profile->interval)? null: $profile->interval;

            $profile->interval = is_null($profile->interval)? 
                        explode(' ', Option::get('interval')): 
                        explode(' ', $profile->interval);

            // find last review date based on nc_gap
            if(is_null($profile->nc_gap)){
                $profile->last_review_date = null;
            } else {
                $profile->last_review_date = Helper::addTodayWith(-$profile->nc_gap);
            }

            return $profile;
        }

        die("There are no active profile");
    }

    

    /**
    *
    *
    * Remove array options from settings
    */
    public static function sortSettings($settings){

        $row = [];

        foreach ($settings as $key => $value) {
            if(is_array($value)){
                $row[$key] = $value[0]?? null;
            } else {
                $row[$key] = $value?? null;
            }
        }

        return $row;
        
    }

    public static function findDaysBetween($startDate, $endDate, $includeStartDate = false, $includeEndDate = false){

        $diff = Helper::dateDiff($endDate, $startDate);
        $dup = $startDate;
        $days = [];

        if($includeStartDate){
            $days[] = $startDate; // include first dates too
        }

        for($i = 1; $i < $diff; $i++){
            $dup = date('Y-m-d', strtotime($dup . "+1 Days"));
            $days[] = $dup;
        }

        if($includeEndDate){
            $days[] = $endDate; // include last dates too
        }

        return $days;
    
    }

}
