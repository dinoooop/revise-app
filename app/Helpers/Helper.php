<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Helpers\Factory\Basic\Mixed;

// General Helper Functions may be useful for other apps

class Helper
{

    public static function today(){
        return date('Y-m-d');
        //return '2022-05-07';
    }

    public static function addTodayWith($num){
        return date('Y-m-d', strtotime(self::today() . "{$num} days"));
    }

    public static function addDateWith($date, $num){
        return date('Y-m-d', strtotime($date . "{$num} days"));
    }

    public static function getReplicateLabel($table, $column, $exist)
    {
        $copy = "{$exist} Copy (%)";
        $count = DB::table($table)->where($column, 'like', $copy)->count();
        $count++;
        $newcopy = str_replace('%', $count, $copy);
        return $newcopy;
    }

    public static function printResponse($response)
    {
        $responseHTML = storage_path('app\public\response.html');

        if(is_array($response) || is_object($response)){
            ob_start();
            echo '<pre>' , print_r($response) , '</pre>';
            $response = ob_get_contents();
            ob_clean();
        }

        file_put_contents($responseHTML, $response);
    }

    public static function snakeCase($string){
        $string = snake_case($string);
        $string = self::removeSpecialCharecters($string);
        return $string;
    }

    public static function removeSpecialCharecters($string) {
        $string = str_replace(' ', '_', $string);
        return preg_replace('/[^A-Za-z0-9\_]/', '', $string);
    }

    public static function keysImplode($array){
        $keys = array_keys($array);
        return implode(',', $keys);
    }


    // {id: 1, name: "Soo"} changes to {label: "soo", value:1}

    public static function convertLabelValuePair($data, $label, $value, $isObj = true){
        $results = [];

        if($isObj){
            foreach ($data as $key => $val) {
                $row['label'] = $val->$label; 
                $row['value'] = $val->$value;
                $results[] = $row;
            }
        } else {
            foreach ($data as $key => $val) {
                $row['label'] = $val[$label];
                $row['value'] = $val[$value];
                $results[] = $row;
            }
        }
        
        return $results;
    }

    // {Temp: 60, Pressure: 94} changes to 
    // [{label: "Temp", value:60}, {label: "Pressure", value:94}]
    public static function convertLabelValueRaw($data){

        $results = [];

        foreach ($data as $key => $val) {
            $row['label'] = $key; 
            $row['value'] = $val;
            $results[] = $row;
        }

        return $results;
    }

    public static function setNameId($data){
        
        $results = [];

        foreach ($data as $key => $val) {
            $row['id'] = $val;
            $row['name'] = $key;
            $results[] = $row;
        }
        
        return $results;
    }

    public static function setNameIdToLabelValue($data){
        
        $results = [];

        foreach ($data as $key => $val) {
            $row['label'] = $val['name'];
            $row['value'] = $val['id'];
            $results[] = $row;
        }
        
        return $results;
    }

    public static function decodeUrlParams($request){

        $params = Mixed::urlParams();

        foreach ($params as $key => $value) {
            if(isset($request->$value)){
                $request->$key = $request->$value;
            }
        }

        return $request;
    }

    /**
    *
    * To check whether any item of $itemTwo in $itemOne 
    */
    public static function anyItemIn(array $itemOne, array $itemTwo){
        $itemOneDiff = array_diff($itemOne, $itemTwo);
        if (count($itemOneDiff) == count($itemOne)) {
            // No item of $itemTwo in $itemOne 
            return false;
        }
        return true;
    }

    public static function dateDiff($date2, $date1){

        if(is_null($date1) || is_null($date2)){
            return NULL;
        }
        
        $datetime2 = new \DateTime($date2);
        $datetime1 = new \DateTime($date1);
        $interval = $datetime2->diff($datetime1);
        return $interval->format('%a');
    
    }


    public static function readCSV($file){

        $handle = fopen($file, 'r');
        
        $results1 = [];

        while (($data = fgetcsv($handle)) !== FALSE) {
            foreach ($data as $key => $value) {
                $row[$key] = $value;
            }
            $results1[] = $row;
        }

        // Set header
        $results2 = [];
        foreach ($results1 as $key => $value) {

            if($key == 0){
                $header = $value;
                continue;
            }

            foreach ($value as $key => $val) {
                $row2[$header[$key]] = $val;
            }

            $results2[] = $row2;            
        }

        fclose($handle);
        return $results2;
    }

    public static function readExcel($file)
    {
        if ($xlsx = SimpleXLSX::parse($file)) {
            $results1 = $xlsx->rows();
        } else {
            echo SimpleXLSX::parseError();
        }
        // Set header
        $results2 = [];
        foreach ($results1 as $key => $value) {

            if($key == 0){
                $header = $value;
                continue;
            }

            foreach ($value as $key => $val) {
                $trimval = trim($val);
                $headerKey = trim($header[$key]);
                $row2[$headerKey] = ($trimval == '') ? null : $trimval;
            }

            $results2[] = $row2;            
        }

        return $results2;
    }

    public static function isImage($str){

        $imgExt = ['.jpg', '.png', '.jpeg'];

        foreach($imgExt as $key => $value){
            if(strpos($str, $value) !== false){
                return true;
            }
        }

        return false;

    }

    public static function dtfName($file = null, $randum = true){

        $ext = '';
        $orderTxt = '';

        if($randum){
            static $order = 1000;
            $orderTxt = '-' . ++$order;
        }
        
        if(!is_null($file)){
            $pathinfo = pathinfo($file);
            $ext = '.' . $pathinfo['extension'];
        }

        return self::today() . '-' . date('H-i-s') . $orderTxt . $ext;
    }

}
