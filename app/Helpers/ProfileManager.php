<?php

namespace App\Helpers;

use App\Helpers\Helper;
use App\Profile;
use App\Helpers\CardDirector;
use App\Helpers\Serve;

// Facedes

class ProfileManager extends CardDirector {
	
	function __construct(){
		parent::__construct();
		$this->where = [];
        $this->pageData = [];
        $this->options = [];
	}

	// Handle
	public function create($request){

		$this->request = $request;
		$this->setSettingsByRequest();
		$this->setCreatePageData();
		$this->setCreatePageDataAdvanced();

		$this->pageData['options'] = $this->options;
		$this->pageData['card_count'] = $this->cardCount();
		return $this->pageData;
	}


	// Handle
	public function edit($request, $id){

		$this->request = $request;
		
		if($this->request->action == 'changed'){

			// When user change the values

			$this->setSettingsByRequest();
			$this->setCreatePageDataAdvanced();

		} else {

			// First come to edit page

			$profile = Serve::getCurrentProfileSettings($id);
			$this->settings = $profile->settings;
			$this->setCreatePageData();
			$this->setCreatePageDataAdvanced();
			
			$this->pageData['selected'] = $profile->settings;
			
			$this->pageData['name'] = $profile->name;
			$this->pageData['nc_gap'] = $profile->nc_gap;
			$this->pageData['interval'] = $profile->display_interval;
			
		}

		$this->pageData['options'] = $this->options;
		$this->pageData['card_count'] = $this->cardCount();

		return $this->pageData;
	}
	
	// Handle
	public function store($request){
		$this->request = $request;

		$this->setSettingsByRequest();
		$this->setCreatePageDataAdvanced();
		$this->pageData['newItem'] = $this->storeDB();
		return $this->pageData;
	}

	// Handle
	public function update($request, $id){
		$this->request = $request;
		$this->id = $id;
		$profile = Profile::find($id);

		// Profile activate request

		if(isset($this->request->is_active) && $this->request->is_active){
			$this->deactivateAll();
			$profile->is_active = $this->request->is_active;
			return $profile->save();
		}


		// profile update request

		$this->setSettingsByRequest();
		$this->pageData['newItem'] = $this->updateDB();

		return $this->pageData;
	}

	public function storeDB(){		
		$profile = new Profile;
        $profile->name = $this->request->name;
        $profile->settings = serialize($this->settings);
        $profile->nc_gap = $this->request->nc_gap?? null;
		$profile->interval = $this->request->interval?? null;
        return $profile->save();
	}

	public function updateDB(){
		$profile = Profile::find($this->id);
        $profile->name = $this->request->name;
        $profile->settings = serialize($this->settings);
        $profile->nc_gap = $this->request->nc_gap?? null;
		$profile->interval = $this->request->interval?? null;
        return $profile->update();
	}

	public function deactivateAll(){
		Profile::where('is_active', 1)->update(['is_active' => 0]);
	}

}
