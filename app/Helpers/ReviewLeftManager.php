<?php 

namespace App\Helpers;

use App\Card;
use App\Subject;
use App\ReviewLeft;
use App\Helpers\Helper;
use App\Helpers\Factory\Stock;
use Facades\App\Helpers\CardManager;
use Illuminate\Http\Request;

class ReviewLeftManager {
	
	function __construct(){
		$this->today = Helper::today();
	}

	// handle
	public function get(){

		$data = [];

		$have = ReviewLeft::where('review_date', $this->today)->first();

		if($have){
			$this->update();
		} else {
			$this->insert();
		}

		$data['items'] = ReviewLeft::orderBy('review_date', 'desc')->limit(10)->get();

		return $data;
	
	}

	public function insert(){

		$data = [
			'review_date' => $this->today,
			'fy' => $this->findCount('FY'),
			'rt' => $this->findCount('RT'),
		];

		ReviewLeft::create($data);
	}


	public function update(){

		$data = [
			'fy' => $this->findCount('FY'),
			'rt' => $this->findCount('RT'),
		];

		ReviewLeft::where('review_date', $this->today)->update($data);
	}

	public function findCount($review_mode){

		$request = new Request([
			'review_mode' => $review_mode,
		]);

		return CardManager::findCount($request);
	
	}

}
