<?php

namespace App\Helpers\Factory\Generate;

use Illuminate\Support\Facades\DB;
use App\Department;
use App\User;
use App\Role;

class Process
{
    public function __construct()
    {
    }

    // user id for departments table
    public function getHeadId($key)
    {
        $department_id = $key + 1;
        $taken = Department::pluck('head_id')->toArray();
        $user = User::where('department_id', $department_id)
            ->whereNotIn('department_id', $taken)
            ->first();

        return $user ? $user->id : null;
    }

    public function userRole()
    {

        // set all department heads
        $role_id = Role::idOf("department_head");
        $departmentHeadUserIds = Department::pluck('head_id')->toArray();
        print_r($departmentHeadUserIds);
        foreach ($departmentHeadUserIds as $value) {
            DB::table('user_role')->insert([
                'user_id' => $value,
                'role_id' => $role_id,
            ]);
        }

        // set remaining users to employee
        $role_id = Role::idOf("employee");
        $noRoleAssignedUserIds = User::doesntHave('roles')->get();
        foreach ($noRoleAssignedUserIds as $value) {
            DB::table('user_role')->insert([
                'user_id' => $value->id,
                'role_id' => $role_id,
            ]);
        }

        // set one super admin
        $user_id = 1;
        $role_id = Role::idOf("super_admin");
        DB::table('user_role')->insert([
            'user_id' => $user_id,
            'role_id' => $role_id,
        ]);

        User::where('id', $user_id)->update(['email' => 'supadmin@mail.com']);

        // set one admin
        $user_id = 2;
        $role_id = Role::idOf("admin");
        DB::table('user_role')->insert([
            'user_id' => $user_id,
            'role_id' => $role_id,
        ]);
        
        User::where('id', $user_id)->update(['email' => 'admin@mail.com']);


    }
}
