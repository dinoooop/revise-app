<?php

namespace App\Helpers\Factory\Generate;

class Fake
{

    public static function objectName($wc = 1)
    {
        $string = 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Enim nulla aliquet porttitor lacus luctus accumsan tortor posuere Arcu risus quis varius quam Vehicula ipsum a arcu cursus vitae congue mauris Elementum pulvinar etiam non quam lacus Porta nibh venenatis cras sed felis Nunc id cursus metus aliquam Erat nam at lectus urna duis Scelerisque varius morbi enim nunc faucibus Vitae elementum curabitur vitae nunc sed velit Lacus vel facilisis volutpat est Nunc sed id semper risus in hendrerit gravida Suspendisse interdum consectetur libero id faucibus nisl tincidunt Dictum non consectetur a erat nam at lectus Et ligula ullamcorper malesuada proin libero';
        $explode = explode(' ', $string);
        $string = [];
        for ($i = 0; $i < $wc; $i++) {
            $rand = array_rand($explode);
            $string[] = $explode[$rand];

        }

        return ucwords(implode(" ", $string));
    }

    public static function personName()
    {
        
        return "Test Name";
    }
}
