<?php

namespace App\Helpers\Factory\Generate;

use App\Helpers\Factory\Stock;

class User
{

    private $index = 0;
    private $designationIndex = -1;
    private $departmentIndex = -1;

    public function set()
    {
        $this->designations = range(1, count(Stock::designations()));
        $this->departments = range(1, count(Stock::departments()));
    }

    public function create()
    {
        $this->index++;
        $this->designationIndex++;
        $this->departmentIndex++;

        $this->gender = $this->rand(['male', 'female']);
        $this->first_name = $this->name();
        $this->last_name = $this->name(true);
        $this->email = $this->email();
        $this->employee_id = $this->employee_id();
        $this->designation_id = $this->designation_id();

        $this->dob = $this->randDate('dob');
        $this->department_id = $this->department_id();
        $this->password = bcrypt('welcome');
        $this->join_date = $this->randDate('join_date');
        $this->status = 1;
    }
    public function get()
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'employee_id' => $this->employee_id,
            'designation_id' => $this->designation_id,
            'gender' => $this->gender,
            'dob' => $this->dob,
            'department_id' => $this->department_id,
            'password' => $this->password,
            'join_date' => $this->join_date,
            'status' => $this->status,

        ];

    }

    public function name($last = false)
    {

        if ($this->gender == 'male' || $last) {
            $names = [
                'John', 'Allen', 'Kev', 'Dev', 'Mike',
                'Michele', 'Williams', 'Colbert', 'Daniel',
                'Clark', 'Adams', 'Erik', 'Gonzalas',
            ];
        } else {
            $names = [
                'Peggy', 'Barbara', 'Daisy', 'Divina',
                'Ella', 'Hanna', 'Jennifer',
            ];
        }

        return $this->rand($names);
    }

    public function email()
    {
        return strtolower($this->first_name) . rand(100, 999) . '@mail.com';
    }

    public function employee_id()
    {
        $id = 1000 + $this->index;
        return 'HRM' . $id;
    }

    public function designation_id()
    {

        if (!isset($this->designations[$this->designationIndex])) {
            $this->designationIndex = 0;
        }
        return $this->designations[$this->designationIndex];
    }

    public function department_id()
    {

        if (!isset($this->departments[$this->departmentIndex])) {
            $this->departmentIndex = 0;
        }
        return $this->departments[$this->departmentIndex];
    }

    public function randDate($type)
    {

        $Y = $type == 'dob' ? range(1980, 2010) : range(2012, 2018);
        $M = range(1, 12);
        $D = range(1, 28);

        return $this->rand($Y)
        . '-' . $this->rand($M)
        . '-' . $this->rand($D);
    }

    public function rand($array)
    {
        $rand = array_rand($array);
        return $array[$rand];
    }
}
