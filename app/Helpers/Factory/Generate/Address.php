<?php

namespace App\Helpers\Factory\Generate;

use App\Address as AddressModel;
use App\User;

class Address
{

    function createForAllUsers() {
        AddressModel::truncate();
        $totalUsers = User::all()->count();
        for ($i = 1; $i <= $totalUsers; $i++) {
            $this->user_id = $i;
            $this->create();
        }
    }

    public function createOfType($type)
    {

        $this->data = factory(AddressModel::class)->make([
            'user_id' => $this->user_id,
            'type' => $type,
        ])->toArray();

        $this->setNameEmail($type);

        AddressModel::create($this->data);

    }

    public function create()
    {
        $type = rand(1, 3);

        switch ($type) {
            case 1:
                $this->createOfType('permanent');
                break;

            case 2:
                $this->createOfType('permanent');
                $this->createOfType('residential');
                break;

            case 3:
                $this->createOfType('permanent');
                $this->createOfType('residential');
                $this->createOfType('emergency');
                break;
        }
    }

    public function setNameEmail($type)
    {
        if ($type == "permanent" || $type == "residential") {
            $this->data['name'] = null;
            $this->data['email'] = null;
        }
    }

    
}
