<?php

namespace App\Helpers\Factory;

use App\Helpers\Factory\Basic\Caps;
use App\Helpers\Factory\Basic\Countries;
use App\Helpers\Factory\Basic\Mixed;
use App\Helpers\Factory\Dummy\Mixed as DummyMixed;
use App\Helpers\Helper;

class Stock
{

    public static function __callStatic($name, $key = null) {
        $content =  Mixed::$name();
        
        if(!isset($key[0])){
            return Helper::setNameId($content);
        }
        
        return $content[$key[0]]?? NULL;
    }

    public static function roles()
    {
        return Mixed::roles();
    }

    public static function caps()
    {
        return Caps::get();
    }

    public static function countries()
    {
        return Countries::get();
    }

    public static function subjects()
    {
        return Mixed::subjects();
    }

    public static function modules()
    {
        return Mixed::modules();
    }

    public static function chapters()
    {
        return DummyMixed::chapters();
    }

    public static function tags()
    {
        return Mixed::tags();
    }

    public static function options()
    {
        return Mixed::options();
    }

    public static function sources()
    {
        return Mixed::sources();
    }

    public static function profiles()
    {
        return DummyMixed::profiles();
    }


}
