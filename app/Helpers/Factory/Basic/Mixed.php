<?php

namespace App\Helpers\Factory\Basic;

use App\Helpers\Factory\Stock;
use App\Card;

class Mixed
{
    public static function options()
    {
        return [
            [
                'option_key' => 'interval',
                'option_value' => '1 7 14 30 60',
            ],
            [
                'option_key' => 'show_subdocs',
                'option_value' => 0,
            ],
            [
                'option_key' => 'image_last_card_id',
                'option_value' => 0,
            ],
            [
                // How many last n days required 
                // to find the estimate date of finish
                'option_key' => 'estimate_finish_day_count',
                'option_value' => 7,
            ],
            [
                // Create new card based on previous settings
                'option_key' => 'last_card_create_settings',
                'option_value' => [],
            ],
            
        ];
    }
    
    public static function roles()
    {
        return [
            'Super Admin',
            'Admin',
            'Department Head',
            'Employee',
        ];
    }

    public static function urlParams()
    {
    	return [
            'sortOrder' => 'so',
            'sortBy' => 'sb',
        ];
    }

    public static function subjects()
    {
        return [
            'Maths',
            'GK',
            'Eng',
            'Mal',
        ];
    }

    public static function modules()
    {
        return [
            1 => [
                'Equation',
                'Arithmetic',
                'Mental Ability',
            ],
            2 => [
                'FAI',
                'FAK',
                'Geo',
                'CA',
                'Bio',
                'Che',
                'Phy',
                'WF',
                'IR',
                'KR',
                'WH',
                'MI',
                'Eco',
                'Civics',
                'Con',
            ],
            3 => [
                'EngVoc',
                'Grammar',
            ],
            4 => [
                'MalVoc',
                'Grammar',
                'Literature',
            ]
        ];
    }

    public static function tags()
    {
        return [
            [
                'name' => 'typo',
                'description' => 'Card contain typo',
            ],
            [
                'name' => 'psd',
                'description' => 'Require psd change',
            ],
            [
                'name' => 'imp',
                'description' => 'Card is important',
            ],
            [
                'name' => 'confused',
                'description' => 'Confused question',
            ],
            [
                'name' => 'dupe',
                'description' => 'Felt duplication',
            ],
            
        ];
    }

    
    public static function sources()
    {
        return [
            'MAK',
            'TB',
            'MAKTB',
            'RF',
            'YT',
            'Mix',
            'Lakshya',
        ];
    }

    // Not In bb
    public static function status()
    {
        return [
            'active' => 1,
            'suspend' => 2,
            'processing' => 3, // the cards need custom code processing
        ];
    }

    // Not in db
    public static function ansType()
    {
        return [
            'vocal' => 1,
            'write' => 2,
        ];
    }

    

    // Not in db
    public static function stage(){
        return [
            'D1' => 1, // New (default)
            'KW' => 6, // Known Well

            'H60' => 2, // Hard
            'F60' => 3, // Failed on date
            'S60' => 4, // Success on date
            'E60' => 5, // Easy

            'H120' => 7, // Hard
            'F120' => 8, // Failed on date
            'S120' => 9, // Success on date
            'E120' => 10, // Easy
        ];
    }

    // Not in db
    public static function result(){
        return [
            'success' => 1,
            'failed' => 2,
        ];
    }

     // Not in db
    public static function general(){
        return [
            // minimum day required to consider effective recall
            'effective_recall_interval' => 7, 
            // 1 {7 14 30 60} consider {} to make S/F count
            'last_n_sfcount' => 4,
        ];
    }

    public static function level(){
        $data = [];
        $levels = Card::distinct()->pluck('level')->all();
        foreach ($levels as $key => $value) {
            $data['level-' . $value] = $value;
        }
        return $data;
    }
}
