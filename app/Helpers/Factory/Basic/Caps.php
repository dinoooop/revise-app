<?php

namespace App\Helpers\Factory\Basic;

class Caps
{
    public static function get()
    {
        return [
[
'cap' => 'View Company Revenue',
'roles' => [1],
],
[
'cap' => 'Manage Departments',
'roles' => [1,2],
],
[
'cap' => 'Manage Documents',
'roles' => [1,2,3],
],
[
'cap' => 'Manage users',
'roles' => [1,2,1,2,3],
],
[
'cap' => 'Manage Designations',
'roles' => [1,2],
],
[
'cap' => 'View Designations',
'roles' => [1,2,3],
],
[
'cap' => 'Manage Emails',
'roles' => [1,2,3],
],
[
'cap' => 'Manage Events',
'roles' => [1,2,3],
],
[
'cap' => 'View Events',
'roles' => [1,2,3],
],
[
'cap' => 'Manage Finance',
'roles' => [1,2,3],
],
[
'cap' => 'Pay Salary',
'roles' => [1,2,3],
],
[
'cap' => 'View Payroll',
'roles' => [1,2,3],
],
[
'cap' => 'Calendar View Event Assigned',
'roles' => [1,2,3,4,5],
],
[
'cap' => 'Manage Holidays',
'roles' => [1,2,3],
],
[
'cap' => 'Manage Leave',
'roles' => [1,2,3,4],
],
[
'cap' => 'Manage Complaint',
'roles' => [1,2,3,4,5],
],
[
'cap' => 'View Complaint',
'roles' => [1,2,3,4,5],
],
[
'cap' => 'Manage Announcement',
'roles' => [1,2,3],
],
[
'cap' => 'Manage Survey',
'roles' => [1,2,3],
],
[
'cap' => 'View User Personal Info',
'roles' => [1,2,3],
],
[
'cap' => 'Manage settings',
'roles' => [1],
],
[
'cap' => 'Manage roles and caps',
'roles' => [1],
],
[
'cap' => 'Show old password field',
'roles' => [4,5],
],
[
'cap' => 'Edit Kill',
'roles' => [1,2,3,4,5],
],
[
'cap' => 'Receive Complaints',
'roles' => [1,2,3,4],
],
];


    }
}
