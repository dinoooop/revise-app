<?php

namespace App\Helpers\Factory\Dummy;

class Mixed
{
    public static function chapters()
    {
        return [

            // Simple Arithmetic
            2 => [
                'AP',
                'Average',
                'Bank',
                'Boat Stream',
                'Digits',
                'Exponents',
                'Geometry',
                'Miscellaneous',
                'Mock Test',
                'Percentage',
                'Profit Lose',
                'PYQ',
                'Ratio',
                'Series',
                'Speed Distance',
                'Square Root',
                'Time Work',
                'Unit Digit',
            ],

            // Mental Ability
            3 => [
                'Age',
                'Blood Relation',
                'Calendar',
                'Clock',
                'Coding Decoding',
                'Direction',
                'Odd One',
                'Ranking Order',        
            ],
            19 => [
                'Collective',
                'Young One',
                'Killing',
                'studies',
                'Gender',
            ],
            20 => [
                'Concord',
                'Question tag',
                'Articles',
                'If clause',
                'Degree of comparison',
            ],
            22 => [
                'Naamam',
                'Linkam',
                'Vachanam',
                'Vibhakthi',
            ],
        ];
    }

    public static function profiles(){

        $settings = [
            'source_id' => [],
            'subject_id' => [2],
            'module_id' => [],
            'chapter_id' => [],
            'stage' => [],
            'status' => [],
            'ans_type' => [],
            'tag_id' => [],
        ];

        return [
            [
                'name' => 'GK all',
                'settings' => serialize($settings),
                'is_active' => 1,
            ]
        ];
    
    }
    
}
