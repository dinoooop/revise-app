<?php

namespace App\Helpers;

use App\Helpers\Helper;
use App\ReviewHistory;


class ReviewHistoryManager {

	function __construct(){

	}

	public function list(){

		$data = [];
		$data['histories'] = ReviewHistory::orderBy('id', 'desc')->limit(20)->get();
		return $data;

	}


}