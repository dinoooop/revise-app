<?php 

namespace App\Helpers;


class MatrixQue {

	function __construct(){
   
	}

	public function init($data){

		// $data = $this->dummyDataSetFunc();

		$records = [];
		foreach ($data as $key => $value) {
			$row = $this->getQueFromOneRow($value);
			$records = array_merge($records, $row);
		}

		return $records;
	
	}

	public function getQueFromOneRow($data){

        foreach ($data as $key => $value) {
        	$head[] = $key;
        }

        foreach ($data as $key => $value) {
        	$val[] = $value;
        }

        $results = [];
        $heading = isset($head[0]) ? $head[0] . ' - ': '';
        foreach ($head as $key => $value) {

        	if(count($head) - 1 == $key){
        		continue;
        	}

        	$row['que'] = $val[0] . ' (' . $heading . $head[$key + 1] . ')';
        	$row['ans'] = $val[$key + 1];
        	$results[] = $row;
        }

        return $results;
	
	}

	public function dummyDataSetFunc(){

		return [
			[
	            'collective' => 'Flowers',
	            'one' => 'Bunch',
	            'two' => 'Wreath',
	            'three' => 'Garlands',
        	],
        	[
	            'collective' => 'Monkey',
	            'one' => 'Troop',
	            'two' => 'Gang',
	            'three' => null,
        	],
		];
	
	}

}