<?php

namespace App\Helpers\Load;

use Illuminate\Support\Facades\DB;
use App\Helpers\Factory\Stock;
use App\Card;
use App\Chapter;
use App\Subdoc;
use App\CardTag;
use App\ReviewHistory;
use App\Option;
use Facades\App\Helpers\Load\CustomData;
use Facades\App\Helpers\ImportManager;
use Facades\App\Helpers\MathsClassifyManager;
use Facades\App\Helpers\OTUExistExcel;
use Facades\App\Helpers\OTUModifyMaths;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;

class FirstData
{

    function __construct(){
        $this->tables = [];
        $showTables = DB::select('SHOW TABLES');
        foreach($showTables as $table) {
            $this->tables[] = $table->Tables_in_laravel_revise_app;
        }
    }

    public function dropTables(){
        foreach($this->tables as $table) {
            Schema::dropIfExists($table);
        }
    }

    public function createTables(){
        shell_exec("php artisan migrate");    
    }

    public function truncateTables(){
        foreach($this->tables as $table) {
            DB::table($table)->truncate();
        }
    }
    
    public function setBasicData()
    {

    	foreach (Stock::subjects() as $value) {
	        DB::table('subjects')->insert([
	            'name' => $value,
	        ]);
	    }

        foreach (Stock::modules() as $key => $value) {
            foreach ($value as $val) {
                DB::table('modules')->insert([
                    'subject_id' => $key,
                    'name' => $val,
                ]);
            }
        }

        foreach (Stock::chapters() as $key => $value) {
            foreach ($value as $val) {
                DB::table('chapters')->insert([
                    'module_id' => $key,
                    'name' => $val,
                ]);
            }
        }

        foreach (Stock::tags() as $key => $value) {
            DB::table('tags')->insert($value);
        }

        foreach (Stock::options() as $key => $value) {
            if(is_array($value['option_value'])){
                $value['option_value'] = serialize($value['option_value']);
            }
            DB::table('options')->insert($value);
        }

        foreach (Stock::sources() as $value) {
            DB::table('sources')->insert([
                'name' => $value,
            ]);
        }

        foreach (Stock::profiles() as $value) {
            DB::table('profiles')->insert($value);
        }
    }

    public function resetOptions(){
        DB::table('options')->truncate();
        foreach (Stock::options() as $key => $value) {
            if(is_array($value['option_value'])){
                $value['option_value'] = serialize($value['option_value']);
            }
            DB::table('options')->insert($value);
        }
    }

    public function setDummyCards(){
        $request = new Request([
            'console_file' => 'public/card-import/quick.xlsx',
        ]);
        ImportManager::insertData($request);
    
    }

    public function backup($bkpfilename = null){
        
        if(is_null($bkpfilename)){
            $bkpfilename = '';
        } else {
            $bkpfilename = '-' . $bkpfilename;
        }

        $mysqldump = 'E:/xampp/mysql/bin/mysqldump';
        $filename = date('Y-m-d-H-i-s') . $bkpfilename . '.sql';
        $filePath = storage_path('app/backup/' . $filename);
        $command = $mysqldump .' --user=root --host=127.0.0.1 laravel_revise_app > ' . $filePath;
        $returnVar = NULL;
        $output = NULL;
        //echo $command;
        exec($command, $output, $returnVar);
        echo $filename;
    }

    public function restoreLastBackup($latestSqlFile = null){
        
        if(is_null($latestSqlFile)){

            $filePath = storage_path('app/backup/*');
            $sql = glob($filePath);

            $total = count($sql);
            if($total == 0){
                echo "No backups available";
                return;
            }

            $lastKey = $total - 1;
            $latestSqlFile = $sql[$lastKey];
            
        } else {
            $latestSqlFile = storage_path('app/backup/' . $latestSqlFile);
        }
        


        $this->dropTables();
        
        $command = 'E:/xampp/mysql/bin/mysql -u root laravel_revise_app < ' . $latestSqlFile;
        
        $returnVar = NULL;
        $output = NULL;
        exec($command, $output, $returnVar);
    }

    // Keep the last five sql backup files
    public function lastFive(){
        $filePath = storage_path('app/backup/*');
        $sqlFiles = glob($filePath);
        rsort($sqlFiles);

        foreach ($sqlFiles as $key => $value) {
            if($key < 5){
                continue;
            }
            
            unlink($value);
        }

    }

    


    public function custom(){
        
        CustomData::updateLastReviewDateSimple();
        
    }
}


