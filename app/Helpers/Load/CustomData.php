<?php

namespace App\Helpers\Load;

use Illuminate\Support\Facades\DB;
use App\Helpers\Factory\Stock;
use App\Card;
use App\Tag;
use App\Chapter;
use App\Subdoc;
use App\CardTag;
use App\MathsClassify;
use App\ReviewHistory;
use Facades\App\Helpers\ImportManager;
use Facades\App\Helpers\MathsClassifyManager;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;

class CustomData {
	
	function __construct(){
		
	}

	public function insertChapter(){

        $data = [
            
        ];

        foreach ($data as $key => $value) {
            
            Chapter::create([
                'module_id' => 3,
                'name' => $value,
            ]);
            
        }
    
    }

    public function sortName(){
        $chapters = Chapter::whereIn('module_id', [2,3])->get();

        foreach ($chapters as $key => $chapter) {
            $chapter->name = str_replace(' ', '-', strtolower($chapter->name));
            $chapter->save();
        }
    }

    public function deleteCard($ids){
        foreach ($ids as $key => $id) {
            Card::where('id', $id)->delete();
            Subdoc::where('card_id', $id)->delete();
            CardTag::where('card_id', $id)->delete();
            ReviewHistory::where('card_id', $id)->delete();
        }
    }

    public function markAlreadyClassifiedMaths(){
    	
    	$data = [
    		'stage' => 3,
    		'chapter_id' => 28,
    	];

    	MathsClassify::where($data)->update(['is_classified' => 1]);
    
    }

    public function loadMathsImgFromDir(){
    	DB::table('maths_classify')->truncate();
        MathsClassifyManager::init();
    }

    public function deleteExistMaths(){
    	

    	$cards = Card::where('subject_id', 1)->get();

    	foreach ($cards as $key => $value) {
    		$id = $value->id;

    		
    		$que = Storage::path('public/classify-img-save/' . $value->que);
    		$ans = Storage::path('public/classify-img-save/' . $value->ans);

    		
			unlink($que);
			unlink($ans);
    		

            Subdoc::where('card_id', $id)->delete();
            CardTag::where('card_id', $id)->delete();
            ReviewHistory::where('card_id', $id)->delete();
    		Card::where('id', $id)->delete();
    	}

    
    }

    public function newTags(){
        $tags = [
            [
                'name' => 'must',
                'description' => 'must check before exam',
            ],
            [
                'name' => 'hard',
                'description' => 'For maths - hard to remember',
            ],
            [
                'name' => 'medium',
                'description' => 'For maths - medium type questions',
            ],
            [
                'name' => 'easy',
                'description' => 'For Maths - easy questions',
            ],
            
        ];

        foreach ($tags as $key => $value) {
            Tag::create($value);
        }
    
    }

    public function updateForKW(){
        $cards = Card::where('stage', Stock::stage('KW'))
            ->where('level', 2)
            ->get();
        foreach ($cards as $key => $value) {
            $value->level = 1;
            $value->save();
        }
    }


    /**
    * Getting output 
    * Executed : 34864/7990 something went wrong
    */
    public function updateLastReviewDate(){

        //Total count

        // total cards to execute
        $this->tce = ReviewHistory::distinct('card_id')->count();
        $this->executed = 0;
        $this->current_card_id = 0;

        ReviewHistory::select('card_id', 'review_date')
            ->groupBy('card_id')
            ->groupBy('review_date')
            ->orderBy('review_date', 'desc')
            ->chunk(50, function ($histories) {
                foreach ($histories as $key => $history) {

                    if($history->card_id != $this->current_card_id){
                        Card::where('id', $history->card_id)
                            ->update(['last_review_date' => $history->review_date]);
                        $this->current_card_id = $history->card_id;
                        $this->executed++;
                    }
                }
            });

        echo 'Executed : ' . $this->executed . '/' . $this->tce;

       


    }


    public function updateLastReviewDateSimple(){

        //Total count

        // total cards to execute
        $this->tce = ReviewHistory::distinct('card_id')->count();
        $this->executed = 0;
        
        ReviewHistory::select('card_id')
            ->groupBy('card_id')
            ->chunk(50, function ($histories) {
                foreach ($histories as $key => $history) {

                    $latest = ReviewHistory::where('card_id', $history->card_id)
                        ->orderBy('review_date', 'desc')
                        ->first();

                    Card::where('id', $latest->card_id)
                        ->update(['last_review_date' => $latest->review_date]);

                    $this->executed++;

                }

            });

        echo 'Executed : ' . $this->executed . '/' . $this->tce;

       


    }
}
