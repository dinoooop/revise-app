<?php

namespace App\Helpers;

use App\Tag;
use App\Card;
use App\Helpers\CardDirector;
use App\Helpers\Factory\Stock;

// A one time use (OTU) class 
// for update maths question for tags
// instead of stage
//
// Use : Completed, can delete this file

class OTUModifyMaths extends CardDirector {
	
	function __construct(){
		parent::__construct();
	}

	public function init(){
		// $this->newTags();
		// $this->updateHard();
		// $this->updateMust();
	}

	public function newTags(){
        $tags = [
            [
                'name' => 'must',
                'description' => 'must check before exam',
            ],
            [
                'name' => 'hard',
                'description' => 'For maths - hard to remember',
            ],
            [
                'name' => 'medium',
                'description' => 'For maths - medium type questions',
            ],
            [
                'name' => 'easy',
                'description' => 'For Maths - easy questions',
            ],
            
        ];

        foreach ($tags as $key => $value) {
            Tag::create($value);
        }
    
    }

    public function updateMust(){
    	$mustId = Tag::where('name', 'must')->first()->id;
    	$cards = $this->getProfileCards();
    	$count = [];
    	foreach ($cards as $key => $card) {
    		$count[] = $card->update(['stage' => Stock::stage('D1')]);
    		$card->tags()->sync([$mustId]);
    	}

    	echo count($count) . ' cards updated';
    }

    public function updateHard(){

    	$hardId = Tag::where('name', 'hard')->first()->id;
    	$cards = $this->getProfileCards();
    	$count = [];
    	foreach ($cards as $key => $card) {
    		$card->update(['stage' => Stock::stage('D1')]);
    		$count[] = $card->tags()->sync([$hardId]);
    	}

    	echo count($count) . ' cards updated';

    }

    public function getProfileCards(){
    	$this->query = Card::query();
		$this->where = $this->profileSettings;
		$this->applyBasicWhere();
		return $this->query->get();
    }

}
