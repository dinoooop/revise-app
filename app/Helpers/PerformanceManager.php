<?php 

namespace App\Helpers;

use App\Card;
use App\Performance;
use App\Helpers\Factory\Stock;
use App\Helpers\Helper;
use App\Helpers\Serve;
use App\Option;
use Illuminate\Support\Facades\Log;
use App\ReviewHistory;
use App\ReviewLeft;
use Illuminate\Support\Facades\Storage;

class PerformanceManager {
	
	function __construct(){
		$this->today = Helper::today();
		$this->updation = false;
		$this->processDates = [];
		$this->closeProcessDates = [];
	}

	// Handle
	// No request
	public function execute(){

		$this->setProcessDates();

		foreach ($this->processDates as $key => $date) {
			$this->setData($date);

			if($this->updation){
				$this->update();
			} else {
				$this->create();
			}
		}

		$this->setCloseProcessDates();

		foreach ($this->closeProcessDates as $key => $date) {
			$this->setData($date);
			$this->update();
		}
		
	}

	public function setProcessDates(){

		$performance = Performance::all()->sortByDesc('review_date')->first();

		if(!isset($performance->id)){
			// No performance records
			
			$reviewHistory = ReviewHistory::all()->sortBy('review_date')->first();

			if(!isset($reviewHistory->id)){
				// Not reviewed any cards
				$this->processDates = [];
			} else {

				if($reviewHistory->review_date == $this->today){
					$this->processDates = [$this->today];
				} else {
					$this->processDates = Serve::findDaysBetween($reviewHistory->review_date, $this->today, true, true);
				}
			}

		} else {

			if($performance->review_date == $this->today){
				$this->processDates = [$this->today];
				$this->updation = true;
			} else {
				$this->processDates = Serve::findDaysBetween($performance->review_date, $this->today, false, true);
				
			}

		}

		Log::info("processDates = ");
		Log::info($this->processDates);
	}

	// Find performance with status NULL, re-evaluate and update
	public function setCloseProcessDates(){
		$performance = Performance::where('status', null)
		->where('review_date', '!=', $this->today)->get();

		foreach ($performance as $key => $value) {
			$this->closeProcessDates[] = $value->review_date;
		}
	
	}

	public function setData($date){

		$this->review_date = $date;

		$this->failed = ReviewHistory::where([
			'review_date' => $this->review_date,
			'result' => Stock::result('failed'),
		])->get()->count();

		$this->success = ReviewHistory::where([
			'review_date' => $this->review_date,
			'result' => Stock::result('success'),
		])->get()->count();

		$this->nc = ReviewHistory::where([
			'review_date' => $this->review_date,
			'result' => null,
		])->get()->count();

		$this->total_card = $this->failed + $this->success + $this->nc;
		$this->mark = $this->getMark();

		if($this->review_date == $this->today){
			$this->status = null;
		} else {
			$this->status = 1; // Closed for no updation
		}
	
	}

	public function getMark(){

		$totalQue = $this->failed + $this->success;

		// apply negative marks
		$success = $this->success - ($this->failed * 1/3);
		
		// Maintained min no of que
		if($totalQue < 100){
			$totalQue = 100;
		}

		return round( ($success / $totalQue) * 100, 2 );
	
	}


	public function create(){

		return Performance::create([
			'review_date' => $this->review_date,
			'success' => $this->success,
			'failed' => $this->failed,
			'nc' => $this->nc,
			'total_card' => $this->total_card,
			'mark' => $this->mark,
			'status' => $this->status,
		])->id;
	
	}

	public function update(){

		return Performance::where('review_date', $this->review_date)->update([
			'success' => $this->success,
			'failed' => $this->failed,
			'nc' => $this->nc,
			'total_card' => $this->total_card,
			'mark' => $this->mark,
			'status' => $this->status,
		]);
	
	}

}
