<?php

namespace App\Helpers;

use App\Subject;
use App\Module;
use App\Chapter;

// Facedes

class SmcManager {
	
	function __construct(){
	}

	// Handle require
	public function create($request){

		$data = [];

		$this->request = $request;
		$this->setCreatePageData();

		$data['options'] = $this->options;
		return $data;
	}

	// Handle
	// smctype * : subject, module, chapter
	// name *
	// action * : store | update
	// subject_id
	// module_id 
	// chapter_id
	public function store($request){

		$data = [];

		$this->request = $request;
		$data['new'] = $this->storeDB();
        
		return $data;
	}


    public function update($request, $id){

        $data = [];

        $this->request = $request;
        $data['new'] = $this->updateDB($id);
        
        return $data;
    }

	
	public function setCreatePageData(){

		$this->options = [];

        $this->options['subject_id'] = Subject::all();

        if($this->request->subject_id != ''){
            $this->options['chapter_id'] = [];
            $this->options['module_id'] = Module::where('subject_id', $this->request->subject_id)->get();
        }

        if($this->request->module_id != ''){
            $this->options['chapter_id'] = Chapter::where('module_id', $this->request->module_id)->get();
        }
	}

	public function storeDB(){

		$data = [];
        
        switch ($this->request->smctype) {
            case 'subject':
                $obj = new Subject;
                $data = $obj->create($this->request->all());
                break;

            case 'module':
                $obj = new Module;
                $data = $obj->create($this->request->all());
                break;

            case 'chapter':
                $obj = new Chapter;
                $data = $obj->create($this->request->all());
                break;
            
            default:
                $data = false;
                break;
        }

        return $data; 
	}

	public function updateDB($id){
		$obj = [];
        
        switch ($this->request->smctype) {
            case 'subject':
                $obj = Subject::find($id);
                break;

            case 'module':
                $obj = Module::find($id);
                break;

            case 'chapter':
                $obj = Chapter::find($id);
                break;
        }

        $obj->name = $this->request->name_edit;
        $obj->save();
        return $obj;


	}

}
