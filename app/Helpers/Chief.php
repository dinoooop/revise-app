<?php

namespace App\Helpers;

use App\Helpers\Helper;
use App\Helpers\Serve;

// Facedes

class Chief {
	function __construct(){
		$this->today = Helper::today();
		$this->profile = Serve::getCurrentProfileSettings();
		$this->profileSettings = $this->profile->settings;
	}
}