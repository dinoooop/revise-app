<?php

namespace App\Helpers;

use App\Helpers\Helper;
use App\Card;
use App\ReviewHistory;
use App\Helpers\Factory\Stock;
use Facades\App\Helpers\EstimateDateFinish;
use App\Helpers\CardDirector;

// Facedes

class StatisticsManager extends CardDirector {
	
	function __construct(){
		parent::__construct();
	}

	public function get(){

		$data = $row = [];

		$this->maxLevel = $this->getMaxLevel();

		$row[] = $this->getDataForLevel(0);

		for ($i=1; $i <= $this->maxLevel ; $i++) { 
			$row[] = $this->getDataForLevel($i);
		}

		$data['levels'] = $row;

		return $data;
		
	}

	public function getDataForLevel($level){

		$data = [];

		$this->level = $level;

		$records = $this->getData('status');
		$data['status'] = Helper::convertLabelValueRaw($records);

		$records = $this->getData('today');
		$data['today'] = Helper::convertLabelValueRaw($records);

		$records = $this->getData('stage');
		$data['stage'] = Helper::convertLabelValueRaw($records);

		$records = $this->getData('other');
		$data['other'] = Helper::convertLabelValueRaw($records);

		return $data;

	}

	public function getData($type){

		$data = [];

		switch ($type) {
			case 'status':
				$data['NC'] = $this->findCount('NC');
				$data['Learning'] = $this->findCount('learning');
				$data['Suspended'] = $this->findCount('suspend');
				$data['Active'] = $data['Learning'] + $data['NC'];
				$data['Total'] = $data['Suspended'] + $data['Active'];
				break;

			case 'stage':				
				$data['D1'] = $this->findCount('D1');
				$data['KW'] = $this->findCount('KW');
				$data['H60'] = $this->findCount('H60');
				$data['F60'] = $this->findCount('F60');				
				$data['S60'] = $this->findCount('S60');
				$data['E60'] = $this->findCount('E60');
				$data['H120'] = $this->findCount('H120');
				$data['F120'] = $this->findCount('F120');
				$data['S120'] = $this->findCount('S120');
				$data['E120'] = $this->findCount('E120');
				break;

			case 'today':
				$data['NC'] = $this->findCount('NC-Today');
				$data['KW'] = $this->findCount('KW-Today');
				$data['FT'] = $this->findCount('F-Today');
				$data['RT'] = $this->findCount('RT-Today');
				break;

			case 'other':
				// Estimate date of finish for profile based on level
				$data = EstimateDateFinish::get($this->level);
				break;

			
		}

		return $data;

	}

	public function getMaxLevel(){
		$this->where = [];
        $this->query = Card::query();
		$this->where = $this->profileSettings;
		unset($this->where['level']);
		unset($this->where['stage']);
        $this->applyBasicWhere();
		return $this->query->max('level');
	}

	// For find count
	public function findCount($type = false, $param = false){

		$this->where = [];
        
        $this->query = Card::query();

        switch ($type) {

            case 'NC':
                $this->where['is_fy'] = null;
                $this->query->where('next_review_date', null);
                break;

            case 'NC-Today':
                $this->query->whereHas('reviewHistory', function($q) {
                        $q->where('review_date', $this->today);
                        $q->where('result', null);
                    });
                break;

            // Failed today
            case 'F-Today':
                $this->query->whereHas('reviewHistory', function($q) {
                        $q->where('review_date', $this->today);
                        $q->where('result', Stock::result('failed'));
                    });
                break;

            case 'KW-Today':
            	$this->where['stage'] = Stock::stage('KW');
                $this->query->whereHas('reviewHistory', function($q) {
                        $q->where('review_date', $this->today);
                        $q->where('result', Stock::result('success'));
                    });
                break;

			case 'RT-Today':

                $this->query->whereHas('reviewHistory', function($q) {
                        $q->where('review_date', $this->today);
                    });
                break;

            case 'learning':
                $this->query->whereNotNull('next_review_date');
                break;

            case 'suspend':
                $this->where['status'] = Stock::status('suspend');
                break;

            case 'D1':
            case 'H60':
            case 'F60':
            case 'S60':
            case 'E60':
            case 'KW':
            case 'H120':
            case 'F120':
            case 'S120':
            case 'E120':
            	$this->where['stage'] = Stock::stage($type);
                break;
        }


        // Over write profile settings
        $this->where = array_merge($this->profileSettings, $this->where);

        if($this->level == 0){
        	unset($this->where['level']);
        } else {
        	$this->where['level'] = $this->level;
        }

        // KW cards needs to include in RT today
        if($type == 'RT-Today'){
        	unset($this->where['stage']);
        }

        $this->applyBasicWhere();
		return $this->query->get()->count();
    
    }

	
}
