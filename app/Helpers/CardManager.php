<?php 

namespace App\Helpers;

use App\Card;
use App\Option;
use App\ReviewLeft;
use App\ReviewHistory;
use App\Helpers\Helper;
use App\Helpers\Factory\Stock;
use App\Helpers\CardDirector;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class CardManager extends CardDirector  {
	
	function __construct(){
        parent::__construct();
		$this->where = [];
        $this->pageData = [];
        $this->options = [];
	}

    public function setRequest($request){
        $this->request = $request;
        if(
            isset($this->request->ans_type) 
            && !is_numeric($this->request->ans_type)
        ){
            $this->request->ans_type = Stock::ansType($this->request->ans_type);
        }
    }

	public function index($request){

		$this->setRequest($request);

		$data = [];
        $this->query = Card::query();
        $this->where = array_merge($this->profileSettings, $this->where);

        $this->applyBasicWhere();
        $this->applyAdvanceWhere();

        $this->query->withCount('reviewHistory');
        $this->query->with('source');


        $data = $this->query->orderBy('id', 'desc')->paginate();

        return $data;
        
	}

    public function create($request){

        $this->setRequest($request);

        if($this->request->action == 'changed'){

            // When user change the values

            $this->setSettingsByRequest();
            $this->setCreatePageDataAdvanced();

            // string type value not works with checkbox
            if(is_array($this->settings['tag_id'])){

                $row = [];
                foreach ($this->settings['tag_id'] as $key => $value) {
                    $row[] = (int) $value;
                }
                $this->settings['tag_id'] = array_unique($row);
            } 
            
            Option::set('last_card_create_settings', $this->settings);

        } else {
            
            $this->settings = Option::get('last_card_create_settings');
            unset($this->settings['que']);
            unset($this->settings['ans']);
            

            $this->setCreatePageData();
            $this->setCreatePageDataAdvanced();
            
            
            //$this->pageData['selected']['tag_id'] = $card->tags->pluck('id')->all();
        }

        $this->pageData['selected'] = $this->settings;
        


        $this->pageData['options'] = $this->options;
        
        return $this->pageData;

    }

    public function edit($request, $id){

        $this->setRequest($request);
        
        $card = Card::find($id);

        if(Helper::isImage($card->que)){
            $card->que = Storage::url('classify-img-save/' . $card->que);
        }

        if(Helper::isImage($card->ans)){
            $card->ans = Storage::url('classify-img-save/' . $card->ans);
        }
        
        if($this->request->action == 'changed'){

            // When user change the values

            $this->setSettingsByRequest();
            $this->setCreatePageDataAdvanced();

        } else {

            // First come to edit page
            $this->settings = $card;
            
            $this->setCreatePageData();
            $this->setCreatePageDataAdvanced();
            
            $this->pageData['selected'] = $card;
            $this->pageData['selected']['tag_id'] = $card->tags->pluck('id')->all();

        }

        if(isset($card->reviewHistory)){
            $rec = [];
            $history = $card->reviewHistory->sortByDesc('review_date')->all();
            foreach ($history as $key => $value) { $rec[] = $value; }
            $this->pageData['history'] = $rec;
        }


        $this->pageData['options'] = $this->options;
        
        return $this->pageData;
    }

    public function update($request, $id){

        $this->setRequest($request);
        $card = Card::find($id);
        $data = $request->all();
        if(Helper::isImage($card->que)){
            unset($data['que']);
        }

        if(Helper::isImage($card->ans)){
            unset($data['ans']);
        }
        
        $card->tags()->sync($request->tag_id);
        return $card->update($data);
    
    }


    // In case dupe change the source
	public function sourceSwitch($request, $id){

        $card = Card::find($id);
        $card->source_id = $request->source_id;
        return $card->save();

    }

    // Get a card for review
    public function review($request){

		$data = [];

        $this->setRequest($request);
		
        $this->query = Card::query();
        $this->setWhere();
        $this->applyBasicWhere();

        if(Option::get('show_subdocs')){
            $this->query->with('subdocs');
        }
        
        $this->query->with('tags');
        $this->query->with('module');
        $this->query->with('reviewHistory');

        $card = $this->query->inRandomOrder()->first();

        if(!isset($card->que)){
            $data['error'] = true;
            $data['errorMsg'] = "No cards!";
            return $data;
        }

        $card->ans = nl2br($card->ans);
        
        if(Helper::isImage($card->que)){
            $card->que = Storage::url('classify-img-save/' . $card->que);
        }

        if(Helper::isImage($card->ans)){
            $card->ans = Storage::url('classify-img-save/' . $card->ans);
        }

        $data['card'] = $card;

        if(isset($card->reviewHistory)){
            $rec = [];
            $history = $card->reviewHistory->sortByDesc('review_date')->all();
            foreach ($history as $key => $value) { $rec[] = $value; }

            $data['history'] = $rec;
            $data['review_count'] = $card->reviewHistory->count();

            $last = $card->reviewHistory->last();

            $data['review_after'] = isset($last->review_date)? Helper::dateDiff($this->today, $last->review_date): 0;
        } else {
            $data['history'] = [];
            $data['review_count'] = 0;
            $data['review_after'] = 0;
        }

        if(Option::get('show_subdocs') && isset($card->subdocs)){         
            foreach ($card->subdocs as $key => $value) {
                $data['subdocs_list'][] = Storage::url('classify-img-save/' . $value->filename);
            }
        
            $data['subdocs_count'] = $card->subdocs->count();
        } else {
            $data['subdocs_count'] = 0;
        }

        $data['card_count'] = $this->getCardCount();
        $data['subject_id'] = $this->request->subject_id;
        $data['review_mode'] = $this->request->review_mode;
        $data['ans_type'] = $this->request->ans_type;
        $data['card_id'] = $card->id;
        $data['selected'] = $card;
        $data['selected'] = $card;
        $data['selected']['tag_id'] = $card->tags->pluck('id')->all();

        return $data;
		
	}

    public function findCount($request){
        $this->setRequest($request);
        $this->query = Card::query();
        $this->setWhere();
        $this->applyBasicWhere();
        return $this->query->get()->count();
        
    }

    public function setWhere(){

        $this->where = [];

        // status, level, stage will available from
        // profile settings

        switch ($this->request->review_mode) {

            case 'NC':
                $this->where['is_fy'] = null;
                if(is_null($this->profile->last_review_date)){
                    // Not reviewed yet
                    $this->query->where('last_review_date', $this->profile->last_review_date);
                } else {
                    // How much old card
                    $this->query->where('last_review_date', '<=', $this->profile->last_review_date);
                }
                
                break;

            case 'FY':
                $this->where['is_fy'] = 1;
                $this->query->where('next_review_date', '<=', $this->today);
                break;

            case 'RT':                
                $this->where['is_fy'] = null;
                $this->query->where('next_review_date', '<=', $this->today);
                break;
        }

        $this->where = array_merge($this->profileSettings, $this->where);

    }

	

	public function applyAdvanceWhere(){
		if (isset($this->request->search)) {
            $this->query->where(function($query){
            	
                $query
                // NO search in file name
                ->where('que', 'not like', "%.jpg")
                ->where('que', 'not like', "%.png")
                ->where(function($que){
                    $que
                    ->where('que', 'like', "%{$this->request->search}%")
                	->orWhere('ans', 'like', "%{$this->request->search}%");
                });
            });
        }

        if(isset($this->request->so)){
            $order = $this->request->so ? 'asc' : 'desc';
            $this->query->orderBy($this->request->sb, $order);
        }
	}

    public function getCardCount(){

        if($this->request->review_mode == 'NC'){

            return ReviewHistory::where('review_date', $this->today)
            ->where('result', null)
            ->get()->count();

        } else if($this->request->review_mode == 'FY'){
            return ReviewLeft::where([
                'review_date' => $this->today,
            ])->value('fy');
        } else if($this->request->review_mode == 'RT'){
            return ReviewLeft::where([
                'review_date' => $this->today,
            ])->value('rt');
        }

    }
    
}
