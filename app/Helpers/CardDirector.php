<?php

namespace App\Helpers;

use App\Helpers\Helper;
use App\Subject;
use App\Module;
use App\Chapter;
use App\Tag;
use App\Source;
use App\Card;
use App\Helpers\Factory\Stock;
use Illuminate\Support\Facades\Log;
use App\Helpers\Chief;
use Illuminate\Database\Eloquent\Builder;

class CardDirector  extends Chief {

	function __construct(){
		parent::__construct();
	}

	public function setSettingsByRequest(){
		$this->settings = [
			'subject_id' => $this->request->subject_id ?? null,
			'module_id' => $this->request->module_id ?? null,
			'chapter_id' => $this->request->chapter_id ?? null,
			'ans_type' => $this->request->ans_type ?? null,
			'source_id' => $this->request->source_id ?? null,
			'tag_id' => $this->request->tag_id ?? [],
			'stage' => $this->request->stage ?? null,
			'status' => $this->request->status ?? null,
			'level' => $this->request->level ?? null,
		];



	}

	public function setCreatePageData(){
		$this->options['subject_id'] = Subject::all();
		$this->options['ans_type'] = Stock::ansType();
		$this->options['source_id'] = Source::all();
		$this->options['tag_id'] = Tag::all();
		$this->options['stage'] = Stock::stage();
		$this->options['status'] = Stock::status();
		$this->options['level'] = Stock::level();
		
		$this->pageData['card_count'] = 0;
	}

	public function setCreatePageDataAdvanced(){

		if(isset($this->settings['subject_id'])){

			if(is_array($this->settings['subject_id'])){
				$this->options['module_id'] = Module::whereIn('subject_id', $this->settings['subject_id'])->get();
			} else {
				$this->options['module_id'] = Module::where('subject_id', $this->settings['subject_id'])->get();
			}

		}

		if(isset($this->settings['module_id'])){

			if(is_array($this->settings['module_id'])){
				$this->options['chapter_id'] = Chapter::whereIn('module_id', $this->settings['module_id'])->get();
			} else {
				$this->options['chapter_id'] = Chapter::where('module_id', $this->settings['module_id'])->get();
			}
		}

	}

	public function setCardCount(){
		$this->pageData['card_count'] = $this->cardCount();
	}

	public function cardCount(){
		$this->query = Card::query();
		$this->where = $this->settings;
		if(!is_null($this->request->nc_gap)){
			$this->where['last_review_date'] = Helper::addTodayWith(-$this->request->nc_gap);
			
		}
		$this->applyBasicWhere();
		return $this->query->count();
	}

	public function applyBasicWhere(){

		foreach ($this->where as $key => $value) {

            if(is_array($value) && empty($value)){
                continue;
            }

            // allow null not '' (stage can be null)

            if($value === ''){
                continue;
            }

            $nullSearchAllowColumn = ['is_fy', 'next_review_date', 'last_review_date', 'result'];

            if(is_null($value)){
            	if(!in_array($key, $nullSearchAllowColumn)){
            		continue;
            	}
            }
            
            if($key == 'last_review_date'){
            	$this->query->where('last_review_date', '<=', $value);
            	continue;
            }

            if($key == 'tag_id'){
                if(!is_null($value)){
                    $this->query->whereHas('tags', function($q) use($value) {
                        $q->whereIn('tag_id', $value);
                    });

                }
                continue;
            }



            if(is_array($value)){
                $this->query->whereIn($key, $value);            	
            } else {
                $this->query->where($key, $value);
            }
        }
        
	}

}