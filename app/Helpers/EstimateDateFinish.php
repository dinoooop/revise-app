<?php

namespace App\Helpers;

use App\Card;
use App\Option;
use App\ReviewHistory;
use App\Helpers\Helper;
use App\Helpers\Serve;
use App\Helpers\Factory\Stock;
use Illuminate\Support\Facades\DB;
use App\Helpers\CardDirector;

// Facedes

class EstimateDateFinish extends CardDirector {
	
	function __construct(){
		parent::__construct();
		$this->estDayCount = Option::get('estimate_finish_day_count');
	}

	function get($level){

		$data = [];

		// Find number of NC for last 7 days

		$this->where = [];
		$this->query = ReviewHistory::query();
		$this->query->leftJoin('cards', 'review_history.card_id', '=','cards.id');
		$this->where['result'] = null;
		$this->where['review_date'] = $this->getLastNDays($this->estDayCount);
		$this->where = array_merge($this->profileSettings, $this->where);
		$this->where['level'] = $level;
        $this->applyBasicWhere();
		$count = $this->query->get()->count();
	
		if($count == 0){
			$data['Approximate date of finish'] = 'Not Available';
			return $data;
		}

		// Avg. cards per day
		$avg = round($count / $this->estDayCount, 2);

		$data["NC for last {$this->estDayCount} days"] = $count;
		$data['Avg. cards per day'] = $avg;

		// Find remaining nc count
		$this->where = [];
		$this->query = Card::query();
		$this->where['is_fy'] = null;
		$this->where['next_review_date'] = null;
		$this->where = array_merge($this->profileSettings, $this->where);
		$this->applyBasicWhere();
		$ncCount = $this->query->get()->count();

		$data['Remaining NC'] = $ncCount;

		// How many days require
		$this->daysReq = round($ncCount / $avg);

		$data['Number of days required to finish'] = $this->daysReq;
		$data['Approximate date of finish'] = Helper::addTodayWith($this->daysReq);

		return $data;

	}

	public function getLastNDays($n){
		$sevenDayBefore = Helper::addTodayWith(-$n);
		$yesterday = Helper::addTodayWith(-1);
		return Serve::findDaysBetween($sevenDayBefore, $yesterday, true, true);
	}

}
