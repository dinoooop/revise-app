<?php 

namespace App\Helpers;

use App\Helpers\Serve;
use Facades\App\Helpers\MatrixQue;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Helper;
use App\Card;
use App\Source;
use App\Subject;
use App\Module;
use App\Chapter;
use App\Tag;
use App\Option;
use Illuminate\Support\Facades\Log;
use App\Helpers\Factory\Stock;

// Facades

class ImportManager {
    
	function __construct(){
        
	}

    // Handle
	public function insertData($request){

        $this->request = $request;

        if(isset($this->request->console_file)){
            // the request is from console command
            $this->file = $this->request->console_file;
            $data = Helper::readExcel(Storage::path($this->file));
            $this->settings = Serve::getCurrentProfileSettings()->settings;
            $this->importSettings = Serve::sortSettings($this->settings);

        } else if($this->request->hasFile('csv_import')) {
            // the request is from user
            $this->file = $this->request->csv_import
            ->storeAs('public/card-import', date('Y-m-d-H-i-s') . '.xlsx');
            $data = Helper::readExcel(Storage::path($this->file));
            $this->settings = Serve::getCurrentProfileSettings()->settings;
            $this->importSettings = Serve::sortSettings($this->settings);
        } else {
            $data[] = $this->request;
            $this->importSettings = Option::get('last_card_create_settings');
        }

        
        

        if(
            isset($this->request->matrix_que) 
            && $this->request->matrix_que
        ){
            $data = MatrixQue::init($data);
        }

    	$results = [];
        $this->load();

    	foreach ($data as $key => $value) {

            if(!isset($this->request->matrix_que) && $value['RevApp'] == 1){
                continue;
            }

            $row = [];

            $this->csvRow = $value;

            if($this->request->processing){
                // custom code have to run
                // after import
                $row['mis'] = $value['mis'];
                $row['sid'] = $value['sid'];

                if(!is_null($value['sid'])){
                    $row['status'] = Stock::status('processing');
                } else {
                    $row['status'] = Stock::status('active');
                }
            }

            if($this->request->voc){
                $value['ans'] = $this->vocAns($value);
            }


            // set mandatory fields
            if(trim($value['que']) == '' || trim($value['ans']) == ''){
                continue;
            }

            $row['que'] = $value['que'];
            $row['ans'] = $value['ans'];
            
            // Find ids
            $row['source_id'] = $this->findColumnId('source')?? 
                $this->importSettings['source_id'];

            $row['subject_id'] = $this->findColumnId('subject')?? 
                $this->importSettings['subject_id'];

            $this->currentRow['subject_id'] = $row['subject_id'];

            $row['module_id'] = $this->findColumnId('module')?? 
                $this->importSettings['module_id'];

            $this->currentRow['module_id'] = $row['module_id'];

            $row['chapter_id'] = $this->findColumnId('chapter')?? 
                $this->importSettings['chapter_id'];
            
            $row = array_merge($this->importSettings, $row);

            // Set default
            $row['ans_type'] = $row['ans_type']?? Stock::ansType('vocal');
            $row['stage'] = $row['stage']?? Stock::stage('D1');
            $row['level'] = $row['level']?? 1;
            $row['status'] = $row['status']?? Stock::status('active');

            $card = Card::create($row);

            // set tags

            $tags = [];

            if(isset($value['imp']) && $value['imp'] == 1){
                $tags[] = $this->tagImpId;
            }

            if(isset($value['confused']) && $value['confused'] == 1){
                $tags[] = $this->tagconfusedId;
            }
            
            if(!empty($tags)){
                $card->tags()->sync($tags);
            }

            if(isset($this->request->tag_id)){
                $card->tags()->sync($this->request->tag_id);
            }

    		$results[] = $card->id;
    	}


    	return $results;
    
    }


    public function load(){

        $row = [];
        $records = Source::all();
        foreach ($records as $key => $value) {
            $name = strtolower($value['name']);
            $row[$name] = $value['id'];
        }
        $this->source = $row;


        $row = [];
        $records = Subject::all();
        foreach ($records as $key => $value) {
            $name = strtolower($value['name']);
            $row[$name] = $value['id'];
        }
        $this->subject = $row;


        $row = [];
        $records = Module::all();
        foreach ($records as $key => $value) {
            $name = strtolower($value['name']);
            $row[$name] = $value['id'];
        }
        $this->module = $row;

        $row = [];
        $records = Chapter::all();
        foreach ($records as $key => $value) {
            $name = strtolower($value['name']);
            $row[$name] = $value['id'];
        }
        $this->chapter = $row;

        
        $this->tagImpId = Tag::where('name', 'imp')->first()->id;
        $this->tagconfusedId = Tag::where('name', 'confused')->first()->id;


    }

    public function findColumnId($columnName){
        $cellValue = $this->csvRow[$columnName]?? null;
        $cellValueDupe = strtolower(trim($cellValue));

        if(is_null($cellValueDupe) || $cellValueDupe == ''){
            return null;
        } else if(isset($this->$columnName[$cellValueDupe])){
            // returning id 
            return $this->$columnName[$cellValueDupe];
        } else {
            // create new
            return $this->createNew($columnName, trim($cellValue));
        } 
        
    }

    public function createNew($columnName, $name){

        switch ($columnName) {
            case 'source':
                $obj = new Source;
                $id = $obj->create([
                    'name' => $name,
                ])->id;
                break;

            case 'module':
                $obj = new Module;
                $id = $obj->create([
                    'name' => $name,
                    'subject_id' => $this->currentRow['subject_id'],
                ])->id;
                break;
            
            case 'chapter':
                $obj = new Chapter;
                $id = $obj->create([
                    'name' => $name,
                    'module_id' => $this->currentRow['module_id'],
                ])->id;
                break;
        }

        $this->load();

        return $id?? null;
    
    }


    public function vocAns($value){
        $ans = "";
        if(isset($value['mal']) && !is_null($value['mal'])){
            $ans .= $value['mal'] . "\n";
        }

        if(isset($value['eng']) && !is_null($value['eng'])){
            $ans .= $value['eng'] . "\n";
        }

        if(isset($value['usage']) && !is_null($value['usage'])){
            $ans .= $value['usage'] . "\n";
        }

        // For foreign words
        if(isset($value['lang']) && !is_null($value['lang'])){
            $ans .= $value['lang'] . "\n";
        }
        return $ans;
    }

}
