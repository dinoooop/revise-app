<?php 

namespace App\Helpers;

use App\Card;
use App\Option;
use App\ReviewHistory;
use App\Helpers\Helper;
use App\Helpers\Factory\Stock;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

// A one time use class 
// for insert already studied cards 
// in same format of the app

// status: usage completed can delete this file

class OTUExistExcel {
	
	function __construct(){
		$this->intervals = explode(' ', Option::get('interval'));
	}
	
	// handle
	public function init(){
		// Perfect read excel history
		$this->xlsxData = $this->readHistoryXlsxFile();

		$this->processCount = Card::where('status', Stock::status('processing'))->get()->count();

		Card::where('status', Stock::status('processing'))
            ->chunkById(50, function ($cards) {
                foreach ($cards as $key => $card) {
                	echo  $this->processCount-- . ' ';
                	$this->card = $card;
                    $this->set();
					$this->update();
                }
            });

		
	
	}

	// Set row for single card
	public function set(){

		$this->data = $this->xlsxData[$this->card->sid];

		$this->mis = $this->card->mis;
		$this->isKW = $this->mis === 0;

		$this->setRunIntervals();
		$this->setResults();
		$this->setDays();

		$this->setStage();

		$this->setReviewHistory();
		$this->setNextReviewDate();

		//$this->card->sid = null;
		$this->card->status = Stock::status('active');
		
	}

	public function update(){


		// ReviewHistory

		foreach ($this->reviewHistory as $key => $value) {
			ReviewHistory::create($value);
		}

		$this->card->save();
	
	}

	public function setDays(){

		$this->days = [];

		// Consider it as day0 (-1)
		$this->last_review_date = Helper::addDateWith($this->data['day1'], -1);
		$this->days[] = $this->last_review_date;

		if($this->isKW){
			return;
		}

		$this->last_review_date = $this->data['day1'];
		$this->days[] = $this->last_review_date;

		if(!is_null($this->data['score7'])){
			$this->last_review_date = Helper::addDateWith($this->last_review_date, 7);
			$this->days[] = $this->last_review_date;
			if($this->results['day7'] == Stock::result('failed') ){
				$this->last_review_date = Helper::addDateWith($this->last_review_date, 1);
				$this->days[] = $this->last_review_date;
			}
		}

		if(!is_null($this->data['score14'])){
			$this->last_review_date = Helper::addDateWith($this->last_review_date, 14);
			$this->days[] = $this->last_review_date;
			if($this->results['day14'] == Stock::result('failed') ){
				$this->last_review_date = Helper::addDateWith($this->last_review_date, 1);
				$this->days[] = $this->last_review_date;
			}
		}

		if(!is_null($this->data['score30'])){
			$this->last_review_date = Helper::addDateWith($this->last_review_date, 30);
			$this->days[] = $this->last_review_date;
			if($this->results['day30'] == Stock::result('failed') ){
				$this->last_review_date = Helper::addDateWith($this->last_review_date, 1);
				$this->days[] = $this->last_review_date;
			}
		}

		if(!is_null($this->data['score60'])){
			$this->last_review_date = Helper::addDateWith($this->last_review_date, 60);
			$this->days[] = $this->last_review_date;
			if($this->results['day60'] == Stock::result('failed') ){
				$this->last_review_date = Helper::addDateWith($this->last_review_date, 1);
				$this->days[] = $this->last_review_date;
			}
		}


	}


	public function setRunIntervals(){

		$this->runIntervals = [];

		if($this->isKW){
			return;
		}

		if(!is_null($this->data['score7'])){
			$this->runIntervals[] = '7';
		}

		if(!is_null($this->data['score14'])){
			$this->runIntervals[] = '14';
		}

		if(!is_null($this->data['score30'])){
			$this->runIntervals[] = '30';
		}

		if(!is_null($this->data['score60'])){
			$this->runIntervals[] = '60';
		}

	}

	public function setResultBasedOnDay(){

		if($this->isKW){
			$this->result = Stock::result('success');
			return;
		}

		if(is_null($this->act_interval)){
			$this->result = null;
		} else if($this->act_interval >= 60){
			$this->result = $this->results['day60'];
		} else if($this->act_interval >= 30){
			$this->result = $this->results['day30'];
		} else if($this->act_interval >= 14){
			$this->result = $this->results['day14'];
		} else if($this->act_interval >= 7){
			$this->result = $this->results['day7'];
		} else {
			$this->result = Stock::result('success');
		} 
		
	}

	public function setResults(){

		//$this->mis = 7143060;
		$this->results = [];

        foreach ($this->runIntervals as $key => $value) {

        	if($value == '30'){
        		$testFor = '28';
        	} else {
        		$testFor = $value;
        	}

            if(strpos($this->mis, $testFor) !== false){
                $this->results['day' . $value] = Stock::result('failed');
            } else {
                $this->results['day' . $value] = Stock::result('success');
            }
        }
	
	}

	public function setStage(){

		$total = array_sum($this->results);
		$count = count($this->results);

		if($count == 4){
			if($total == 4){
				$this->card->stage = 'E60';
			} else if($total == 8){
				$this->card->stage = 'H60';
			} else {
				if($this->results['day60'] == Stock::result('success')){
					$this->card->stage = 'S60';
				} else {
					$this->card->stage = 'F60';
				}
			}
		} else {

			if($this->mis === 0){
				$this->card->stage = 'KW';
			} else {
				$this->card->stage = 'D1';
			}

		}

		$this->card->stage = Stock::stage($this->card->stage);
	
	}


	public function setReviewHistory(){

		$this->reviewHistory = [];

		foreach ($this->days as $key => $value) {
			$row = [];

			$row['card_id'] = $this->card->id;
			$row['review_date'] = $value;

			if($key == 0){
				$this->act_interval = null;
			} else {
				$this->act_interval = Helper::dateDiff(
					$this->days[$key], 
					$this->days[$key - 1]
				);
			}

			$row['act_interval'] = $this->act_interval;

			$this->setResultBasedOnDay();
			$row['result'] = $this->result;

			$this->reviewHistory[] = $row;

			//ReviewHistory::create($row);
		}
	
	}

	

	public function print(){

		echo '<pre>' , print_r($this) , '</pre>';
		exit();
	
	}

	public function setNextReviewDate(){

		// [0, 1, 2,   3,  4] order
		// [1, 7, 14, 30, 60] interval

		$this->card->last_interval_order = null;

		if(!is_null($this->data['score7'])){
			$this->card->last_interval_order = 1;
		}

		if(!is_null($this->data['score14'])){
			$this->card->last_interval_order = 2;
		}

		if(!is_null($this->data['score30'])){
			$this->card->last_interval_order = 3;
		}

		if(!is_null($this->data['score60'])){
			$this->card->last_interval_order = 4;
		}

        $newIntervalOrder = is_null($this->card->last_interval_order) ? 0 : $this->card->last_interval_order + 1;
        
        if(isset($this->intervals[$newIntervalOrder])){
        	$interval = $this->intervals[$newIntervalOrder];
        	$this->card->next_review_date = Helper::addDateWith($this->last_review_date, $interval);
        	$this->card->last_interval_order = $newIntervalOrder;
        } else {
        	// upgrade the question to next level
        	$this->card->level = 2;
        	$this->card->next_review_date = null;
        	$this->card->last_interval_order = null;
        }
        
    }

    public function readHistoryXlsxFile(){
    	$file = Storage::path('public/processing-files/study-set-history.xlsx');
    	$data = Helper::readExcel($file);

    	$results = [];
    	foreach ($data as $key => $value) {
    		$results[$value['sid']] = $value;
    	}

    	return $results;    
    }
	
}
