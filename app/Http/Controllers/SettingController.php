<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Option;

class SettingController extends Controller
{

    public function create()
    {
        $results = [];
        $results['interval'] = Option::get('interval');
        $results['show_subdocs'] = Option::get('show_subdocs');

        $results['estimate_finish_day_count'] = Option::get('estimate_finish_day_count');

        $data['results'] = $results;
        return response()->json($data);
    }
    
    public function store(Request $request)
    {
        $data = [];

        Option::set('interval', $request->interval);
        Option::set('show_subdocs', $request->show_subdocs);
        Option::set('estimate_finish_day_count', $request->estimate_finish_day_count);

        $data['results'] = true;
        return response()->json($data);
    }

}
