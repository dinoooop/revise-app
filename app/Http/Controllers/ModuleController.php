<?php

namespace App\Http\Controllers;

use App\Module;
use App\Http\Requests\ModuleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;

class ModuleController extends Controller
{

    public function index(Request $request)
    {

        $data = [];
        if(isset($request->subject_id)){
            $data['results'] = Module::where('subject_id', $request->subject_id)->get();
        } else {
            $data['results'] = Module::all();
        }
        
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $data = [];
        
        $subject = new Module;
        $data['results'] = $subject->create($request->all());
        return response()->json($data);
    }

    public function show($id)
    {
        $data = [];
        $data['results'] = Module::find($id);
        return response()->json($data);
    }


    public function update(Request $request, $id)
    {
        $data = [];
        $subject = Module::find($id);
        $data['results'] = $subject->update($request->all());
        return response()->json($data);
    }

    public function destroy(Request $request, $id)
    {
        // No delete
    }
}
