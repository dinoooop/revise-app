<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Http\Requests\TagRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;

class TagController extends Controller
{

    public function index(Request $request)
    {

        $data = [];
        $data['results'] = Tag::all();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $data = [];
        
        $tag = new Tag;
        $data['results'] = $tag->create($request->all());
        return response()->json($data);
    }

    public function edit($id)
    {
        $data = [];
        $data['results'] = Tag::find($id);
        return response()->json($data);
    }


    public function update(Request $request, $id)
    {
        $data = [];
        $tag = Tag::find($id);
        $data['results'] = $tag->update($request->all());
        return response()->json($data);
    }

    public function destroy(Request $request, $id)
    {
        $tag = Tag::find($id);
        $data['results'] = $tag->delete();
        return response()->json($data);
    }
}
