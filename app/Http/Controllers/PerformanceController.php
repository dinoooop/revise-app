<?php

namespace App\Http\Controllers;

use App\Performance;
use Illuminate\Http\Request;
use Facades\App\Helpers\PerformanceManager;

class PerformanceController extends Controller
{

    public function index()
    {
    	
    	PerformanceManager::execute();

    	$data = [];
    	$data['results'] = Performance::orderBy('review_date', 'desc')->paginate(100);
    	return response()->json($data);
    }

}
