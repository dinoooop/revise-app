<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use App\User;

class StockController extends Controller
{

    private $limit = 6;

	//api/select-option
	//addresses/state?search=
	//addresses/city?search=
    public function selectOption(Request $request){
    	$data['results'] = [];
    	$table = $request->table;
    	$column = $request->column;
    	
    	if(!$this->isValidateTableColumn($table, $column)){
    		return response()->json($data);
    	}
    	
    	if(isset($request->search)){
    		$query = DB::table($table);
    		$query->where($column, 'like', "%{$request->search}%");
    		$query->distinct($column);
    		$data['results'] = $query->limit($this->limit)->pluck($column);
    	}
    	
    	return response()->json($data);
    }

	//api/select-option-label-value
	//countries/name/id?search=
	//roles/label/id/all
	//capabilities/label/id/all
	//departments/name/id/all
	//designations/name/id/all
    public function selectOptionNameValue(Request $request){

    	$data['results'] = [];
    	$table = $request->table;
    	$label = $request->label;
    	$value = $request->value;

    	if(!$this->isValidateTableColumn($table, $label)){
    		return response()->json($data);
    	}

    	$query = DB::table($table)->select($label, $value);
    	if(isset($request->all)){
    		$data['results'] = Helper::convertLabelValuePair($query->get(), $label, $value);	
    	}
    	
    	if(isset($request->search)){
    		$query->where($label, 'like', "%{$request->search}%");
            $results = $query->limit($this->limit)->get();
    		$data['results'] = Helper::convertLabelValuePair($results, $label, $value);
    	}

    	return response()->json($data);
    }

    public function isValidateTableColumn($table, $column){

    	$allowTable = [
	    	'countries',
	    	'roles',
	    	'capabilities',
	    	'subjects',
	    	'designations',
	    	'addresses',
            'tags',
    	];

    	$allowColumns = [
	    	'state', 
	    	'city', 
	    	'label', 
	    	'name',
    	];

    	return in_array($table, $allowTable) && in_array($column, $allowColumns);
    
    }

    public function users(Request $request){

        $data['results'] = [];

        if(!isset($request->search)){
            return response()->json($data);            
        }

        $results = User::select(DB::raw('id, concat(first_name, " ", last_name) as name'))
        ->where('first_name', 'like', "%{$request->search}%")
        ->orWhere('last_name', 'like', "%{$request->search}%")
        ->limit($this->limit)
        ->get();

        $data['results'] = Helper::convertLabelValuePair($results, 'name', 'id');   

        return response()->json($data);

    
    }
}
