<?php

namespace App\Http\Controllers;

use App\Source;
use App\Http\Requests\SourceRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;

class SourceController extends Controller
{

    public function index(Request $request)
    {

        $data = [];
        $data['results'] = Source::all();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $data = [];
        
        $source = new Source;
        $data['results'] = $source->create($request->all());
        return response()->json($data);
    }

    public function show($id)
    {
        $data = [];
        $data['results'] = Source::find($id);
        return response()->json($data);
    }

    public function edit($id)
    {
        $data = [];
        $data['results'] = Source::find($id);
        return response()->json($data);
    }


    public function update(Request $request, $id)
    {
        $data = [];
        $source = Source::find($id);
        $data['results'] = $source->update($request->all());
        return response()->json($data);
    }

    public function destroy(Request $request, $id)
    {
        // No delete
    }
}
