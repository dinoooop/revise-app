<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use App\CardTag;
use App\Subdoc;
use App\ReviewHistory;
use Facades\App\Helpers\CardManager;
use Facades\App\Helpers\ImportManager;

class CardController extends Controller
{
    
    public function index(Request $request)
    {
        $data = [];
        $data['results'] = CardManager::index($request);
        return response()->json($data);
    }

    public function edit(Request $request, $id)
    {
        
        $data['results'] = CardManager::edit($request, $id);
        
        return response()->json($data);
    }

    public function create(Request $request)
    {
        
        $data['results'] = CardManager::create($request);
        
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $data = [];

        if ($request->hasFile('csv_import')) {
            $inserted = ImportManager::insertData($request);
            $data['results']['message'] = count($inserted) . ' cards have inserted';
        }


        // create card using html form
        if ($request->action == 'store') {
            $inserted = ImportManager::insertData($request);
            $data['results']['message'] = 'card created';
        }


        
     
        return response()->json($data);
        
    }

    public function show($id)
    {
        // No show
    }


    public function update(Request $request, $id)
    {
        $data = [];
        if($request->action == 'source_switch'){
            $data['results'] = CardManager::sourceSwitch($request, $id);
        } else {

            $data['results'] = CardManager::update($request, $id);
        }
        //$data['results'] = $request->all();
        return response()->json($data);
    }

    public function destroy(Request $request, $id)
    {
        $card = Card::find($id);
        $data['results'] = $card->delete();
        // delete from tag
        CardTag::where('card_id', $id)->delete();
        Subdoc::where('card_id', $id)->delete();
        ReviewHistory::where('card_id', $id)->delete();
        // delete from subdoc
        // delete from review history

        return response()->json($data);
    }
}
