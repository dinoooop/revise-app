<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Helper;
use App\Helpers\Factory\Stock;
use App\Helpers\SimpleXLSX;
use App\Helpers\Serve;
use App\Card;
use App\Module;
use App\Chapter;
use App\Performance;
use App\Subject;
use App\ReviewHistory;
use App\Option;
use App\Tag;
use App\ReviewLeft;
use Facades\App\Helpers\ReviewManager;
use Facades\App\Helpers\ImportManager;
use Facades\App\Helpers\ImageClassifyManager;
use Facades\App\Helpers\ReviewLeftManager;
use Facades\App\Helpers\CardManager;
use Facades\App\Helpers\PerformanceManager;
use Facades\App\Helpers\StatisticsManager;
use Facades\App\Helpers\EstimateDateFinish;
use Facades\App\Helpers\MatrixQue;
use Facades\App\Helpers\OTUExistExcel;
use Facades\App\Http\Controllers\ImageClassifyController;

class TestController extends Controller
{
    public function test(){

        

        

        echo '<pre>' , print_r([]) , '</pre>';
        exit();
        
    }


    

    public function applyBasicWhere(){

        foreach ($this->where as $key => $value) {

            if(is_array($value) && empty($value)){
                continue;
            }

            // allow null not '' (stage can be null)

            if($value === ''){
                continue;
            }

            $nullSearchAllowColumn = ['is_fy', 'next_review_date', 'result'];

            if(is_null($value)){
                if(!in_array($key, $nullSearchAllowColumn)){
                    continue;
                }
            }
            
            if($key == 'tag_id'){
                if(!is_null($value)){
                    $this->query->whereHas('tags', function($q) use($value) {
                        $q->whereIn('tag_id', $value);
                    });

                }
                continue;
            }

            if(is_array($value)){
                $this->query->whereIn($key, $value);                
            } else {
                $this->query->where($key, $value);
            }
        }
        
    }

    public function isFailed(){

        $data = '';

        $days = ['7', '14', '28', '60'];

        foreach ($days as $key => $value) {
            if(strpos($data, $value) !== false){
                $this->result['day' . $value] = Stock::result('failed');
            } else {
                $this->result['day' . $value] = Stock::result('success');
            }
        }
    
    }

    public function mark(){
        return 'null';
    
    }

    public function findCount($review_mode){

        $request = new Request([
            'review_mode' => $review_mode,
        ]);

        return CardManager::findCount($request);
    
    }

    public function testDateBetween(){

        foreach ($variable as $key => $value) {
            if(is_array($value) && empty($value)){
                continue;
            }

            if(is_null($value)){
                continue;
            }
        }
        
        $start = '2021-07-01';
        $end = '2021-07-10';
        $data = Serve::findDaysBetween($start, $end, true, true);

    
        echo '<pre>' , print_r($data) , '</pre>';
        exit();

    }

    public function getCardForReview(){

        $request = [
            'review_mode' => 'NC',
            'subject_id' => 2,
            'ans_type' => 1,
        ];

        $request = new Request($request);
        $data = CardManager::review($request);
        echo '<pre>' , print_r($data) , '</pre>';
        exit();
    
    }

    public function testReview(Request $request)
    {

        $request = [
            'action' => 'review',
            'result' => null,
            'status' => 2,
            'stage' => null,
            'tag_id' => [1,2],
        ];

        $request = new Request($request);
        $data = ReviewManager::cardReviewed(2, $request);

        echo '<pre>' , print_r($data) , '</pre>';
        exit();

    }

    

    public function generateSampleReviewHistory(){
    	$day = '2021-02-12';

    	$interval = [0, 1, 7, 14, 30, 60];
    	foreach ($interval as $key => $value) {
    		$day = date('Y-m-d', strtotime($day . "+{$value} Days"));
    		ReviewHistory::create([
	            'card_id' => 30,
	            'review_date'  => $day,
	            'act_interval' => $value,
	            'result' => 1
	        ]);
    	}
    }

    public function sqlBackup(Request $request){
        $filename = 'revise_app_' . date('Y-m-d-H-i-s') . '.sql';
        echo env('DB_DATABASE');
        
        $command = "mysqldump --user=" . env('DB_USERNAME') 
        . " --password=" . env('DB_PASSWORD') 
        . " --host=" . env('DB_HOST') 
        . " " . env('DB_DATABASE') . "  > " 
        . storage_path() . "/app/backup/" . $filename;

        $returnVar = NULL;
        $output = NULL;
        echo $command;
        //exec($command, $output, $returnVar);

    }

}

