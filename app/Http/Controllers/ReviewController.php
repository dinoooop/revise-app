<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use Facades\App\Helpers\CardManager;
use Facades\App\Helpers\ReviewManager;
use Facades\App\Helpers\ReviewLeftManager;

class ReviewController extends Controller
{
    


    public function create(Request $request)
    {
        // Get a card for review
        $data = [];
        $data['results'] = CardManager::review($request);
        return response()->json($data);

    }

    public function update(Request $request, $id)
    {
        // update the card as reviewed
        $data = [];

        if(
            $request->action == 'failed-last' 
            || $request->action == 'success-last'
        ){
            $data['results'] = ReviewManager::undoLastReview($request);
        } else {
            $data['results'] = ReviewManager::cardReviewed($id, $request);
        }
        
        return response()->json($data);
    }

}
