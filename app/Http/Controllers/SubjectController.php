<?php

namespace App\Http\Controllers;

use App\Subject;
use App\Module;
use App\Chapter;
use App\Http\Requests\SubjectRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;
use App\Helpers\Factory\Stock;

class SubjectController extends Controller
{

    public function index(Request $request)
    {
        $data = [];
        $data['results'] = Subject::all();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $data = [];
        
        $subject = new Subject;
        $data['results'] = $subject->create($request->all());
                
        return response()->json($data);
    }

    public function show($id)
    {
        $data = [];
        $data['results'] = Subject::find($id);
        return response()->json($data);
    }


    public function update(Request $request, $id)
    {
        $data = [];
        $subject = Subject::find($id);
        $data['results'] = $subject->update($request->all());
        return response()->json($data);
    }

    public function destroy(Request $request, $id)
    {
        // No delete
    }

}
