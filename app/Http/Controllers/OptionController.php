<?php

namespace App\Http\Controllers;

use App\Option;
use App\Http\Requests\OptionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;

class OptionController extends Controller
{

    public function index(Request $request)
    {

        $data = [];
        $data['results'] = Option::all();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        // No store by default there will be value
        // Only update
    }

    public function show($id)
    {
        // NULL
    }


    public function update(Request $request, $id=0)
    {
        // No individual updation

        $data = [];
        
        foreach ($request->all() as $key => $value) {
            $option = Option::where('option_key', $key)->first();
            $option->option_value = $value;
            $data[] = $option->update();
        }
        
        return response()->json($data);
    }

    public function destroy(Request $request, $id)
    {
        // No delete
    }
}
