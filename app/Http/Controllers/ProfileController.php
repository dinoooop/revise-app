<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Facades\App\Helpers\ProfileManager;

class ProfileController extends Controller
{

    public function index(Request $request)
    {

        $data = $results = $row = [];
        
        $profiles = Profile::orderBy('name')->get();
        foreach ($profiles as $key => $profile) {
            $row['id'] = $profile->id;
            $row['name'] = $profile->name;
            $row['is_active'] = $profile->is_active;
            $row['settings'] = unserialize($profile->settings);
            $results[] = $row;
        }

        $data['results'] = $results;
        return response()->json($data);
    }

    // Preofile create page (View)
    public function create(Request $request)
    {
        $data['results'] = ProfileManager::create($request);
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $data = [];
        $data['results'] = ProfileManager::store($request);
        return response()->json($data);
        
    }

    public function edit(Request $request, $id)
    {
        $data['results'] = ProfileManager::edit($request, $id);
        return response()->json($data);
    }

    public function show($id)
    {
        // No Show
    }

    public function update(Request $request, $id)
    {
        $data = [];
        $data['results'] = ProfileManager::update($request, $id);
        return response()->json($data);
        
    }

    public function destroy(Request $request, $id)
    {
        $profile = Profile::find($id);
        $data['results'] = $profile->delete();
        return response()->json($data);
    }
}
