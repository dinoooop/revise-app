<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Http\Requests\ChapterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Helpers\Helper;

class ChapterController extends Controller
{

    public function index(Request $request)
    {

        $data = [];
        if(isset($request->module_id)){
            $data['results'] = Chapter::where('module_id', $request->module_id)->get();
        } else {
            $data['results'] = Chapter::all();
        }
        
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $data = [];
        
        $chapter = new Chapter;
        $data['results'] = $chapter->create($request->all());
        return response()->json($data);
    }

    public function show($id)
    {
        $data = [];
        $data['results'] = Chapter::find($id);
        return response()->json($data);
    }


    public function update(Request $request, $id)
    {
        $data = [];
        $chapter = Chapter::find($id);
        $data['results'] = $chapter->update($request->all());
        return response()->json($data);
    }

    public function destroy(Request $request, $id)
    {
        // No delete
    }
}
