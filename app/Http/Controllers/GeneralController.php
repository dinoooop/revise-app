<?php

namespace App\Http\Controllers;

use App\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Facades\App\Helpers\StatisticsManager;
use Facades\App\Helpers\ReviewLeftManager;
use Facades\App\Helpers\MathsClassifyManager;
use Facades\App\Helpers\ReviewHistoryManager;

class GeneralController extends Controller
{

    public function index(Request $request)
    {

        $data = [];
        
        if($request->action == 'statistics'){
            $data['results'] = StatisticsManager::get();
        }

        if($request->action == 'review_left'){
            $data['results'] = ReviewLeftManager::get();
        }

        if($request->action == 'maths_classify'){
            $data['results'] = MathsClassifyManager::getFirst();
        }

        if($request->action == 'review_history'){
            $data['results'] = ReviewHistoryManager::list();
        }
        
        return response()->json($data);
    }

    public function store(Request $request){

        if($request->action == 'maths_classify'){
            $data['results'] = MathsClassifyManager::move($request);
        }

    
    }

}
