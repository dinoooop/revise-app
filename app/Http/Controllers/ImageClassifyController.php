<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Facades\App\Helpers\ImageClassifyManager;

class ImageClassifyController extends Controller
{

    /**
    *
    * action - modulelist, first, next, prev
    *
    * module (optional)
    */
    public function create(Request $request)
    {
        $data = [];

        ImageClassifyManager::setRequest($request);

        if($request->action == 'first'){

            // request : -
            // action = next / prev
            // module = 'english'
            //
            // response : -
            // image_url
            // next_prev_active
            $data['results'] = ImageClassifyManager::getFirst();

        } else if($request->action == 'next'){

            // request : -
            // action = next / prev
            // module = 'english'
            // current = ''
            //
            // response : -
            // image_url
            // next_prev_active
            $data['results'] = ImageClassifyManager::getNext();

        } else if($request->action == 'prev'){
            $data['results'] = ImageClassifyManager::getPrev();
        } else {
            // No params
            // List modules
            $data['results'] = ImageClassifyManager::getModuleList();
        }

        return response()->json($data);
    }

    /**
    *
    * module
    * current
    * type
    * action (optional)
    * upto (action = fast)
    * tag_id (optional)
    * ans_type (optional)
    */
    public function store(Request $request)
    {
        $data = [];

        if($request->action == 'fast'){
            ImageClassifyManager::setRequest($request);
            $data['results'] = ImageClassifyManager::fastInsert();
        } else {
            ImageClassifyManager::setRequest($request);
            $data['results'] = ImageClassifyManager::saveFile();
        }
        return response()->json($data);
    }

}
