<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormTesterController extends Controller
{

    public function create(Request $request)
    {
        
    }

    public function store(Request $request)
    {
        $data = [];

        if(isset($request->name)){
            $data['results'] = "Name have set";
        } else {
            $data['results'] = "Name not set";
        }

        return response()->json($data);
    }

}
