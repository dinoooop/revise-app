<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Facades\App\Helpers\SmcManager;

class SmcController extends Controller
{

    public function create(Request $request)
    {
        $data = [];
        $data['results'] = SmcManager::create($request);
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $data = [];
        $data['results'] = SmcManager::store($request);
        return response()->json($data);
    }


    public function update(Request $request, $id)
    {
        $data = [];
        $data['results'] = SmcManager::update($request, $id);
        return response()->json($data);
    }

}
