<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = [
    	'que',
		'ans',
		'stage',
		'is_fy',
		'next_review_date',
		'last_interval_order',
		'last_review_date',
		'status',
		'sid',
		'mis',
		'level',
		'subject_id',
		'module_id',
		'chapter_id',
		'source_id',
		'ans_type',
	];

	public function tags()
	{
		return $this->belongsToMany('App\Tag');
	}

	public function module()
	{
		return $this->hasOne('App\Module', 'id', 'module_id');
	}

	public function source()
	{
		return $this->hasOne('App\Source', 'id', 'source_id');
	}

	public function subdocs()
	{
		return $this->hasMany('App\Subdoc');
	}

	public function reviewHistory()
	{
		return $this->hasMany('App\ReviewHistory');
	}

	


}
