<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MathsClassify extends Model
{
	protected $table = 'maths_classify';
    protected $fillable = [
    	'filename',
    	'is_classified',
    	'filepath',
		'subject_id',
		'module_id',
		'chapter_id',
		'ans_type',
		'source_id',
		'stage',
		'level',
		'status',
	];

	public $timestamps = false;
}
