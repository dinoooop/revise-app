<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    public $timestamps = false;
    protected $paginate = 100;

	protected $fillable = [
		'review_date',
		'success',
		'failed',
		'nc',
		'total_card',
		'mark',
		'status',
	];
}
